package json;

import main.Logger;
import model.*;
import odds.MatchOdds;
import odds.OddsBuilder;
import odds.markets.*;
import model.statistics.EmptyModelStatistics;
import model.statistics.ModelStatitics;
import model.statistics.Stats;
import model.statistics.StatisticsType;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.JSONUtils;

import java.util.ArrayList;
import java.util.List;

public class JSONRetriever {

    public static List<Fixture> getFixturesFromJSONObject(JSONObject obj){

        List<Fixture> fixturesModel = new ArrayList<>();

        for ( Object keyObject: obj.keySet() ){

            String key = (String) keyObject;
            JSONObject fixture = JSON.getJSON(obj,key);
            if ( fixture == null ){
                Logger.getLogger(JSONRetriever.class).warn("Fixture JSONObject is null. Skipping...");
                continue;
            }

            String id = JSON.getString(fixture,"fixture_id");
            String event_timestamp = JSON.getString(fixture,"event_timestamp");
            String event_date = JSON.getString(fixture,"event_date");
            String league_id = JSON.getString(fixture,"league_id");
            String round = JSON.getString(fixture,"round");
            String homeTeam_id = JSON.getString(fixture,"homeTeam_id");
            String awayTeam_id = JSON.getString(fixture,"awayTeam_id");
            String homeTeam = JSON.getString(fixture,"homeTeam");
            String awayTeam = JSON.getString(fixture,"awayTeam");
            String status = JSON.getString(fixture,"status");
            String statusShort = JSON.getString(fixture,"statusShort");
            String goalsHomeTeam = JSON.getString(fixture,"goalsHomeTeam");
            String goalsAwayTeam = JSON.getString(fixture,"goalsAwayTeam");
            String halftime_score = JSON.getString(fixture,"halftime_score");
            String final_score = JSON.getString(fixture,"final_score");
            Object penalty = fixture.get("penalty");
            String elapsed = JSON.getString(fixture,"elapsed");
            String firstHalfStart =  JSON.getString(fixture,"firstHalfStart");
            String secondHalfStart = JSON.getString(fixture,"secondHalfStart");

            Fixture fixtureModel = new Fixture(id,event_timestamp,event_date,league_id,round,homeTeam_id,awayTeam_id,
                    homeTeam,awayTeam,status,statusShort,goalsHomeTeam,goalsAwayTeam,halftime_score,final_score,
                    penalty,elapsed,firstHalfStart,secondHalfStart);

            fixturesModel.add(fixtureModel);

        }
        return fixturesModel;
    }


    public static List<Team> getTeamsFromJSONObject(JSONObject obj){
        List<Team> teams = new ArrayList<>();

        for( Object keyObject: obj.keySet() ){
            String key = (String) keyObject;
            JSONObject teamObj = JSON.getJSON(obj,key);

            String id = JSON.getString(teamObj, "team_id");
            String name = JSON.getString(teamObj, "name");
            String logoUrl = JSON.getString(teamObj, "logo");

            Team team = new Team(id,name,logoUrl);
            teams.add(team);

        }
        return teams;
    }

    public static List<Season> getSeasonsFromJSONObject(JSONObject obj){
        List<Season> seasons = new ArrayList<>();

        for (Object keyObject : obj.keySet()) {
            String key = (String) keyObject;
            String year = JSON.getString(obj,key);
            Season season = new Season(year);

            seasons.add(season);
        }
        return seasons;
    }

    public static List<Country> getCountriesFromJSONObject(JSONObject obj) {
        List<Country> countries = new ArrayList<>();

        for (Object keyObject : obj.keySet()) {
            String key = (String) keyObject;
            String name = JSON.getString(obj, key);
            Country country = new Country(key, name);

            countries.add(country);
        }
        return countries;
    }

    public static List<League> getLeaguesFromJSONObject(JSONObject obj) {
        List<League> leagues = new ArrayList<>();

        for (Object keyObject : obj.keySet()) {

            String key = (String) keyObject;
            JSONObject leagueObj = JSON.getJSON(obj,key);

            String id = JSON.getString(leagueObj, "league_id");
            String name = JSON.getString(leagueObj, "name");
            String countryName = JSON.getString(leagueObj, "country");
            String seasonYear = JSON.getString(leagueObj, "season");
            String seasonStartDate = JSON.getString(leagueObj, "season_start");
            String seasonEndDate = JSON.getString(leagueObj, "season_end");
            String logoUrl = JSON.getString(leagueObj, "logo");

            League league = new League(id, name, countryName, seasonYear, seasonStartDate, seasonEndDate, logoUrl);
            leagues.add(league);
        }

        return leagues;
    }

    public static MatchOdds getOddsFromFixtureJSONObject(JSONObject obj, String fixtureId){

        if ( obj == null ){
            Logger.getLogger(JSONRetriever.class).warn("JSONObject contains no Odds. Empty Match Odds returned");
            return new MatchOdds(); //empty match odd
        }

        JSONObject winTheMatchObj = JSONUtils.getChildObjectThatStartsWith(obj, "Win the match");
        AbstractMarketOdds hdaOdds = OddsBuilder.buildHDAOdds(winTheMatchObj);

        JSONObject halfTimeObj = JSONUtils.getChildObjectThatStartsWith(obj, "Half Time");
        AbstractMarketOdds htOdds = OddsBuilder.buildHTOdds(halfTimeObj);

        //Not 'Exact score'
        JSONObject exactScoreObj = JSONUtils.getChildObjectThatStartsWith(obj, "Score exact");
        AbstractMarketOdds exactScoreOdds = OddsBuilder.buildExactScoreOdds(exactScoreObj);

        //Not 'Dorble chance'
        JSONObject doubleChanceObj = JSONUtils.getChildObjectThatStartsWith(obj, "Double chance");
        AbstractMarketOdds dcOdds = OddsBuilder.buildDCOdds(doubleChanceObj);

        JSONObject hdaBTTSObj = JSONUtils.getChildObjectThatStartsWith(obj, "Result & The 2 teams score");
        AbstractMarketOdds hdaBttsOdds = OddsBuilder.buildHDABTTSOdds(hdaBTTSObj);

        JSONObject bttsObj = JSONUtils.getChildObjectThatStartsWith(obj, "Both teams score");
        AbstractMarketOdds bttsOdds = OddsBuilder.buildBTTSOdds(bttsObj);

        JSONObject OU4_5ob = JSONUtils.getChildObjectThatStartsWith(obj, "Over/Under 4,5 goals");
        AbstractMarketOdds underOver4_5Odds = OddsBuilder.buildUnderOver4_5Odds(OU4_5ob);

        JSONObject OU3_5ob = JSONUtils.getChildObjectThatStartsWith(obj, "Over/Under 3,5 goals");
        AbstractMarketOdds underOver3_5Odds = OddsBuilder.buildUnderOver3_5Odds(OU3_5ob);

        JSONObject OU2_5ob = JSONUtils.getChildObjectThatStartsWith(obj, "Over/Under 2,5 goals");
        AbstractMarketOdds underOver2_5Odds = OddsBuilder.buildUnderOver2_5Odds(OU2_5ob);

        JSONObject OU1_5ob = JSONUtils.getChildObjectThatStartsWith(obj, "Over/Under 1,5 goals");
        AbstractMarketOdds underOver1_5Odds = OddsBuilder.buildUnderOver1_5Odds(OU1_5ob);

        JSONObject firstGoalOb = JSONUtils.getChildObjectThatStartsWith(obj, "1st goal");
        AbstractMarketOdds firstGoalOdds = OddsBuilder.buildFirstGoalOdds(firstGoalOb);

        JSONObject htFtOb = JSONUtils.getChildObjectThatStartsWith(obj, "HT/FT");
        AbstractMarketOdds htFtOdds = OddsBuilder.buildHT_FTOdds(htFtOb);

        AbstractMarketOdds hdaUnderOverOdds = new EmptyMarketOdds(); //TODO?

        JSONObject doubleChanceHtFtObj = JSONUtils.getChildObjectThatStartsWith(obj, "Double Chance HT");
        AbstractMarketOdds dcHtOdds = OddsBuilder.buildDC_HTOdds(doubleChanceHtFtObj);

        JSONObject exactScoreHtObj = JSONUtils.getChildObjectThatStartsWith(obj, "Exact score HT");
        AbstractMarketOdds exactScoreHtOdds = OddsBuilder.buildExactScoreHTOdds(exactScoreHtObj);

        JSONObject bttsHTObj = JSONUtils.getChildObjectThatStartsWith(obj, "Both teams score HT");
        AbstractMarketOdds bttsHTOdds = OddsBuilder.buildBTTS_HTOdds(bttsHTObj);

        return new MatchOdds(
            fixtureId,hdaOdds,bttsHTOdds,bttsOdds,dcHtOdds,dcOdds,firstGoalOdds,hdaUnderOverOdds,htFtOdds,
                htOdds, underOver1_5Odds, underOver2_5Odds, underOver3_5Odds, underOver4_5Odds,
                exactScoreOdds, exactScoreHtOdds, hdaBttsOdds
        );

    }

    public static Standing getStandingsFromJSONArray(JSONArray array, String leagueId){

        if ( array == null ) return null;
        List<StandingPosition> positionList = new ArrayList<>();

        for( int i = 0; i < array.length(); i++ ){
            JSONObject positionOb = (JSONObject) array.get(i);

            String rank = JSON.getString(positionOb, "rank");
            String teamName = JSON.getString(positionOb, "teamName");
            String group = JSON.getString(positionOb, "group");
            String lastUpdate = JSON.getString(positionOb, "lastUpdate");
            int played = JSON.getInteger(positionOb, "matchsPlayed");
            int won = JSON.getInteger(positionOb, "win");
            int draws = JSON.getInteger(positionOb, "draw");
            int lost = JSON.getInteger(positionOb, "lose");
            int goalsFor = JSON.getInteger(positionOb, "goalsFor");
            int goalsConceived = JSON.getInteger(positionOb, "goalsAgainst");
            int goalDiff = JSON.getInteger(positionOb, "goalsDiff");
            int points = JSON.getInteger(positionOb, "points");

            StandingPosition position = new StandingPosition(rank, teamName, played, won, draws,
                    lost, goalsFor, goalsConceived, goalDiff, points, group, lastUpdate);

            positionList.add(position);

        }

        return new Standing(leagueId,positionList);
    }

    public static ModelStatitics getTeamStatistics(JSONObject obj, String teamId){

        if ( obj == null ) return new EmptyModelStatistics(teamId);

        JSONObject matches = JSON.getJSON(obj,"matchs");
        if ( matches == null ) return new EmptyModelStatistics(teamId);

        JSONObject matchesPlayedOb = JSON.getJSON(matches, "matchsPlayed");
        int matchesPlayedHome = JSON.getInteger(matchesPlayedOb,"home");
        int matchesPlayedAway = JSON.getInteger(matchesPlayedOb, "away");
        int matchesPlayedTotal = JSON.getInteger(matchesPlayedOb, "total");

        JSONObject winsOb = JSON.getJSON(matches, "wins");
        int winsHome = JSON.getInteger(winsOb, "home");
        int winsAway = JSON.getInteger(winsOb, "away");
        int winsTotal = JSON.getInteger(winsOb, "total");

        JSONObject drawOb = JSON.getJSON(matches, "draws");
        int drawsHome = JSON.getInteger(drawOb, "home");
        int drawsAway = JSON.getInteger(drawOb, "away");
        int drawsTotal = JSON.getInteger(drawOb, "total");

        JSONObject lostOb = JSON.getJSON(matches, "loses");
        int lostHome = JSON.getInteger(lostOb, "home");
        int lostAway = JSON.getInteger(lostOb, "away");
        int lostTotal = JSON.getInteger(lostOb, "total");


        JSONObject goalsOb = JSON.getJSON(obj, "goals");
        if ( goalsOb == null ) return new EmptyModelStatistics(teamId);

        JSONObject goalsFor = JSON.getJSON(goalsOb, "goalsFor");
        int goalsForHome = JSON.getInteger(goalsFor, "home");
        int goalsForAway = JSON.getInteger(goalsFor, "away");
        int goalsForTotal = JSON.getInteger(goalsFor, "total");

        JSONObject goalsRec = JSON.getJSON(goalsOb, "goalsAgainst");
        int goalsRecHome = JSON.getInteger(goalsRec, "home");
        int goalsRecAway = JSON.getInteger(goalsRec, "away");
        int goalsRecTotal = JSON.getInteger(goalsRec, "total");

        Stats homeStats = new Stats(StatisticsType.HOME_STATISTICS, matchesPlayedHome,
                winsHome, drawsHome, lostHome, goalsForHome, goalsRecHome);

        Stats awayStats = new Stats(StatisticsType.AWAY_STATISTICS, matchesPlayedAway,
                winsAway, drawsAway, lostAway, goalsForAway, goalsRecAway);

        Stats totalStats = new Stats(StatisticsType.TOTAL_STATISTICS, matchesPlayedTotal,
                winsTotal, drawsTotal, lostTotal, goalsForTotal, goalsRecTotal);

        return new ModelStatitics(teamId,homeStats,awayStats,totalStats);
    }
}
