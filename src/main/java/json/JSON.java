package json;

import main.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.JSONUtils;

public class JSON {

    public static String getString(Object obj, String key) {

        if ( obj == null ){
            Logger.getLogger(JSON.class).debug("Value for key: " + key + " is NULL");
            return "";
        }
        if ( ! (obj instanceof JSONObject) ){
            Logger.getLogger(JSON.class).debug("Object: " + obj + " is not a JSON Object.");
            return "";
        }
        JSONObject jsonObject = (JSONObject) obj;

        if (jsonObject.isNull(key)) {
            Logger.getLogger(JSON.class).debug("Value for key: " + key + " is NULL");
            return "";
        }

        if (!jsonObject.has(key)) {
            Logger.getLogger(JSON.class).debug("JSONObject: " + jsonObject + " has no key: " + key);
            return "";
        }

        return jsonObject.getString(key);

    }

    public static Integer getInteger(Object obj, String key) {

        if (obj == null) {
            Logger.getLogger(JSON.class).warn("Value for key: " + key + " is NULL");
            return -1;
        }
        if (!(obj instanceof JSONObject)) {
            Logger.getLogger(JSON.class).warn("Object: " + obj + " is not a JSON Object.");
            return -1;
        }

        JSONObject jsonObject = (JSONObject) obj;
        if ( jsonObject.isNull(key) ) {
            Logger.getLogger(JSON.class).debug("Value for key: " + key + " is NULL");
            return -1;
        }

        if ( !jsonObject.has(key) ) {
            Logger.getLogger(JSON.class).debug("JSONObject: " + obj + " has no key: " + key);
            return -1;
        }

        return jsonObject.getInt(key);

    }


    public static JSONObject getJSON(JSONObject obj, String key) {

        if ( obj.isNull(key) )  {
            Logger.getLogger(JSON.class).debug("Value for key: " + key + " is NULL\nKeys available: " +
                    JSONUtils.getJSONObjectKeys(obj));
            return null;
        }

        if ( !obj.has(key) ) {
            Logger.getLogger(JSON.class).debug("JSONObject: " + obj + " has no key: " + key+"\nKeys available: "
                    + JSONUtils.getJSONObjectKeys(obj));
            return null;
        }

        Object item = obj.get(key);
        if (item instanceof JSONObject) {
            return obj.getJSONObject(key);
        } else {
            Logger.getLogger(JSON.class).debug("JSONObject: " + obj + " has no JSONObject for key: " + key +
                    "\nType of Key: " + item.getClass().getCanonicalName());

            return null;
        }

    }

    public static JSONArray getJSONArray(JSONObject obj, String key) {
        if ( obj == null ){
            Logger.getLogger(JSON.class).debug("Input object for key: " + key + " is NULL.");
            return null;
        }

        if (obj.isNull(key)) {
            Logger.getLogger(JSON.class).debug("Value for key: " + key + " is NULL\nKeys available: " +
                    JSONUtils.getJSONObjectKeys(obj));
            return null;
        }

        if (!obj.has(key)) {
            Logger.getLogger(JSON.class).debug("JSONObject: " + obj + " has no key: " + key + "\nKeys available: "
                    + JSONUtils.getJSONObjectKeys(obj));
            return null;
        }

        Object item = obj.get(key);
        if ( item instanceof JSONArray ){
            return obj.getJSONArray(key);
        }
        else{
            Logger.getLogger(JSON.class).debug("JSONObject: " + obj + " has no JSONArray for key: " + key +
                    "\nType of Key: "+ item.getClass().getCanonicalName());
            return null;
        }

    }

    public static JSONArray getJSONArrayByIndex(JSONArray array, int index) {

        int len = array.length();
        if ( index < 0 || index > len - 1 ){
            Logger.getLogger(JSON.class).debug("JSONArray: " + array + " has: " + len +
                    " elements. Input index: " + index);
            return null;
        }
        return array.getJSONArray(index);

    }

    public static Boolean getBoolean(JSONObject obj, String key) {

        if (obj.isNull(key)) {
            Logger.getLogger(JSON.class).debug("Value for key: " + key + " is NULL\nKeys available: " +
                    JSONUtils.getJSONObjectKeys(obj));
            return false;
        }

        if (!obj.has(key)) {
            Logger.getLogger(JSON.class).debug("JSONObject: " + obj + " has no key: " + key + "\nKeys available: "
                    + JSONUtils.getJSONObjectKeys(obj));
            return false;
        }

        return obj.getBoolean(key);

    }

}
