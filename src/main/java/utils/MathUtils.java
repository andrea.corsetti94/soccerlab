package utils;

import org.apache.commons.math3.distribution.PoissonDistribution;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathUtils {

    public static double round(double value, int digits){
        if ( value == 0 || Double.isNaN(value) ) return 0;
        return new BigDecimal(value).setScale(digits, RoundingMode.HALF_UP).doubleValue();
    }

    public static double poisson(int numOfEvents, double mean){
        if ( mean <= 0 ) return 0;
        double poissonDistribution = new PoissonDistribution(mean, PoissonDistribution.DEFAULT_EPSILON).probability(numOfEvents);
        return Math.max(0.001, poissonDistribution);
    }

}
