package utils;

import data.db.DB;
import data.db.DBStatements;
import data.db.dao.DAOAlgorithm;
import data.db.dao.DAOBets;
import data.db.retriever.MatchPickRetriever;
import model.Algorithm;
import model.Bet;
import model.MatchPick;

import java.util.*;

/*
TO create new Algorithms and place new bets:

        //CREATE TABLE `AlgorithmV2` ( `id` TEXT NOT NULL COLLATE NOCASE, PRIMARY KEY(`id`) )
        String table = "ALGORITHMV2";
        String names = "";

        AlgoBetsUtils.createNewAlgorithms(table, names);
        AlgoBetsUtils.placeMoreBets(table, 200);
        //delete from AlgorithmV2 where AlgorithmV2.id in (select Algorithm.id from Algorithm);
        //insert into Algorithm select * from AlgorithmV2;
        //DataUpdater.updatePickBetResults();

 */

public class AlgoBetsUtils {

    public static void placeMoreBets(String tableName, int nOfBetsToPlace){
        Map<String, List<String>> mapAlgoFixtures = new HashMap<>();
        DAOBets daoBets = new DAOBets();

        final double lowerBoundOdds = 1.10;
        final double upperBoundOdds = 4.5;
        List<Bet> bets = new ArrayList<>();

        List<MatchPick> matchPickList = new MatchPickRetriever().retrievePicksByOdds(lowerBoundOdds, upperBoundOdds);

        List<Algorithm> algorithms = new DAOAlgorithm().extract("SELECT * FROM "+tableName+";");
        for (Algorithm algorithm : algorithms) {
            int betsToPlace = new Random().nextInt(nOfBetsToPlace + 1) + nOfBetsToPlace;

            List<MatchPick> algoPicks = randomPicks(matchPickList, betsToPlace);
            for (MatchPick pick : algoPicks) {

                if (mapAlgoFixtures.get(algorithm.getIdName()) != null
                        && mapAlgoFixtures.get(algorithm.getIdName()).contains(pick.getFixtureId()))
                    continue;

                Bet bet = new Bet(algorithm.getIdName(), pick.getFixtureId(), pick.getMarketValue(), randomAmount(), pick.getOddValue(), 0);
                bets.add(bet);

                mapAlgoFixtures.computeIfAbsent(algorithm.getIdName(), k -> new ArrayList<>());
                mapAlgoFixtures.get(algorithm.getIdName()).add(pick.getFixtureId());

            }
        }

        daoBets.put(bets);
    }

    public static List<MatchPick> randomPicks(List<MatchPick> matchPickList, int numOfPicks) {
        int listSize = matchPickList.size();
        if (numOfPicks >= listSize) return matchPickList;

        List<MatchPick> chosenPicks = new ArrayList<>();
        int i = 0;
        while (i < numOfPicks) {
            int listIndexToChoose = new Random().nextInt(listSize);
            MatchPick matchPick = matchPickList.get(listIndexToChoose);
            if (!chosenPicks.contains(matchPick)) {
                chosenPicks.add(matchPick);
                i++;
            }
        }

        return chosenPicks;
    }

    public static int randomAmount() {
        //int[] amounts = {20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100};
        int[] amounts = {20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90};
        int size = amounts.length;

        return amounts[new Random().nextInt(size)];
    }


    /**
     * @param tableName, name of the table which will contain new Algorithms. It's suggested to be a temporary table
     * @param names, a string which contains many many names separeted by newLine char
     */
    public static void createNewAlgorithms(String tableName, String names){
        List<Object[]> statementParams = new ArrayList<>();

        List<String> inserted = new ArrayList<>();
        for(String name: names.split("\n")){
            String toInsert = name.trim();

            if ( inserted.contains(toInsert) ) continue;
            inserted.add(toInsert);

            statementParams.add(new Object[]{toInsert});

        }

        DB.executeBatch(new DBStatements("INSERT INTO " + tableName + " VALUES(?);", statementParams));
    }
}
