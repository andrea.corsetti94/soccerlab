package utils;

import data.db.retriever.LeagueDataRetriever;
import main.Logger;
import model.Fixture;
import model.League;
import model.statistics.StatisticsType;

import java.util.*;
import java.util.stream.Collectors;

public class FixturesUtils {

    public static Comparator<Fixture> getDateComparator(){
        return (fixture, t1) -> {
            if ( fixture.getEventDate().before(t1.getEventDate() ) ) return -1;
            if (fixture.getEventDate().after(t1.getEventDate())) return 1;
            return 0;
        };
    }

    public static List<Fixture> completed(List<Fixture> fixtures){
        return fixtures.stream().filter( Fixture::isCompleted ).collect(Collectors.toList());
    }

    public static List<Fixture> filterByHDA(List<Fixture> fixtures, String teamId, StatisticsType type){

        if ( type == StatisticsType.TOTAL_STATISTICS ) return fixtures;

        if ( type == StatisticsType.HOME_STATISTICS ){
            return fixtures.stream().filter( fix -> fix.getHomeTeamId().equals(teamId))
                    .collect(Collectors.toList());
        }

        if ( type == StatisticsType.AWAY_STATISTICS ){
            return fixtures.stream().filter( fix -> fix.getAwayTeamId().equals(teamId))
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    public static int goalScored(List<Fixture> fixtures, String scorerTeamId){
        int goalsScored = 0;
        for(Fixture f: fixtures){
            goalsScored += f.getTeamGoalsFor(scorerTeamId);
        }
        return goalsScored;
    }

    public static int goalReceived(List<Fixture> fixtures, String receiverTeamId) {
        int goalsReceived = 0;
        for (Fixture f : fixtures) {
            goalsReceived += f.getTeamGoalsConceived(receiverTeamId);
        }
        return goalsReceived;
    }

    public static int goalsFromScore(String score, boolean home) {
        try{
            if ( home ){
                return Integer.parseInt(score.split("-")[0].trim());
            }
            else{
                return Integer.parseInt(score.split("-")[1].trim());
            }

        }
        catch(Exception e){
            Logger.getLogger(FixturesUtils.class).warn("Goals from score string: " + score + " not retrieved");
            throw e;
        }
    }

    public static String countryNameByFixture(Fixture fixture){
        String leagueId = fixture.getLeagueId();
        League league = new LeagueDataRetriever().getLeague(leagueId);
        if ( league == null ) return "";
        return league.getCountryName();
    }

    public static String leagueNameByFixture(Fixture fixture){
        try {
            return new LeagueDataRetriever().getLeague(fixture.getLeagueId()).getName();
        }
        catch (Exception e){
            Logger.getLogger(FixturesUtils.class).error("Error retrieving league name by fixture. Fixture: " + fixture, e);
            return "";
        }
    }
}
