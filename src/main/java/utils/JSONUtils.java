package utils;

import json.JSON;
import main.Logger;
import org.json.JSONObject;

import java.util.*;

public class JSONUtils {

    /**
     * This method gets the JSONObject children of a JSONObject taken in input and orders them by an attribute defined on the
     * children objects.
     */
    public static List<JSONObject> orderJSONObjectChildrenByChildAttribute(JSONObject father, String childParameterKey){

        TreeMap<String,JSONObject> orderedChildrenMap = new TreeMap<>();

        for(Object keyOb: father.keySet() ){
            String key = (String) keyOb;
            JSONObject child = JSON.getJSON(father,key);
            if ( child == null ) continue;
            String parameterValue = child.getString(childParameterKey);
            orderedChildrenMap.put(parameterValue,child);
        }

        return new ArrayList<>(orderedChildrenMap.values());
    }

    /**
     * This method gets the JSONObject children of a JSONObject taken in input and orders them by their keys
     */
    public static List<JSONObject> orderJSONObjectChildrenByChildKey(JSONObject father) {

        TreeMap<String, JSONObject> orderedChildrenMap = new TreeMap<>();

        for (Object keyOb : father.keySet()) {
            String key = (String) keyOb;
            JSONObject child = JSON.getJSON(father, key);
            orderedChildrenMap.put(key, child);
        }

        return new ArrayList<>(orderedChildrenMap.values());
    }

    public static JSONObject getChildObjectThatStartsWith(JSONObject father, String begin){
        for (Object keyOb : father.keySet()) {
            String key = (String) keyOb;
            if ( key.startsWith(begin) ){
                return JSON.getJSON(father,key);
            }
        }

        Logger.getLogger(JSONUtils.class).debug("No JSONObject child of: " + father + " starts with: " + begin);
        return null;
    }

    public static String getJSONObjectKeys(JSONObject obj){
        if ( obj == null ) return "";

        String keys = "Keys available for object: " + obj + "\n";

        for(Object ob: obj.keySet()){
            String key = (String) ob;
            keys = keys.concat(key + ", ");
        }

        return keys;
    }

}
