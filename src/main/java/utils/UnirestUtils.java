package utils;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import main.Configs;
import main.Logger;

import java.util.HashMap;
import java.util.Map;

public class UnirestUtils {

    public static HttpResponse<JsonNode> get(String unirestGet, Map<String,String> headers){

        if(headers == null){
            headers = new HashMap<>();
        }

        headers.put("X-RapidAPI-Key", Configs.getApiKey());

        HttpResponse<JsonNode> response;

        Logger.getLogger(UnirestUtils.class).info("API CALL: " + unirestGet);
        try{
            response = Unirest.get(unirestGet)
                    .headers(headers)
                    .asJson();
            return response;
        }
        catch(UnirestException ex){
            Logger.getLogger(FixturesUtils.class).error("Unirest get FAILED. Unirest get String: " + unirestGet,ex);
            return null;
        }
    }
}
