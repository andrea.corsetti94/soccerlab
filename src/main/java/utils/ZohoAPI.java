package utils;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ZohoAPI {


    public static void postXML(){

        try {
            URL url = new URL("https://creator.zoho.com/api/xml/write");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/xml");

            OutputStream os = connection.getOutputStream();

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            FileReader fileReader = new FileReader("C:\\Users\\acorsett\\curl\\xml.xml");
            StreamSource source = new StreamSource(fileReader);
            StreamResult result = new StreamResult(os);
            transformer.transform(source, result);

            os.flush();
            connection.getResponseCode();
            connection.disconnect();
        }
        catch ( Exception e ){
            e.printStackTrace();
        }

    }
}
