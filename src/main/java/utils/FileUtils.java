package utils;

import main.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtils {

    public static Date lastModified(File file, String format){

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String dateString = sdf.format(file.lastModified());

        return DateUtils.formatDate(dateString, format);
    }

    public static void copy(String sourcePath, String destinationPath)  {
        try{
            Files.copy(Paths.get(sourcePath), new FileOutputStream(destinationPath));
            Logger.getLogger(FileUtils.class).info("File: " + sourcePath + " copied to: " + destinationPath);
        }
        catch (IOException e){
            Logger.getLogger(FileUtils.class).error("File: " + sourcePath + " couldnt be copied to: "
            + destinationPath +" due to this error: ", e);

        }
    }

}
