package utils;

import java.util.Arrays;
import java.util.regex.Pattern;

public class DBUtils {

    public static String normalize(String input){
        return input.replaceAll("'","''");
    }


    public static boolean checkPreparedStatementValidity(String preparedStatement, int paramsArrayLen){
        int wildCards = preparedStatement.length() -
                preparedStatement.replaceAll(Pattern.quote("?"), "").length();
        return wildCards == paramsArrayLen;
    }

    public static String evalSQLString(String statement, Object... params){
        return statement + "  " + Arrays.asList(params);
    }
}
