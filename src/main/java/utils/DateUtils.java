package utils;

import main.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static Date formatDate(String inputDate, String format){
        try{
            return new SimpleDateFormat(format).parse(inputDate);
        }
        catch (ParseException ex){
            Logger.getLogger(DateUtils.class).error("Unable to parse String:" + inputDate + " with format: " + format,ex);
        }
        return null;
    }

    public static String toString(Date date, String format){
        return new SimpleDateFormat(format).format(date);
    }

    public static boolean matchesDate(String input, String regex){
        try {
            Date date = new SimpleDateFormat(regex).parse(input);
            if ( date != null ){
                return true;
            }
        } catch (ParseException ex) {
            Logger.getLogger(DateUtils.class).warn("Unable to parse String:" + input + " with format: " + regex, ex);
        }
        return false;
    }

    public static boolean dateDifferenceGreaterThan(Date nextTime, Date previousTime, long comparePeriod){
        return nextTime.getTime() - previousTime.getTime() >= comparePeriod;
    }

    /**
     * It takes a date in input and returns the date representing the input date plus the number of days to advance taken
     * as second input parameter. If daysToAdvance is negative the output date comes before input date.
     * @param inputDate
     * @param daysToAdvance
     * @return
     */
    public static Date advanceDays(Date inputDate, int daysToAdvance){
        Calendar cal = Calendar.getInstance();
        cal.setTime(inputDate);
        cal.add(Calendar.DATE, daysToAdvance);
        return cal.getTime();
    }


    /**
     * This method formats a fixture event date in format: (example) 2019-02-12T20:00:00+00:00 to a Date object
     * @param time
     * @return
     */
    private static Date formatJSONEventDate(String time){
        int indexOf = time.indexOf("+");
        if (indexOf != -1) {
            time = time.substring(0, indexOf);
        }

        time = time.replace("T"," ");
        return DateUtils.formatDate(time, "yyyy-MM-dd HH:mm:ss");

    }

    public static Date formatFixtureDate(String inputString){
        String jsonDateFormat = ".*T.*\\+.*"; //2019-02-12T20:00:00+00:00
        String dbDateFormat = "[^T]*"; //yyyy-MM-dd HH:mm
        if ( inputString.matches(jsonDateFormat) ){
            return formatJSONEventDate(inputString);
        }
        else if( inputString.matches(dbDateFormat ) ){
            return DateUtils.formatDate(inputString, "yyyy-MM-dd HH:mm");
        }
        else{
            Logger.getLogger(DateUtils.class).error(" String : " + inputString + " doesnt match " +
                    " this format: " +jsonDateFormat + " nor " + dbDateFormat + " NULL date returned.");
            return null;
        }
    }

}
