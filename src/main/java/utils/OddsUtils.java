package utils;

import odds.markets.AbstractMarketOdds;
import odds.values.OddValue;

import java.util.*;

public class OddsUtils {

    /**
     * Returns the element at index <index> in an array of oddValues ordered by asc.
     * If index < 0 or > values.length null is returned
     * @param index index of the element to be returned in the ordered list. If == 0, the smallest odd value is returned.
     * @param values list of odds values
     * @return
     */
    public static OddValue getOddsValueAtIndex(int index, List<OddValue> values){
        if ( values == null || values.size() == 0) return null;

        values.sort(Comparator.comparingDouble(OddValue::getValue));

        if ( index < 0 || index > values.size() - 1 ) return null;
        return values.get(index);
    }

    /**
     * Takes a double in input and returns this double + a random offset that goes from 0 to 0.04;
     * @param lowLimit
     * @return
     */
    public static double randomMargin(double lowLimit){

        int integerOffset = new Random().nextInt(4);
        double offsetToCents = (double) integerOffset / 100;

        return lowLimit + offsetToCents;
    }


    private static double[] oddsValues(List<OddValue> oddValues){
        if ( oddValues == null || oddValues.size() == 0 ) return new double[]{};

        double[] values = new double[oddValues.size()];
        for(int i = 0; i < oddValues.size(); i++){
            values[i] = oddValues.get(i).getValue();
        }

        return values;
    }


    /**
     * Compute book margin from an input odds.
     * Returns 0 if argument is not legit.
     * @param odds
     * @return
     */
    public static double bookMargin(AbstractMarketOdds odds){
        if ( odds == null || odds.getValues() == null || odds.getValues().isEmpty() ) return 0;

        List<OddValue> values = odds.getValues();
        double[] oddsValues = oddsValues(values);

        double margin = 0.0;
        for (double d : oddsValues) {
            margin += 1 / d;
        }
        return margin;
    }

    public static double chanceToOdds(double chance){
        final double marginOffset = 0.92; //quote downshifting

        return Math.max(1.01, MathUtils.round( 1/chance * marginOffset, 2));
    }

    public static double oddsToChance(double odds){
        final double marginOffset = 1.08; //quote downshifting

        return MathUtils.round(1/odds * marginOffset, 2);
    }

    public static double expectedValue(double odds, double chance){
        if ( chance == 0 ) return 0;
        return MathUtils.round((chance/100) - (1/odds), 2);
    }
}
