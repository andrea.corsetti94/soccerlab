package odds.values;

import main.Logger;
import odds.Markets;
import utils.MathUtils;

public class OddValue {

    private double value;
    private final Markets.MarketValue marketValue;

    public OddValue(String value, Markets.MarketValue marketValue) {
        this.marketValue = marketValue;
        try {
            this.setValue(Double.parseDouble(value));
        }
        catch(Exception ex){
            Logger.getLogger(OddValue.class).error("Exception while parsing String value: " + value,ex);
            this.value = 0;
        }
    }

    public void setValue(double value){
        this.value = MathUtils.round(value, 2);
    }

    public double getValue() {
        return value;
    }

    public Markets.MarketValue getMarketValue() {
        return marketValue;
    }

    @Override
    public String toString() {
        return "{" +
                "value=" + value +
                '}';
    }
}
