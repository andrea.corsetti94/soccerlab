package odds.values;

import odds.Markets;

class EmptyOddValue extends OddValue {

    public EmptyOddValue() {
        super("0", Markets.MarketValue.EMPTY_MARKET_VALUE);
    }

}
