package odds;

import main.Configs;
import odds.markets.AbstractMarketOdds;
import odds.markets.EmptyMarketOdds;

import java.util.Arrays;
import java.util.List;

public class MatchOdds {

    private final String fixtureId;

    private final AbstractMarketOdds hdaOdds;
    private final AbstractMarketOdds bttsHtOdds;
    private final AbstractMarketOdds bttsOdds;
    private final AbstractMarketOdds dcHTOdds;
    private final AbstractMarketOdds dcOdds;
    private final AbstractMarketOdds firstGoalOdds;
    private final AbstractMarketOdds hdaUnderOverOdds;
    private final AbstractMarketOdds htFtOdds;
    private final AbstractMarketOdds htOdds;
    private final AbstractMarketOdds underOver1_5Odds;
    private final AbstractMarketOdds underOver2_5Odds;
    private final AbstractMarketOdds underOver3_5Odds;
    private final AbstractMarketOdds underOver4_5Odds;
    private final AbstractMarketOdds exactScoreOdds;
    private final AbstractMarketOdds exactScoreHTOdds;
    private final AbstractMarketOdds hdaBttsOdds;

    private List<AbstractMarketOdds> oddsList;

    public MatchOdds(String fixtureId, AbstractMarketOdds hdaOdds, AbstractMarketOdds bttsHtOdds, AbstractMarketOdds bttsOdds, AbstractMarketOdds dcHTOdds,
                     AbstractMarketOdds dcOdds, AbstractMarketOdds firstGoalOdds, AbstractMarketOdds hdaUnderOverOdds, AbstractMarketOdds htFtOdds,
                     AbstractMarketOdds htOdds, AbstractMarketOdds underOver1_5Odds, AbstractMarketOdds underOver2_5Odds, AbstractMarketOdds underOver3_5Odds,
                     AbstractMarketOdds underOver4_5Odds, AbstractMarketOdds exactScoreOdds, AbstractMarketOdds exactScoreHTOdds, AbstractMarketOdds hdaBttsOdds) {

        this.fixtureId = fixtureId;
        this.hdaOdds = hdaOdds;
        this.bttsHtOdds = bttsHtOdds;
        this.bttsOdds = bttsOdds;
        this.dcHTOdds = dcHTOdds;
        this.dcOdds = dcOdds;
        this.firstGoalOdds = firstGoalOdds;
        this.hdaUnderOverOdds = hdaUnderOverOdds;
        this.htFtOdds = htFtOdds;
        this.htOdds = htOdds;
        this.underOver1_5Odds = underOver1_5Odds;
        this.underOver2_5Odds = underOver2_5Odds;
        this.underOver3_5Odds = underOver3_5Odds;
        this.underOver4_5Odds = underOver4_5Odds;
        this.exactScoreOdds = exactScoreOdds;
        this.exactScoreHTOdds = exactScoreHTOdds;
        this.hdaBttsOdds = hdaBttsOdds;

        balanceIfNeeded();

        oddsList = Arrays.asList(hdaOdds, bttsHtOdds, bttsOdds, dcHTOdds, dcOdds, firstGoalOdds, hdaUnderOverOdds, htFtOdds,
                htOdds, underOver1_5Odds, underOver2_5Odds, underOver3_5Odds, underOver4_5Odds, exactScoreOdds,
                exactScoreHTOdds, hdaBttsOdds);
    }

    private void balanceIfNeeded(){

        if ( Configs.isOddsBalancerActive() ){
            OddsBalancer.balance(this.hdaOdds);
            OddsBalancer.balance(this.bttsHtOdds);
            OddsBalancer.balance(this.bttsOdds);
            OddsBalancer.balance(this.dcHTOdds);
            OddsBalancer.balance(this.dcOdds);
            OddsBalancer.balance(this.firstGoalOdds);
            OddsBalancer.balance(this.hdaUnderOverOdds);
            OddsBalancer.balance(this.htFtOdds);
            OddsBalancer.balance(this.htOdds);
            OddsBalancer.balance(this.underOver1_5Odds);
            OddsBalancer.balance(this.underOver2_5Odds);
            OddsBalancer.balance(this.underOver3_5Odds);
            OddsBalancer.balance(this.underOver4_5Odds);
            OddsBalancer.balance(this.exactScoreOdds);
            OddsBalancer.balance(this.exactScoreHTOdds);
            OddsBalancer.balance(this.hdaBttsOdds);
        }
    }

    public MatchOdds(){
        this.fixtureId = "";
        this.hdaOdds = new EmptyMarketOdds();
        this.bttsHtOdds = new EmptyMarketOdds();
        this.bttsOdds = new EmptyMarketOdds();
        this.dcHTOdds = new EmptyMarketOdds();
        this.dcOdds = new EmptyMarketOdds();
        this.firstGoalOdds = new EmptyMarketOdds();
        this.hdaUnderOverOdds = new EmptyMarketOdds();
        this.htFtOdds = new EmptyMarketOdds();
        this.htOdds = new EmptyMarketOdds();
        this.underOver1_5Odds = new EmptyMarketOdds();
        this.underOver2_5Odds = new EmptyMarketOdds();
        this.underOver3_5Odds = new EmptyMarketOdds();
        this.underOver4_5Odds = new EmptyMarketOdds();
        this.exactScoreOdds = new EmptyMarketOdds();
        this.exactScoreHTOdds = new EmptyMarketOdds();
        this.hdaBttsOdds = new EmptyMarketOdds();
    }

    public String getFixtureId() {
        return fixtureId;
    }

    public AbstractMarketOdds getHdaOdds() {
        return hdaOdds;
    }

    public AbstractMarketOdds getBttsHtOdds() {
        return bttsHtOdds;
    }

    public AbstractMarketOdds getBttsOdds() {
        return bttsOdds;
    }

    public AbstractMarketOdds getDcHTOdds() {
        return dcHTOdds;
    }

    public AbstractMarketOdds getDcOdds() {
        return dcOdds;
    }

    public AbstractMarketOdds getFirstGoalOdds() {
        return firstGoalOdds;
    }

    public AbstractMarketOdds getHdaUnderOverOdds() {
        return hdaUnderOverOdds;
    }

    public AbstractMarketOdds getHtFtOdds() {
        return htFtOdds;
    }

    public AbstractMarketOdds getHtOdds() {
        return htOdds;
    }

    public AbstractMarketOdds getUnderOver1_5Odds() {
        return underOver1_5Odds;
    }

    public AbstractMarketOdds getUnderOver2_5Odds() {
        return underOver2_5Odds;
    }

    public AbstractMarketOdds getUnderOver3_5Odds() {
        return underOver3_5Odds;
    }

    public AbstractMarketOdds getUnderOver4_5Odds() {
        return underOver4_5Odds;
    }

    public AbstractMarketOdds getExactScoreOdds() {
        return exactScoreOdds;
    }

    public AbstractMarketOdds getExactScoreHTOdds() {
        return exactScoreHTOdds;
    }

    public AbstractMarketOdds getHdaBttsOdds() {
        return hdaBttsOdds;
    }

    public List<AbstractMarketOdds> getOddsList(){
        return oddsList;

    }
    @Override
    public String toString() {
        return "Match Odds {" +
                "fixtureId='" + fixtureId + '\'' +
                "\nhdaOdds=" + hdaOdds +
                "\nbttsHtOdds=" + bttsHtOdds +
                "\nbttsOdds=" + bttsOdds +
                "\ndcHTOdds=" + dcHTOdds +
                "\ndcOdds=" + dcOdds +
                "\nfirstGoalOdds=" + firstGoalOdds +
                "\nhdaUnderOverOdds=" + hdaUnderOverOdds +
                "\nhtFtOdds=" + htFtOdds +
                "\nhtOdds=" + htOdds +
                "\nunderOver1_5Odds=" + underOver1_5Odds +
                "\nunderOver2_5Odds=" + underOver2_5Odds +
                "\nunderOver3_5Odds=" + underOver3_5Odds +
                "\nunderOver4_5Odds=" + underOver4_5Odds +
                "\nexactScoreOdds=" + exactScoreOdds +
                "\nexactScoreHTOdds=" + exactScoreHTOdds +
                "\nhdaBttsOdds=" + hdaBttsOdds +
                '}';
    }

    public boolean isLegit(){
        if ( fixtureId == null || fixtureId.isEmpty() ) return false; //if it's completely empty
        for( AbstractMarketOdds odds: oddsList ){
            if ( odds.isLegit() ) return true;
        }
        return false;
    }
}
