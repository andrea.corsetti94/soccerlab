package odds;

import json.JSON;
import odds.markets.*;
import odds.values.*;
import org.json.JSONObject;
import utils.JSONUtils;

import java.util.List;

public class OddsBuilder {

    public static AbstractMarketOdds buildHDAOdds(JSONObject winTheMatchObj){
        if ( winTheMatchObj == null ) return new EmptyMarketOdds();

        String winTheMatchHome = JSON.getString(JSON.getJSON(winTheMatchObj, "1"), "odd");
        String winTheMatchDraw = JSON.getString(JSON.getJSON(winTheMatchObj, "N"), "odd");
        String winTheMatchAway = JSON.getString(JSON.getJSON(winTheMatchObj, "2"), "odd");

        return new HDAOdds(
                new OddValue(winTheMatchHome, Markets.MarketValue.HDA_HOME),
                new OddValue(winTheMatchDraw, Markets.MarketValue.HDA_DRAW),
                new OddValue(winTheMatchAway, Markets.MarketValue.HDA_AWAY)
        );
    }

    public static AbstractMarketOdds buildHTOdds(JSONObject htOddsOb) {
        if (htOddsOb == null) return new EmptyMarketOdds();

        String htHome = JSON.getString(JSON.getJSON(htOddsOb, "1"), "odd");
        String htDraw = JSON.getString(JSON.getJSON(htOddsOb, "N"), "odd");
        String htAway = JSON.getString(JSON.getJSON(htOddsOb, "2"), "odd");

        return new HTOdds(
                new OddValue(htHome, Markets.MarketValue.HT_HOME),
                new OddValue(htDraw, Markets.MarketValue.HT_DRAW),
                new OddValue(htAway, Markets.MarketValue.HT_AWAY)
        );
    }

    public static AbstractMarketOdds buildUnderOver4_5Odds(JSONObject OU4_5ob) {
        if (OU4_5ob == null) return new EmptyMarketOdds();

        JSONObject overOb = JSONUtils.getChildObjectThatStartsWith(OU4_5ob, "Over");
        JSONObject underOb = JSONUtils.getChildObjectThatStartsWith(OU4_5ob, "Under");

        String over = JSON.getString(overOb, "odd");
        String under = JSON.getString(underOb, "odd");

        return new UnderOver4_5Odds(
                new OddValue(over, Markets.MarketValue.O4_5),
                new OddValue(under, Markets.MarketValue.U4_5)
        );
    }

    public static AbstractMarketOdds buildUnderOver3_5Odds(JSONObject OU3_5ob) {
        if (OU3_5ob == null) return new EmptyMarketOdds();

        JSONObject overOb = JSONUtils.getChildObjectThatStartsWith(OU3_5ob,"Over");
        JSONObject underOb = JSONUtils.getChildObjectThatStartsWith(OU3_5ob, "Under");

        String over = JSON.getString(overOb, "odd");
        String under = JSON.getString(underOb, "odd");

        return new UnderOver3_5Odds(
                new OddValue(over, Markets.MarketValue.O3_5),
                new OddValue(under, Markets.MarketValue.U3_5)
        );
    }

    public static AbstractMarketOdds buildUnderOver2_5Odds(JSONObject OU2_5ob) {
        if (OU2_5ob == null) return new EmptyMarketOdds();

        JSONObject overOb = JSONUtils.getChildObjectThatStartsWith(OU2_5ob, "Over");
        JSONObject underOb = JSONUtils.getChildObjectThatStartsWith(OU2_5ob, "Under");

        String over = JSON.getString(overOb, "odd");
        String under = JSON.getString(underOb, "odd");

        return new UnderOver2_5Odds(
                new OddValue(over, Markets.MarketValue.O2_5),
                new OddValue(under, Markets.MarketValue.U2_5)
        );
    }

    public static AbstractMarketOdds buildFirstGoalOdds(JSONObject firstGoalOb) {
        if (firstGoalOb == null) return new EmptyMarketOdds();

        List<JSONObject> children = JSONUtils.orderJSONObjectChildrenByChildAttribute(firstGoalOb,"pos");

        if (children.size() != 3) {
            System.out.println("TODO: DA investigare anche quiiii!!!!!!!!!!!!!!!");
            return new EmptyMarketOdds();
        }

        String home = JSON.getString(children.get(0), "odd");
        String away = JSON.getString(children.get(1), "odd");
        String noGoal = JSON.getString(children.get(2), "odd");

        return new FirstGoalOdds(
                new OddValue(home, Markets.MarketValue.FG_HOME),
                new OddValue(away, Markets.MarketValue.FG_AWAY),
                new OddValue(noGoal, Markets.MarketValue.FG_NO_GOAL)
        );
    }

    public static AbstractMarketOdds buildHT_FTOdds(JSONObject htFtOb) {
        if (htFtOb == null) return new EmptyMarketOdds();

        String homeHome = JSON.getString(JSON.getJSON(htFtOb, "1/1"), "odd");
        String homeDraw = JSON.getString(JSON.getJSON(htFtOb, "1/N"), "odd");
        String homeAway = JSON.getString(JSON.getJSON(htFtOb, "1/2"), "odd");
        String drawHome = JSON.getString(JSON.getJSON(htFtOb, "N/1"), "odd");
        String drawDraw = JSON.getString(JSON.getJSON(htFtOb, "N/N"), "odd");
        String drawAway = JSON.getString(JSON.getJSON(htFtOb, "N/2"), "odd");
        String awayHome = JSON.getString(JSON.getJSON(htFtOb, "2/1"), "odd");
        String awayAway = JSON.getString(JSON.getJSON(htFtOb, "2/2"), "odd");
        String awayDraw = JSON.getString(JSON.getJSON(htFtOb, "2/N"), "odd");


        return new HT_FTOdds(
                new OddValue(homeHome, Markets.MarketValue.HT_FT_HOME_HOME),
                new OddValue(homeAway, Markets.MarketValue.HT_FT_HOME_AWAY),
                new OddValue(homeDraw, Markets.MarketValue.HT_FT_HOME_DRAW),
                new OddValue(drawAway, Markets.MarketValue.HT_FT_DRAW_AWAY),
                new OddValue(drawDraw, Markets.MarketValue.HT_FT_DRAW_DRAW),
                new OddValue(drawHome, Markets.MarketValue.HT_FT_DRAW_HOME),
                new OddValue(awayAway, Markets.MarketValue.HT_FT_AWAY_AWAY),
                new OddValue(awayDraw, Markets.MarketValue.HT_FT_AWAY_DRAW),
                new OddValue(awayHome, Markets.MarketValue.HT_FT_AWAY_HOME)
        );
    }

    public static AbstractMarketOdds buildUnderOver1_5Odds(JSONObject OU1_5ob) {
        if (OU1_5ob == null) return new EmptyMarketOdds();

        JSONObject overOb = JSONUtils.getChildObjectThatStartsWith(OU1_5ob, "Over");
        JSONObject underOb = JSONUtils.getChildObjectThatStartsWith(OU1_5ob, "Under");

        String over = JSON.getString(overOb, "odd");
        String under = JSON.getString(underOb, "odd");

        return new UnderOver1_5Odds(
                new OddValue(over, Markets.MarketValue.O1_5),
                new OddValue(under, Markets.MarketValue.U1_5)
        );
    }

    public static AbstractMarketOdds buildBTTSOdds(JSONObject bttsOb) {
        if (bttsOb == null) return new EmptyMarketOdds();

        String yes = JSON.getString(JSON.getJSON(bttsOb,"Yes"), "odd");
        String no = JSON.getString(JSON.getJSON(bttsOb, "No"), "odd");

        return new BTTSOdds(
                new OddValue(yes, Markets.MarketValue.BTTS_YES),
                new OddValue(no, Markets.MarketValue.BTTS_NO)
        );
    }

    public static AbstractMarketOdds buildBTTS_HTOdds(JSONObject bttsOb) {
        if (bttsOb == null) return new EmptyMarketOdds();

        String yes = JSON.getString(JSON.getJSON(bttsOb, "Yes"), "odd");
        String no = JSON.getString(JSON.getJSON(bttsOb, "No"), "odd");

        return new BTTS_HTOdds(
                new OddValue(yes, Markets.MarketValue.BTTS_HT_YES),
                new OddValue(no, Markets.MarketValue.BTTS_HT_NO)
        );
    }

    public static AbstractMarketOdds buildDCOdds(JSONObject doubleChanceObj) {
        if (doubleChanceObj == null) return new EmptyMarketOdds();

        String homeDraw = JSON.getString(JSON.getJSON(doubleChanceObj,"1/N"), "odd");
        String drawAway = JSON.getString(JSON.getJSON(doubleChanceObj, "N/2"), "odd");
        String homeAway = JSON.getString(JSON.getJSON(doubleChanceObj, "1/2"), "odd");

        return new DCOdds(
                new OddValue(homeDraw, Markets.MarketValue.DC_HOME_DRAW),
                new OddValue(drawAway, Markets.MarketValue.DC_DRAW_AWAY),
                new OddValue(homeAway, Markets.MarketValue.DC_HOME_AWAY)
        );
    }

    public static AbstractMarketOdds buildDC_HTOdds(JSONObject doubleChanceObj) {
        if (doubleChanceObj == null) return new EmptyMarketOdds();

        String homeDraw = JSON.getString(JSON.getJSON(doubleChanceObj, "1/N"), "odd");
        String drawAway = JSON.getString(JSON.getJSON(doubleChanceObj, "N/2"), "odd");
        String homeAway = JSON.getString(JSON.getJSON(doubleChanceObj, "1/2"), "odd");

        return new DC_HTOdds(
                new OddValue(homeDraw, Markets.MarketValue.DC_HT_HOME_DRAW),
                new OddValue(drawAway, Markets.MarketValue.DC_HT_DRAW_AWAY),
                new OddValue(homeAway, Markets.MarketValue.DC_HT_HOME_AWAY)
        );
    }

    public static AbstractMarketOdds buildHDABTTSOdds(JSONObject hdaBTTSObj) {
        if (hdaBTTSObj == null) return new EmptyMarketOdds();

        List<JSONObject> orderedHDA_BTTSOddsValues = JSONUtils.orderJSONObjectChildrenByChildAttribute(hdaBTTSObj, "pos");

        if (orderedHDA_BTTSOddsValues.size() != 6){
            System.out.println("TODO: DA investigare!!!!!!!!!!!!!!!");
            return new EmptyMarketOdds();
        }

        JSONObject homeYesOb = orderedHDA_BTTSOddsValues.get(0);
        JSONObject homeNoOb = orderedHDA_BTTSOddsValues.get(1);
        JSONObject drawYesOb = orderedHDA_BTTSOddsValues.get(2);
        JSONObject drawNoOb = orderedHDA_BTTSOddsValues.get(3);
        JSONObject awayYesOb = orderedHDA_BTTSOddsValues.get(4);
        JSONObject awayNoOb = orderedHDA_BTTSOddsValues.get(5);

        String homeYes = JSON.getString(homeYesOb,"odd");
        String homeNo = JSON.getString(homeNoOb, "odd");
        String drawYes = JSON.getString(drawYesOb, "odd");
        String drawNo = JSON.getString(drawNoOb, "odd");
        String awayYes = JSON.getString(awayYesOb, "odd");
        String awayNo = JSON.getString(awayNoOb, "odd");


        return new HDA_BTTSOdds(
                new OddValue(homeYes, Markets.MarketValue.HDA_BTTS_HOME_YES),
                new OddValue(homeNo, Markets.MarketValue.HDA_BTTS_HOME_NO),
                new OddValue(drawYes, Markets.MarketValue.HDA_BTTS_DRAW_YES),
                new OddValue(drawNo, Markets.MarketValue.HDA_BTTS_DRAW_NO),
                new OddValue(awayYes, Markets.MarketValue.HDA_BTTS_AWAY_YES),
                new OddValue(awayNo, Markets.MarketValue.HDA_BTTS_AWAY_NO)
        );
    }

    public static AbstractMarketOdds buildExactScoreOdds(JSONObject exactScoreObj) {
        if (exactScoreObj == null) return new EmptyMarketOdds();

        String nilNil = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 0"), "odd");
        String nilOne = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 1"), "odd");
        String nilTwo = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 2"), "odd");
        String nilThree = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 3"), "odd");
        String nilFour = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 4"), "odd");
        String oneNil = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 0"), "odd");
        String twoNil = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 0"), "odd");
        String threeNil = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 0"), "odd");
        String fourNil = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 0"), "odd");
        String oneOne = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 1"), "odd");
        String oneTwo = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 2"), "odd");
        String oneThree = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 3"), "odd");
        String oneFour = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 4"), "odd");
        String twoOne = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 1"), "odd");
        String twoTwo = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 2"), "odd");
        String twoThree = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 3"), "odd");
        String twoFour = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 4"), "odd");
        String threeOne = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 1"), "odd");
        String threeTwo = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 2"), "odd");
        String threeThree = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 3"), "odd");
        String threeFour = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 4"), "odd");
        String fourOne = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 1"), "odd");
        String fourTwo = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 2"), "odd");
        String fourThree = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 3"), "odd");
        String fourFour = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 4"), "odd");
        String other = JSON.getString(JSON.getJSON(exactScoreObj, "Other"), "odd");

        return new ExactScoreOdds(
                new OddValue(nilNil, Markets.MarketValue.NIL_NIL),
                new OddValue(nilOne, Markets.MarketValue.NIL_ONE),
                new OddValue(nilTwo, Markets.MarketValue.NIL_TWO),
                new OddValue(nilThree, Markets.MarketValue.NIL_THREE),
                new OddValue(nilFour, Markets.MarketValue.NIL_FOUR),
                new OddValue(oneNil, Markets.MarketValue.ONE_NIL),
                new OddValue(twoNil, Markets.MarketValue.TWO_NIL),
                new OddValue(threeNil, Markets.MarketValue.THREE_NIL),
                new OddValue(fourNil, Markets.MarketValue.FOUR_NIL),
                new OddValue(oneOne, Markets.MarketValue.ONE_ONE),
                new OddValue(oneTwo, Markets.MarketValue.ONE_TWO),
                new OddValue(oneThree, Markets.MarketValue.ONE_THREE),
                new OddValue(oneFour, Markets.MarketValue.ONE_FOUR),
                new OddValue(twoOne, Markets.MarketValue.TWO_ONE),
                new OddValue(twoTwo, Markets.MarketValue.TWO_TWO),
                new OddValue(twoThree, Markets.MarketValue.TWO_THREE),
                new OddValue(twoFour, Markets.MarketValue.TWO_FOUR),
                new OddValue(threeOne, Markets.MarketValue.THREE_ONE),
                new OddValue(threeTwo, Markets.MarketValue.THREE_TWO),
                new OddValue(threeThree, Markets.MarketValue.THREE_THREE),
                new OddValue(threeFour, Markets.MarketValue.THREE_FOUR),
                new OddValue(fourOne, Markets.MarketValue.FOUR_ONE),
                new OddValue(fourTwo, Markets.MarketValue.FOUR_TWO),
                new OddValue(fourThree, Markets.MarketValue.FOUR_THREE),
                new OddValue(fourFour, Markets.MarketValue.FOUR_FOUR),
                new OddValue(other, Markets.MarketValue.OTHER)
        );
    }

    public static AbstractMarketOdds buildExactScoreHTOdds(JSONObject exactScoreObj) {
        if (exactScoreObj == null) return new EmptyMarketOdds();

        String nilNil = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 0"), "odd");
        String nilOne = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 1"), "odd");
        String nilTwo = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 2"), "odd");
        String nilThree = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 3"), "odd");
        String nilFour = JSON.getString(JSON.getJSON(exactScoreObj, "0 - 4"), "odd");
        String oneNil = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 0"), "odd");
        String twoNil = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 0"), "odd");
        String threeNil = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 0"), "odd");
        String fourNil = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 0"), "odd");
        String oneOne = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 1"), "odd");
        String oneTwo = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 2"), "odd");
        String oneThree = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 3"), "odd");
        String oneFour = JSON.getString(JSON.getJSON(exactScoreObj, "1 - 4"), "odd");
        String twoOne = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 1"), "odd");
        String twoTwo = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 2"), "odd");
        String twoThree = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 3"), "odd");
        String twoFour = JSON.getString(JSON.getJSON(exactScoreObj, "2 - 4"), "odd");
        String threeOne = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 1"), "odd");
        String threeTwo = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 2"), "odd");
        String threeThree = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 3"), "odd");
        String threeFour = JSON.getString(JSON.getJSON(exactScoreObj, "3 - 4"), "odd");
        String fourOne = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 1"), "odd");
        String fourTwo = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 2"), "odd");
        String fourThree = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 3"), "odd");
        String fourFour = JSON.getString(JSON.getJSON(exactScoreObj, "4 - 4"), "odd");
        String other = JSON.getString(JSON.getJSON(exactScoreObj, "Other"), "odd");

        return new ExactScoreHTOdds(
                new OddValue(nilNil, Markets.MarketValue.NIL_NIL_HT),
                new OddValue(nilOne, Markets.MarketValue.NIL_ONE_HT),
                new OddValue(nilTwo, Markets.MarketValue.NIL_TWO_HT),
                new OddValue(nilThree, Markets.MarketValue.NIL_THREE_HT),
                new OddValue(nilFour, Markets.MarketValue.NIL_FOUR_HT),
                new OddValue(oneNil, Markets.MarketValue.ONE_NIL_HT),
                new OddValue(twoNil, Markets.MarketValue.TWO_NIL_HT),
                new OddValue(threeNil, Markets.MarketValue.THREE_NIL_HT),
                new OddValue(fourNil, Markets.MarketValue.FOUR_NIL_HT),
                new OddValue(oneOne, Markets.MarketValue.ONE_ONE_HT),
                new OddValue(oneTwo, Markets.MarketValue.ONE_TWO_HT),
                new OddValue(oneThree, Markets.MarketValue.ONE_THREE_HT),
                new OddValue(oneFour, Markets.MarketValue.ONE_FOUR_HT),
                new OddValue(twoOne, Markets.MarketValue.TWO_ONE_HT),
                new OddValue(twoTwo, Markets.MarketValue.TWO_TWO_HT),
                new OddValue(twoThree, Markets.MarketValue.TWO_THREE_HT),
                new OddValue(twoFour, Markets.MarketValue.TWO_FOUR_HT),
                new OddValue(threeOne, Markets.MarketValue.THREE_ONE_HT),
                new OddValue(threeTwo, Markets.MarketValue.THREE_TWO_HT),
                new OddValue(threeThree, Markets.MarketValue.THREE_THREE_HT),
                new OddValue(threeFour, Markets.MarketValue.THREE_FOUR_HT),
                new OddValue(fourOne, Markets.MarketValue.FOUR_ONE_HT),
                new OddValue(fourTwo, Markets.MarketValue.FOUR_TWO_HT),
                new OddValue(fourThree, Markets.MarketValue.FOUR_THREE_HT),
                new OddValue(fourFour, Markets.MarketValue.FOUR_FOUR_HT),
                new OddValue(other, Markets.MarketValue.OTHER_HT)
        );
    }
}
