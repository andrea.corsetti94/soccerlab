package odds;

import odds.markets.AbstractMarketOdds;
import odds.values.OddValue;
import utils.OddsUtils;

import java.util.List;

class OddsBalancer {

    /**
     * Under this margin the balancer will not change odds
     */
    private static final double BOOK_MARGIN_LOW_LIMIT = 1.08;

    /**
     * Odds with book margin < BOOK_MARGIN_LOW_LIMIT are not changed.
     * @param odds
     */
    public static void balance(AbstractMarketOdds odds){
        if ( odds == null || odds.getValues() == null || odds.getValues().isEmpty() ) return;

        double margin = OddsUtils.bookMargin(odds);
        if ( margin < BOOK_MARGIN_LOW_LIMIT ) return;

        List<OddValue> oddsValues = odds.getValues();

        OddValue smallestOdd;
        OddValue highestOdd;
        OddValue mediumOdd;
        double newLowOddValue;
        double newHighOddValue;
        double newMeidumOddValue;

        switch ( oddsValues.size() ){
            case 2:
                smallestOdd = OddsUtils.getOddsValueAtIndex(0, oddsValues);
                newLowOddValue = smallestOdd.getValue() * 1.04;
                smallestOdd.setValue(newLowOddValue);

                highestOdd = OddsUtils.getOddsValueAtIndex(1, oddsValues);
                newHighOddValue = Math.pow(OddsUtils.randomMargin(1.07) - (1 / newLowOddValue ), -1);
                highestOdd.setValue(newHighOddValue);
                break;
            case 3:
                smallestOdd = OddsUtils.getOddsValueAtIndex(0, oddsValues);
                newLowOddValue = smallestOdd.getValue() * 1.03;
                smallestOdd.setValue(newLowOddValue);

                mediumOdd = OddsUtils.getOddsValueAtIndex(1, oddsValues);
                newMeidumOddValue = mediumOdd.getValue() * 1.08;
                mediumOdd.setValue(newMeidumOddValue);

                highestOdd = OddsUtils.getOddsValueAtIndex(2, oddsValues);
                newHighOddValue = Math.pow(OddsUtils.randomMargin(1.07) - (1/newLowOddValue) - (1/newMeidumOddValue), -1);
                highestOdd.setValue(newHighOddValue);
                break;
            default:
                break;
        }

    }
}
