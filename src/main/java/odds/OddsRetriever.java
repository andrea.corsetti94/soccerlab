package odds;

import data.API;
import main.Logger;
import model.Fixture;
import odds.markets.AbstractMarketOdds;
import odds.values.OddValue;

import java.util.List;

public class OddsRetriever {

    public double retrieveFixtureOddsForMarket(Fixture fixture, Markets.MarketValue marketValue) {

        String fixtureId = fixture.getId();
        MatchOdds matchOdds = new API().getOddsForFixture(fixtureId);

        return fecthFixtureOdds(fixture, matchOdds, marketValue);
    }

    public double fecthFixtureOdds(Fixture fixture, MatchOdds matchOdds, Markets.MarketValue marketValue){
        String fixtureId = fixture.getId();
        Markets.MarketType marketType = marketValue.getType();

        if (!matchOdds.isLegit()) {
            Logger.getLogger(this.getClass()).warn("Odds found for fixture id: " + fixtureId + " are not legit! ");
            return 0;
        }

        for (AbstractMarketOdds odds : matchOdds.getOddsList()) {
            if (odds.getMarketType() == marketType) {
                List<OddValue> oddsValues = odds.getValues();

                for (OddValue value : oddsValues) {
                    if (value.getMarketValue() == marketValue) {
                        Logger.getLogger(this.getClass()).debug("Odds for fixture id: " + fixtureId + " and Market: " +
                                marketValue.toString() + " : " + value.getValue());
                        return value.getValue();
                    }
                }
            }
        }

        Logger.getLogger(this.getClass()).debug("No Odds found for fixture id: " + fixtureId + " Market: " +
                marketValue.toString() );
        return 0;
    }

}
