package odds;

import utils.MathUtils;

public class MarketChance {

    private final Markets.MarketValue marketValue;
    private final double chance;

    public MarketChance(Markets.MarketValue marketValue, double chance) {
        this.marketValue = marketValue;
        this.chance = chance;
    }

    public Markets.MarketValue getMarketValue() {
        return marketValue;
    }

    public double getChance() {
        return chance;
    }

    public double getChanceBy100(){
        return MathUtils.round(chance * 100, 1);
    }

    public String getChanceRepresentation(){
        double chance = getChanceBy100();
        if ( chance < 1 ){
            return " < 1 %";
        }
        if ( chance > 99 ){
            return " > 99 %";
        }
        return chance + " %";
    }

    @Override
    public String toString() {
        return "MarketChance{" +
                "marketValue=" + marketValue +
                ", chance=" + this.getChanceRepresentation() +
                '}';
    }
}
