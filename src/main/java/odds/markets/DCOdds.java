package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class DCOdds extends AbstractMarketOdds {

    private final OddValue homeDrawOdds;
    private final OddValue drawAwayOdds;
    private final OddValue homeAwayOdds;

    public DCOdds(OddValue homeDrawOdds, OddValue drawAwayOdds, OddValue homeAwayOdds) {
        this.homeDrawOdds = homeDrawOdds;
        this.drawAwayOdds = drawAwayOdds;
        this.homeAwayOdds = homeAwayOdds;
    }

    public OddValue getHomeDrawOdds() {
        return homeDrawOdds;
    }

    public OddValue getDrawAwayOdds() {
        return drawAwayOdds;
    }

    public OddValue getHomeAwayOdds() {
        return homeAwayOdds;
    }

    @Override
    public String toString() {
        return "DCOdds{" +
                "homeDrawOdds=" + homeDrawOdds +
                ", drawAwayOdds=" + drawAwayOdds +
                ", homeAwayOdds=" + homeAwayOdds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(homeDrawOdds, drawAwayOdds, homeAwayOdds);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.DC;
    }
}
