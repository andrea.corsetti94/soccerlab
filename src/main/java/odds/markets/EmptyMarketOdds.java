package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Collections;
import java.util.List;

public class EmptyMarketOdds extends AbstractMarketOdds {

    @Override
    public String toString() {
        return "Not Available";
    }

    @Override
    public List<OddValue> getValues() {
        return Collections.emptyList();
    }

    @Override
    public boolean isLegit() {
        return false;
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.EMPTY_MARKET_TYPE;
    }
}
