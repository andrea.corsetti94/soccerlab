package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class UnderOver1_5Odds extends AbstractMarketOdds {

    private final OddValue under1_5Odds;
    private final OddValue over1_5Odds;

    public UnderOver1_5Odds(OddValue under1_5Odds, OddValue over1_5Odds) {
        this.under1_5Odds = under1_5Odds;
        this.over1_5Odds = over1_5Odds;
    }

    public OddValue getUnder1_5Odds() {
        return under1_5Odds;
    }

    public OddValue getOver1_5Odds() {
        return over1_5Odds;
    }

    @Override
    public String toString() {
        return "UnderOver1_5Odds{" +
                "under1_5Odds=" + under1_5Odds +
                ", over1_5Odds=" + over1_5Odds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(under1_5Odds, over1_5Odds);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.UO1_5;
    }
}
