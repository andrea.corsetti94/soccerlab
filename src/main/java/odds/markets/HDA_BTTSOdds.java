package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class HDA_BTTSOdds extends AbstractMarketOdds {

    private final OddValue homeYesOdds;
    private final OddValue homeNoOdds;
    private final OddValue drawYesOdds;
    private final OddValue drawNoOdds;
    private final OddValue awayYesOdds;
    private final OddValue awayNoOdds;

    public HDA_BTTSOdds(OddValue homeYesOdds, OddValue homeNoOdds, OddValue drawYesOdds,
                        OddValue drawNoOdds, OddValue awayYesOdds, OddValue awayNoOdds) {
        this.homeYesOdds = homeYesOdds;
        this.homeNoOdds = homeNoOdds;
        this.drawYesOdds = drawYesOdds;
        this.drawNoOdds = drawNoOdds;
        this.awayYesOdds = awayYesOdds;
        this.awayNoOdds = awayNoOdds;
    }

    public OddValue getHomeYesOdds() {
        return homeYesOdds;
    }

    public OddValue getHomeNoOdds() {
        return homeNoOdds;
    }

    public OddValue getDrawYesOdds() {
        return drawYesOdds;
    }

    public OddValue getDrawNoOdds() {
        return drawNoOdds;
    }

    public OddValue getAwayYesOdds() {
        return awayYesOdds;
    }

    public OddValue getAwayNoOdds() {
        return awayNoOdds;
    }

    @Override
    public String toString() {
        return "HDA_BTTSOdds{" +
                "homeYesOdds=" + homeYesOdds +
                ", homeNoOdds=" + homeNoOdds +
                ", drawYesOdds=" + drawYesOdds +
                ", drawNoOdds=" + drawNoOdds +
                ", awayYesOdds=" + awayYesOdds +
                ", awayNoOdds=" + awayNoOdds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(homeYesOdds, homeNoOdds, drawYesOdds, drawNoOdds, awayYesOdds, awayNoOdds);
    }


    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.HDA_BTTS;
    }
}
