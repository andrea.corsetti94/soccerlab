package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class BTTS_HTOdds extends AbstractMarketOdds {

    private final OddValue yesBTTS_HT_Odds;
    private final OddValue noBTTS_HT_Odds;

    public BTTS_HTOdds(OddValue yesBTTS_HT_Odds, OddValue noBTTS_HT_Odds) {
        this.yesBTTS_HT_Odds = yesBTTS_HT_Odds;
        this.noBTTS_HT_Odds = noBTTS_HT_Odds;
    }

    public OddValue getYesBTTS_HT_Odds() {
        return yesBTTS_HT_Odds;
    }

    public OddValue getNoBTTS_HT_Odds() {
        return noBTTS_HT_Odds;
    }

    @Override
    public String toString() {
        return "BTTS_HTOdds{" +
                "yesBTTS_HT_Odds=" + yesBTTS_HT_Odds +
                ", noBTTS_HT_Odds=" + noBTTS_HT_Odds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(yesBTTS_HT_Odds, noBTTS_HT_Odds);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.BTTS_HT;
    }

}
