package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class UnderOver4_5Odds extends AbstractMarketOdds {

    private final OddValue under4_5Odds;
    private final OddValue over4_5Odds;

    public UnderOver4_5Odds(OddValue under4_5Odds, OddValue over4_5Odds) {
        this.under4_5Odds = under4_5Odds;
        this.over4_5Odds = over4_5Odds;
    }

    public OddValue getUnder4_5Odds() {
        return under4_5Odds;
    }

    public OddValue getOver4_5Odds() {
        return over4_5Odds;
    }

    @Override
    public String toString() {
        return "UnderOver4_5Odds{" +
                "under4_5Odds=" + under4_5Odds +
                ", over4_5Odds=" + over4_5Odds +
                '}';
    }

    @Override
    public List<OddValue> getValues(){
        return Arrays.asList(under4_5Odds, over4_5Odds);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.UO4_5;
    }

}
