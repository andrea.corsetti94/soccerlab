package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class BTTSOdds extends AbstractMarketOdds {

    private final OddValue yesBTTSOddsValues;
    private final OddValue noBTTSOddsValues;

    public BTTSOdds(OddValue yesBTTSOddsValues, OddValue noBTTSOddsValues) {
        this.yesBTTSOddsValues = yesBTTSOddsValues;
        this.noBTTSOddsValues = noBTTSOddsValues;
    }

    public OddValue getYesBTTSOddsValues() {
        return yesBTTSOddsValues;
    }

    public OddValue getNoBTTSOddsValues() {
        return noBTTSOddsValues;
    }

    @Override
    public String toString() {
        return "BTTSOdds{" +
                "yesBTTSOddsValues=" + yesBTTSOddsValues +
                ", noBTTSOddsValues=" + noBTTSOddsValues +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(yesBTTSOddsValues, noBTTSOddsValues);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.BTTS;
    }
}
