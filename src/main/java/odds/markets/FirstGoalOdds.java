package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class FirstGoalOdds extends AbstractMarketOdds {

    private final OddValue noGoalFirstGoalOddsValues;
    private final OddValue awayFirstGoalOddsValues;
    private final OddValue homeFirstGoalOddsValues;

    public FirstGoalOdds(OddValue noGoalFirstGoalOddsValues, OddValue awayFirstGoalOddsValues, OddValue homeFirstGoalOddsValues) {
        this.noGoalFirstGoalOddsValues = noGoalFirstGoalOddsValues;
        this.awayFirstGoalOddsValues = awayFirstGoalOddsValues;
        this.homeFirstGoalOddsValues = homeFirstGoalOddsValues;
    }

    public OddValue getNoGoalFirstGoalOddsValues() {
        return noGoalFirstGoalOddsValues;
    }

    public OddValue getAwayFirstGoalOddsValues() {
        return awayFirstGoalOddsValues;
    }

    public OddValue getHomeFirstGoalOddsValues() {
        return homeFirstGoalOddsValues;
    }

    @Override
    public String toString() {
        return "FirstGoalOdds{" +
                "noGoalFirstGoalOddsValues=" + noGoalFirstGoalOddsValues +
                ", awayFirstGoalOddsValues=" + awayFirstGoalOddsValues +
                ", homeFirstGoalOddsValues=" + homeFirstGoalOddsValues +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(noGoalFirstGoalOddsValues, awayFirstGoalOddsValues, homeFirstGoalOddsValues);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.FIRST_GOAL;
    }

}
