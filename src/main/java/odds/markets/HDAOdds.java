package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class HDAOdds extends AbstractMarketOdds {

    private final OddValue homeOdds;
    private final OddValue drawOdds;
    private final OddValue awayOdds;

    public HDAOdds(OddValue homeOdds, OddValue drawOdds, OddValue awayOdds) {
        this.homeOdds = homeOdds;
        this.drawOdds = drawOdds;
        this.awayOdds = awayOdds;
    }

    public OddValue getHomeOdds() {
        return homeOdds;
    }

    public OddValue getDrawOdds() {
        return drawOdds;
    }

    public OddValue getAwayOdds() {
        return awayOdds;
    }

    @Override
    public String toString() {
        return "HDAOdds{" +
                "homeOdds=" + homeOdds +
                ", drawOdds=" + drawOdds +
                ", awayOdds=" + awayOdds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(homeOdds, drawOdds, awayOdds);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.HDA;
    }
}
