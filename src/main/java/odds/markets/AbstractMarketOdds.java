package odds.markets;

import odds.Markets;
import odds.values.OddValue;
import utils.OddsUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMarketOdds {

    public abstract List<OddValue> getValues();

    public boolean isLegit(){
        for (OddValue value : getValues()) {
            if (value.getValue() <= 0) return false;
        }
        return !(OddsUtils.bookMargin(this) < 1.01);
    }

    public abstract Markets.MarketType getMarketType();

    public List<Double> getOddValues(){
        List<Double> oddsValues = new ArrayList<>();

        getValues().forEach( oddValue -> oddsValues.add(oddValue.getValue()));
        return oddsValues;
    }
}
