package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class ExactScoreOdds extends AbstractMarketOdds {

    private final OddValue nilNilOdds;
    private final OddValue nilOneOdds;
    private final OddValue nilTwoOdds;
    private final OddValue nilThreeOdds;
    private final OddValue nilFourOdds;
    private final OddValue oneNilOdds;
    private final OddValue twoNilOdds;
    private final OddValue threeNilOdds;
    private final OddValue fourNilOdds;
    private final OddValue oneOneOdds;
    private final OddValue oneTwoOdds;
    private final OddValue oneThreeOdds;
    private final OddValue oneFourOdds;
    private final OddValue twoOneOdds;
    private final OddValue twoTwoOdds;
    private final OddValue twoThreeOdds;
    private final OddValue twoFourOdds;
    private final OddValue threeOneOdds;
    private final OddValue threeTwoOdds;
    private final OddValue threeThreeOdds;
    private final OddValue threeFourOdds;
    private final OddValue fourOneOdds;
    private final OddValue fourTwoOdds;
    private final OddValue fourThreeOdds;
    private final OddValue fourFourOdds;
    private final OddValue othersOdds;

    public ExactScoreOdds(OddValue nilNilOdds, OddValue nilOneOdds, OddValue nilTwoOdds,
                          OddValue nilThreeOdds, OddValue nilFourOdds, OddValue oneNilOdds,
                          OddValue twoNilOdds, OddValue threeNilOdds, OddValue fourNilOdds,
                          OddValue oneOneOdds, OddValue oneTwoOdds, OddValue oneThreeOdds,
                          OddValue oneFourOdds, OddValue twoOneOdds, OddValue twoTwoOdds,
                          OddValue twoThreeOdds, OddValue twoFourOdds, OddValue threeOneOdds,
                          OddValue threeTwoOdds, OddValue threeThreeOdds, OddValue threeFourOdds,
                          OddValue fourOneOdds, OddValue fourTwoOdds, OddValue fourThreeOdds,
                          OddValue fourFourOdds, OddValue othersOdds) {

        this.nilNilOdds = nilNilOdds;
        this.nilOneOdds = nilOneOdds;
        this.nilTwoOdds = nilTwoOdds;
        this.nilThreeOdds = nilThreeOdds;
        this.nilFourOdds = nilFourOdds;
        this.oneNilOdds = oneNilOdds;
        this.twoNilOdds = twoNilOdds;
        this.threeNilOdds = threeNilOdds;
        this.fourNilOdds = fourNilOdds;
        this.oneOneOdds = oneOneOdds;
        this.oneTwoOdds = oneTwoOdds;
        this.oneThreeOdds = oneThreeOdds;
        this.oneFourOdds = oneFourOdds;
        this.twoOneOdds = twoOneOdds;
        this.twoTwoOdds = twoTwoOdds;
        this.twoThreeOdds = twoThreeOdds;
        this.twoFourOdds = twoFourOdds;
        this.threeOneOdds = threeOneOdds;
        this.threeTwoOdds = threeTwoOdds;
        this.threeThreeOdds = threeThreeOdds;
        this.threeFourOdds = threeFourOdds;
        this.fourOneOdds = fourOneOdds;
        this.fourTwoOdds = fourTwoOdds;
        this.fourThreeOdds = fourThreeOdds;
        this.fourFourOdds = fourFourOdds;
        this.othersOdds = othersOdds;
    }

    public OddValue getNilNilOdds() {
        return nilNilOdds;
    }

    public OddValue getNilOneOdds() {
        return nilOneOdds;
    }

    public OddValue getNilTwoOdds() {
        return nilTwoOdds;
    }

    public OddValue getNilThreeOdds() {
        return nilThreeOdds;
    }

    public OddValue getNilFourOdds() {
        return nilFourOdds;
    }

    public OddValue getOneNilOdds() {
        return oneNilOdds;
    }

    public OddValue getTwoNilOdds() {
        return twoNilOdds;
    }

    public OddValue getThreeNilOdds() {
        return threeNilOdds;
    }

    public OddValue getFourNilOdds() {
        return fourNilOdds;
    }

    public OddValue getOneOneOdds() {
        return oneOneOdds;
    }

    public OddValue getOneTwoOdds() {
        return oneTwoOdds;
    }

    public OddValue getOneThreeOdds() {
        return oneThreeOdds;
    }

    public OddValue getOneFourOdds() {
        return oneFourOdds;
    }

    public OddValue getTwoOneOdds() {
        return twoOneOdds;
    }

    public OddValue getTwoTwoOdds() {
        return twoTwoOdds;
    }

    public OddValue getTwoThreeOdds() {
        return twoThreeOdds;
    }

    public OddValue getTwoFourOdds() {
        return twoFourOdds;
    }

    public OddValue getThreeOneOdds() {
        return threeOneOdds;
    }

    public OddValue getThreeTwoOdds() {
        return threeTwoOdds;
    }

    public OddValue getThreeThreeOdds() {
        return threeThreeOdds;
    }

    public OddValue getThreeFourOdds() {
        return threeFourOdds;
    }

    public OddValue getFourOneOdds() {
        return fourOneOdds;
    }

    public OddValue getFourTwoOdds() {
        return fourTwoOdds;
    }

    public OddValue getFourThreeOdds() {
        return fourThreeOdds;
    }

    public OddValue getFourFourOdds() {
        return fourFourOdds;
    }

    public OddValue getOthersOdds() {
        return othersOdds;
    }

    @Override
    public String toString() {
        return "ExactScoreOdds{" +
                "nilNilOdds=" + nilNilOdds +
                ", nilOneOdds=" + nilOneOdds +
                ", nilTwoOdds=" + nilTwoOdds +
                ", nilThreeOdds=" + nilThreeOdds +
                ", nilFourOdds=" + nilFourOdds +
                ", oneNilOdds=" + oneNilOdds +
                ", twoNilOdds=" + twoNilOdds +
                ", threeNilOdds=" + threeNilOdds +
                ", fourNilOdds=" + fourNilOdds +
                ", oneOneOdds=" + oneOneOdds +
                ", oneTwoOdds=" + oneTwoOdds +
                ", oneThreeOdds=" + oneThreeOdds +
                ", oneFourOdds=" + oneFourOdds +
                ", twoOneOdds=" + twoOneOdds +
                ", twoTwoOdds=" + twoTwoOdds +
                ", twoThreeOdds=" + twoThreeOdds +
                ", twoFourOdds=" + twoFourOdds +
                ", threeOneOdds=" + threeOneOdds +
                ", threeTwoOdds=" + threeTwoOdds +
                ", threeThreeOdds=" + threeThreeOdds +
                ", threeFourOdds=" + threeFourOdds +
                ", fourOneOdds=" + fourOneOdds +
                ", fourTwoOdds=" + fourTwoOdds +
                ", fourThreeOdds=" + fourThreeOdds +
                ", fourFourOdds=" + fourFourOdds +
                ", othersOdds=" + othersOdds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(
                nilNilOdds, nilOneOdds, nilTwoOdds, nilThreeOdds, nilFourOdds,
                oneNilOdds, oneOneOdds, oneTwoOdds, oneThreeOdds, oneFourOdds,
                twoNilOdds, twoOneOdds, twoTwoOdds, twoThreeOdds, twoFourOdds,
                threeNilOdds, threeOneOdds, threeTwoOdds, threeThreeOdds, threeFourOdds,
                fourNilOdds, fourOneOdds, fourTwoOdds, fourThreeOdds, fourFourOdds,
                othersOdds
        );
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.EXACT_SCORE;
    }

}
