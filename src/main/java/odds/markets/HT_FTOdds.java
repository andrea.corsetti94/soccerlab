package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class HT_FTOdds extends AbstractMarketOdds {

    private final OddValue homeAwayHT_FTOddsValues;
    private final OddValue homeHomeHT_FTOddsValues;
    private final OddValue homeDrawHT_FTOddsValues;
    private final OddValue drawHomeHT_FTOddsValues;
    private final OddValue drawDrawHT_FTOddsValues;
    private final OddValue drawAwayHT_FTOddsValues;
    private final OddValue awayHomeHT_FTOddsValues;
    private final OddValue awayDrawHT_FTOddsValues;
    private final OddValue awayAwayHT_FTOddsValues;

    public HT_FTOdds(OddValue homeAwayHT_FTOddsValues, OddValue homeHomeHT_FTOddsValues, OddValue homeDrawHT_FTOddsValues,
                     OddValue drawHomeHT_FTOddsValues, OddValue drawDrawHT_FTOddsValues, OddValue drawAwayHT_FTOddsValues,
                     OddValue awayHomeHT_FTOddsValues, OddValue awayDrawHT_FTOddsValues, OddValue awayAwayHT_FTOddsValues) {

        this.homeAwayHT_FTOddsValues = homeAwayHT_FTOddsValues;
        this.homeHomeHT_FTOddsValues = homeHomeHT_FTOddsValues;
        this.homeDrawHT_FTOddsValues = homeDrawHT_FTOddsValues;
        this.drawHomeHT_FTOddsValues = drawHomeHT_FTOddsValues;
        this.drawDrawHT_FTOddsValues = drawDrawHT_FTOddsValues;
        this.drawAwayHT_FTOddsValues = drawAwayHT_FTOddsValues;
        this.awayHomeHT_FTOddsValues = awayHomeHT_FTOddsValues;
        this.awayDrawHT_FTOddsValues = awayDrawHT_FTOddsValues;
        this.awayAwayHT_FTOddsValues = awayAwayHT_FTOddsValues;
    }

    public OddValue getHomeAwayHT_FTOddsValues() {
        return homeAwayHT_FTOddsValues;
    }

    public OddValue getHomeHomeHT_FTOddsValues() {
        return homeHomeHT_FTOddsValues;
    }

    public OddValue getHomeDrawHT_FTOddsValues() {
        return homeDrawHT_FTOddsValues;
    }

    public OddValue getDrawHomeHT_FTOddsValues() {
        return drawHomeHT_FTOddsValues;
    }

    public OddValue getDrawDrawHT_FTOddsValues() {
        return drawDrawHT_FTOddsValues;
    }

    public OddValue getDrawAwayHT_FTOddsValues() {
        return drawAwayHT_FTOddsValues;
    }

    public OddValue getAwayHomeHT_FTOddsValues() {
        return awayHomeHT_FTOddsValues;
    }

    public OddValue getAwayDrawHT_FTOddsValues() {
        return awayDrawHT_FTOddsValues;
    }

    public OddValue getAwayAwayHT_FTOddsValues() {
        return awayAwayHT_FTOddsValues;
    }

    @Override
    public String toString() {
        return "HT_FTOdds{" +
                "homeAwayHT_FTOddsValues=" + homeAwayHT_FTOddsValues +
                ", homeHomeHT_FTOddsValues=" + homeHomeHT_FTOddsValues +
                ", homeDrawHT_FTOddsValues=" + homeDrawHT_FTOddsValues +
                ", drawHomeHT_FTOddsValues=" + drawHomeHT_FTOddsValues +
                ", drawDrawHT_FTOddsValues=" + drawDrawHT_FTOddsValues +
                ", drawAwayHT_FTOddsValues=" + drawAwayHT_FTOddsValues +
                ", awayHomeHT_FTOddsValues=" + awayHomeHT_FTOddsValues +
                ", awayDrawHT_FTOddsValues=" + awayDrawHT_FTOddsValues +
                ", awayAwayHT_FTOddsValues=" + awayAwayHT_FTOddsValues +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(
                homeAwayHT_FTOddsValues, homeHomeHT_FTOddsValues, homeDrawHT_FTOddsValues,
                drawAwayHT_FTOddsValues, drawDrawHT_FTOddsValues, drawHomeHT_FTOddsValues,
                awayAwayHT_FTOddsValues, awayDrawHT_FTOddsValues, awayHomeHT_FTOddsValues
        );
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.HT_FT;
    }

}
