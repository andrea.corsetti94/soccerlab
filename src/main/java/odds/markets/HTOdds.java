package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class HTOdds extends AbstractMarketOdds {

    private final OddValue homeHTOddsValues;
    private final OddValue drawHTOddsValues;
    private final OddValue awayHTOddsValues;

    public HTOdds(OddValue homeHTOddsValues, OddValue drawHTOddsValues, OddValue awayHTOddsValues) {
        this.homeHTOddsValues = homeHTOddsValues;
        this.drawHTOddsValues = drawHTOddsValues;
        this.awayHTOddsValues = awayHTOddsValues;
    }

    public OddValue getHomeHTOddsValues() {
        return homeHTOddsValues;
    }

    public OddValue getDrawHTOddsValues() {
        return drawHTOddsValues;
    }

    public OddValue getAwayHTOddsValues() {
        return awayHTOddsValues;
    }

    @Override
    public String toString() {
        return "HTOdds{" +
                "homeHTOddsValues=" + homeHTOddsValues +
                ", drawHTOddsValues=" + drawHTOddsValues +
                ", awayHTOddsValues=" + awayHTOddsValues +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(homeHTOddsValues, drawHTOddsValues, awayHTOddsValues);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.HT;
    }

}
