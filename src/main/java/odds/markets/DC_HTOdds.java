package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class DC_HTOdds extends AbstractMarketOdds {

    private final OddValue homeDrawHTOdds;
    private final OddValue drawAwayHTOdds;
    private final OddValue homeAwayHTOdds;

    public DC_HTOdds(OddValue homeDrawHTOdds, OddValue drawAwayHTOdds, OddValue homeAwayHTOdds) {
        this.homeDrawHTOdds = homeDrawHTOdds;
        this.drawAwayHTOdds = drawAwayHTOdds;
        this.homeAwayHTOdds = homeAwayHTOdds;
    }

    public OddValue getHomeDrawHTOdds() {
        return homeDrawHTOdds;
    }

    public OddValue getDrawAwayHTOdds() {
        return drawAwayHTOdds;
    }

    public OddValue getHomeAwayHTOdds() {
        return homeAwayHTOdds;
    }

    @Override
    public String toString() {
        return "DC_HTOdds{" +
                "homeDrawHTOdds=" + homeDrawHTOdds +
                ", drawAwayHTOdds=" + drawAwayHTOdds +
                ", homeAwayHTOdds=" + homeAwayHTOdds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(homeDrawHTOdds, drawAwayHTOdds, homeAwayHTOdds);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.DC_HT;
    }

}
