package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class HDA_UnderOverOdds extends AbstractMarketOdds {

    private final OddValue homeO1_5Odds;
    private final OddValue homeO2_5Odds;
    private final OddValue homeO3_5Odds;
    private final OddValue homeO4_5Odds;
    private final OddValue homeU1_5Odds;
    private final OddValue homeU2_5Odds;
    private final OddValue homeU3_5Odds;
    private final OddValue homeU4_5Odds;
    private final OddValue drawO1_5Odds;
    private final OddValue drawO2_5Odds;
    private final OddValue drawO3_5Odds;
    private final OddValue drawO4_5Odds;
    private final OddValue drawU1_5Odds;
    private final OddValue drawU2_5Odds;
    private final OddValue drawU3_5Odds;
    private final OddValue drawU4_5Odds;
    private final OddValue awayO1_5Odds;
    private final OddValue awayO2_5Odds;
    private final OddValue awayO3_5Odds;
    private final OddValue awayO4_5Odds;
    private final OddValue awayU1_5Odds;
    private final OddValue awayU2_5Odds;
    private final OddValue awayU3_5Odds;
    private final OddValue awayU4_5Odds;

    public HDA_UnderOverOdds(OddValue homeO1_5Odds, OddValue homeO2_5Odds, OddValue homeO3_5Odds,
                             OddValue homeO4_5Odds, OddValue homeU1_5Odds, OddValue homeU2_5Odds,
                             OddValue homeU3_5Odds, OddValue homeU4_5Odds, OddValue drawO1_5Odds,
                             OddValue drawO2_5Odds, OddValue drawO3_5Odds, OddValue drawO4_5Odds,
                             OddValue drawU1_5Odds, OddValue drawU2_5Odds, OddValue drawU3_5Odds,
                             OddValue drawU4_5Odds, OddValue awayO1_5Odds, OddValue awayO2_5Odds,
                             OddValue awayO3_5Odds, OddValue awayO4_5Odds, OddValue awayU1_5Odds,
                             OddValue awayU2_5Odds, OddValue awayU3_5Odds, OddValue awayU4_5Odds) {

        this.homeO1_5Odds = homeO1_5Odds;
        this.homeO2_5Odds = homeO2_5Odds;
        this.homeO3_5Odds = homeO3_5Odds;
        this.homeO4_5Odds = homeO4_5Odds;
        this.homeU1_5Odds = homeU1_5Odds;
        this.homeU2_5Odds = homeU2_5Odds;
        this.homeU3_5Odds = homeU3_5Odds;
        this.homeU4_5Odds = homeU4_5Odds;
        this.drawO1_5Odds = drawO1_5Odds;
        this.drawO2_5Odds = drawO2_5Odds;
        this.drawO3_5Odds = drawO3_5Odds;
        this.drawO4_5Odds = drawO4_5Odds;
        this.drawU1_5Odds = drawU1_5Odds;
        this.drawU2_5Odds = drawU2_5Odds;
        this.drawU3_5Odds = drawU3_5Odds;
        this.drawU4_5Odds = drawU4_5Odds;
        this.awayO1_5Odds = awayO1_5Odds;
        this.awayO2_5Odds = awayO2_5Odds;
        this.awayO3_5Odds = awayO3_5Odds;
        this.awayO4_5Odds = awayO4_5Odds;
        this.awayU1_5Odds = awayU1_5Odds;
        this.awayU2_5Odds = awayU2_5Odds;
        this.awayU3_5Odds = awayU3_5Odds;
        this.awayU4_5Odds = awayU4_5Odds;
    }

    public OddValue getHomeO1_5Odds() {
        return homeO1_5Odds;
    }

    public OddValue getHomeO2_5Odds() {
        return homeO2_5Odds;
    }

    public OddValue getHomeO3_5Odds() {
        return homeO3_5Odds;
    }

    public OddValue getHomeO4_5Odds() {
        return homeO4_5Odds;
    }

    public OddValue getHomeU1_5Odds() {
        return homeU1_5Odds;
    }

    public OddValue getHomeU2_5Odds() {
        return homeU2_5Odds;
    }

    public OddValue getHomeU3_5Odds() {
        return homeU3_5Odds;
    }

    public OddValue getHomeU4_5Odds() {
        return homeU4_5Odds;
    }

    public OddValue getDrawO1_5Odds() {
        return drawO1_5Odds;
    }

    public OddValue getDrawO2_5Odds() {
        return drawO2_5Odds;
    }

    public OddValue getDrawO3_5Odds() {
        return drawO3_5Odds;
    }

    public OddValue getDrawO4_5Odds() {
        return drawO4_5Odds;
    }

    public OddValue getDrawU1_5Odds() {
        return drawU1_5Odds;
    }

    public OddValue getDrawU2_5Odds() {
        return drawU2_5Odds;
    }

    public OddValue getDrawU3_5Odds() {
        return drawU3_5Odds;
    }

    public OddValue getDrawU4_5Odds() {
        return drawU4_5Odds;
    }

    public OddValue getAwayO1_5Odds() {
        return awayO1_5Odds;
    }

    public OddValue getAwayO2_5Odds() {
        return awayO2_5Odds;
    }

    public OddValue getAwayO3_5Odds() {
        return awayO3_5Odds;
    }

    public OddValue getAwayO4_5Odds() {
        return awayO4_5Odds;
    }

    public OddValue getAwayU1_5Odds() {
        return awayU1_5Odds;
    }

    public OddValue getAwayU2_5Odds() {
        return awayU2_5Odds;
    }

    public OddValue getAwayU3_5Odds() {
        return awayU3_5Odds;
    }

    public OddValue getAwayU4_5Odds() {
        return awayU4_5Odds;
    }

    @Override
    public String toString() {
        return "HDA_UnderOverOdds{" +
                "homeO1_5Odds=" + homeO1_5Odds +
                ", homeO2_5Odds=" + homeO2_5Odds +
                ", homeO3_5Odds=" + homeO3_5Odds +
                ", homeO4_5Odds=" + homeO4_5Odds +
                ", homeU1_5Odds=" + homeU1_5Odds +
                ", homeU2_5Odds=" + homeU2_5Odds +
                ", homeU3_5Odds=" + homeU3_5Odds +
                ", homeU4_5Odds=" + homeU4_5Odds +
                ", drawO1_5Odds=" + drawO1_5Odds +
                ", drawO2_5Odds=" + drawO2_5Odds +
                ", drawO3_5Odds=" + drawO3_5Odds +
                ", drawO4_5Odds=" + drawO4_5Odds +
                ", drawU1_5Odds=" + drawU1_5Odds +
                ", drawU2_5Odds=" + drawU2_5Odds +
                ", drawU3_5Odds=" + drawU3_5Odds +
                ", drawU4_5Odds=" + drawU4_5Odds +
                ", awayO1_5Odds=" + awayO1_5Odds +
                ", awayO2_5Odds=" + awayO2_5Odds +
                ", awayO3_5Odds=" + awayO3_5Odds +
                ", awayO4_5Odds=" + awayO4_5Odds +
                ", awayU1_5Odds=" + awayU1_5Odds +
                ", awayU2_5Odds=" + awayU2_5Odds +
                ", awayU3_5Odds=" + awayU3_5Odds +
                ", awayU4_5Odds=" + awayU4_5Odds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(
                homeO1_5Odds, homeO2_5Odds, homeO3_5Odds, homeO4_5Odds,
                homeU1_5Odds, homeU2_5Odds, homeU3_5Odds, homeU4_5Odds,
                drawO1_5Odds, drawO2_5Odds, drawO3_5Odds, drawO4_5Odds,
                drawU1_5Odds, drawU2_5Odds, drawU3_5Odds, drawU4_5Odds,
                awayO1_5Odds, awayO2_5Odds, awayO3_5Odds, awayO4_5Odds,
                awayU1_5Odds, awayU2_5Odds, awayU3_5Odds, awayU4_5Odds
        );
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.HDA_UNDEROVER;
    }
}
