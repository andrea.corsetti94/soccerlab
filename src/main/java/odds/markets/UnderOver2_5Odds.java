package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class UnderOver2_5Odds extends AbstractMarketOdds {

    private final OddValue under2_5Odds;
    private final OddValue over2_5Odds;

    public UnderOver2_5Odds(OddValue under2_5Odds, OddValue over2_5Odds) {
        this.under2_5Odds = under2_5Odds;
        this.over2_5Odds = over2_5Odds;
    }

    public OddValue getUnder2_5Odds() {
        return under2_5Odds;
    }

    public OddValue getOver2_5Odds() {
        return over2_5Odds;
    }

    @Override
    public String toString() {
        return "UnderOver2_5Odds{" +
                "under2_5Odds=" + under2_5Odds +
                ", over2_5Odds=" + over2_5Odds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(under2_5Odds, over2_5Odds);
    }


    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.UO2_5;
    }
}
