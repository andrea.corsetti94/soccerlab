package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class ExactScoreHTOdds extends AbstractMarketOdds {

    private final OddValue nilNilHTOdds;
    private final OddValue nilOneHTOdds;
    private final OddValue nilTwoHTOdds;
    private final OddValue nilThreeHTOdds;
    private final OddValue nilFourHTOdds;
    private final OddValue oneNilHTOdds;
    private final OddValue twoNilHTOdds;
    private final OddValue threeNilHTOdds;
    private final OddValue fourNilHTOdds;
    private final OddValue oneOneHTOdds;
    private final OddValue oneTwoHTOdds;
    private final OddValue oneThreeHTOdds;
    private final OddValue oneFourHTOdds;
    private final OddValue twoOneHTOdds;
    private final OddValue twoTwoHTOdds;
    private final OddValue twoThreeHTOdds;
    private final OddValue twoFourHTOdds;
    private final OddValue threeOneHTOdds;
    private final OddValue threeTwoHTOdds;
    private final OddValue threeThreeHTOdds;
    private final OddValue threeFourHTOdds;
    private final OddValue fourOneHTOdds;
    private final OddValue fourTwoHTOdds;
    private final OddValue fourThreeHTOdds;
    private final OddValue fourFourHTOdds;
    private final OddValue othersHTOdds;

    public ExactScoreHTOdds(OddValue nilNilHTOdds, OddValue nilOneHTOdds, OddValue nilTwoHTOdds,
                            OddValue nilThreeHTOdds, OddValue nilFourHTOdds, OddValue oneNilHTOdds,
                            OddValue twoNilHTOdds, OddValue threeNilHTOdds, OddValue fourNilHTOdds,
                            OddValue oneOneHTOdds, OddValue oneTwoHTOdds, OddValue oneThreeHTOdds,
                            OddValue oneFourHTOdds, OddValue twoOneHTOdds, OddValue twoTwoHTOdds,
                            OddValue twoThreeHTOdds, OddValue twoFourHTOdds, OddValue threeOneHTOdds,
                            OddValue threeTwoHTOdds, OddValue threeThreeHTOdds, OddValue threeFourHTOdds,
                            OddValue fourOneHTOdds, OddValue fourTwoHTOdds, OddValue fourThreeHTOdds,
                            OddValue fourFourHTOdds, OddValue othersHTOdds) {

        this.nilNilHTOdds = nilNilHTOdds;
        this.nilOneHTOdds = nilOneHTOdds;
        this.nilTwoHTOdds = nilTwoHTOdds;
        this.nilThreeHTOdds = nilThreeHTOdds;
        this.nilFourHTOdds = nilFourHTOdds;
        this.oneNilHTOdds = oneNilHTOdds;
        this.twoNilHTOdds = twoNilHTOdds;
        this.threeNilHTOdds = threeNilHTOdds;
        this.fourNilHTOdds = fourNilHTOdds;
        this.oneOneHTOdds = oneOneHTOdds;
        this.oneTwoHTOdds = oneTwoHTOdds;
        this.oneThreeHTOdds = oneThreeHTOdds;
        this.oneFourHTOdds = oneFourHTOdds;
        this.twoOneHTOdds = twoOneHTOdds;
        this.twoTwoHTOdds = twoTwoHTOdds;
        this.twoThreeHTOdds = twoThreeHTOdds;
        this.twoFourHTOdds = twoFourHTOdds;
        this.threeOneHTOdds = threeOneHTOdds;
        this.threeTwoHTOdds = threeTwoHTOdds;
        this.threeThreeHTOdds = threeThreeHTOdds;
        this.threeFourHTOdds = threeFourHTOdds;
        this.fourOneHTOdds = fourOneHTOdds;
        this.fourTwoHTOdds = fourTwoHTOdds;
        this.fourThreeHTOdds = fourThreeHTOdds;
        this.fourFourHTOdds = fourFourHTOdds;
        this.othersHTOdds = othersHTOdds;
    }

    @Override
    public String toString() {
        return "ExactScoreHTOdds{" +
                "nilNilHTOdds=" + nilNilHTOdds +
                ", nilOneHTOdds=" + nilOneHTOdds +
                ", nilTwoHTOdds=" + nilTwoHTOdds +
                ", nilThreeHTOdds=" + nilThreeHTOdds +
                ", nilFourHTOdds=" + nilFourHTOdds +
                ", oneNilHTOdds=" + oneNilHTOdds +
                ", twoNilHTOdds=" + twoNilHTOdds +
                ", threeNilHTOdds=" + threeNilHTOdds +
                ", fourNilHTOdds=" + fourNilHTOdds +
                ", oneOneHTOdds=" + oneOneHTOdds +
                ", oneTwoHTOdds=" + oneTwoHTOdds +
                ", oneThreeHTOdds=" + oneThreeHTOdds +
                ", oneFourHTOdds=" + oneFourHTOdds +
                ", twoOneHTOdds=" + twoOneHTOdds +
                ", twoTwoHTOdds=" + twoTwoHTOdds +
                ", twoThreeHTOdds=" + twoThreeHTOdds +
                ", twoFourHTOdds=" + twoFourHTOdds +
                ", threeOneHTOdds=" + threeOneHTOdds +
                ", threeTwoHTOdds=" + threeTwoHTOdds +
                ", threeThreeHTOdds=" + threeThreeHTOdds +
                ", threeFourHTOdds=" + threeFourHTOdds +
                ", fourOneHTOdds=" + fourOneHTOdds +
                ", fourTwoHTOdds=" + fourTwoHTOdds +
                ", fourThreeHTOdds=" + fourThreeHTOdds +
                ", fourFourHTOdds=" + fourFourHTOdds +
                ", othersHTOdds=" + othersHTOdds +
                '}';
    }

    public OddValue getNilNilHTOdds() {
        return nilNilHTOdds;
    }

    public OddValue getNilOneHTOdds() {
        return nilOneHTOdds;
    }

    public OddValue getNilTwoHTOdds() {
        return nilTwoHTOdds;
    }

    public OddValue getNilThreeHTOdds() {
        return nilThreeHTOdds;
    }

    public OddValue getNilFourHTOdds() {
        return nilFourHTOdds;
    }

    public OddValue getOneNilHTOdds() {
        return oneNilHTOdds;
    }

    public OddValue getTwoNilHTOdds() {
        return twoNilHTOdds;
    }

    public OddValue getThreeNilHTOdds() {
        return threeNilHTOdds;
    }

    public OddValue getFourNilHTOdds() {
        return fourNilHTOdds;
    }

    public OddValue getOneOneHTOdds() {
        return oneOneHTOdds;
    }

    public OddValue getOneTwoHTOdds() {
        return oneTwoHTOdds;
    }

    public OddValue getOneThreeHTOdds() {
        return oneThreeHTOdds;
    }

    public OddValue getOneFourHTOdds() {
        return oneFourHTOdds;
    }

    public OddValue getTwoOneHTOdds() {
        return twoOneHTOdds;
    }

    public OddValue getTwoTwoHTOdds() {
        return twoTwoHTOdds;
    }

    public OddValue getTwoThreeHTOdds() {
        return twoThreeHTOdds;
    }

    public OddValue getTwoFourHTOdds() {
        return twoFourHTOdds;
    }

    public OddValue getThreeOneHTOdds() {
        return threeOneHTOdds;
    }

    public OddValue getThreeTwoHTOdds() {
        return threeTwoHTOdds;
    }

    public OddValue getThreeThreeHTOdds() {
        return threeThreeHTOdds;
    }

    public OddValue getThreeFourHTOdds() {
        return threeFourHTOdds;
    }

    public OddValue getFourOneHTOdds() {
        return fourOneHTOdds;
    }

    public OddValue getFourTwoHTOdds() {
        return fourTwoHTOdds;
    }

    public OddValue getFourThreeHTOdds() {
        return fourThreeHTOdds;
    }

    public OddValue getFourFourHTOdds() {
        return fourFourHTOdds;
    }

    public OddValue getOthersHTOdds() {
        return othersHTOdds;
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(
                nilNilHTOdds, nilOneHTOdds, nilTwoHTOdds, nilThreeHTOdds, nilFourHTOdds,
                oneNilHTOdds, oneTwoHTOdds, oneThreeHTOdds, oneFourHTOdds,
                twoNilHTOdds, twoOneHTOdds, twoTwoHTOdds, twoThreeHTOdds, twoFourHTOdds,
                threeNilHTOdds, threeOneHTOdds, threeTwoHTOdds, threeThreeHTOdds, threeFourHTOdds,
                fourNilHTOdds, fourOneHTOdds, fourTwoHTOdds, fourThreeHTOdds, fourFourHTOdds,
                othersHTOdds
        );
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.EXACT_SCORE_HT;
    }
}
