package odds.markets;

import odds.Markets;
import odds.values.OddValue;

import java.util.Arrays;
import java.util.List;

public class UnderOver3_5Odds extends AbstractMarketOdds {

    private final OddValue under3_5Odds;
    private final OddValue over3_5Odds;

    public UnderOver3_5Odds(OddValue under3_5Odds, OddValue over3_5Odds) {

        this.under3_5Odds = under3_5Odds;
        this.over3_5Odds = over3_5Odds;
    }

    public OddValue getUnder3_5Odds() {
        return under3_5Odds;
    }

    public OddValue getOver3_5Odds() {
        return over3_5Odds;
    }

    @Override
    public String toString() {
        return "UnderOver3_5Odds{" +
                "under3_5Odds=" + under3_5Odds +
                ", over3_5Odds=" + over3_5Odds +
                '}';
    }

    @Override
    public List<OddValue> getValues() {
        return Arrays.asList(under3_5Odds, over3_5Odds);
    }

    @Override
    public Markets.MarketType getMarketType() {
        return Markets.MarketType.UO3_5;
    }
}
