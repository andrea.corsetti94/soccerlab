package data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import json.JSON;
import json.JSONRetriever;
import main.Configs;
import main.Logger;
import model.*;
import model.statistics.EmptyModelStatistics;
import model.statistics.ModelStatitics;
import odds.MatchOdds;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.DateUtils;
import utils.FixturesUtils;
import utils.UnirestUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class API {

    // ++++++++++++++++++++++++++++++++++++++ COUNTRY ++++++++++++++++++++++++++++++++++++++++

    List<Country> retrieveCountryList() {

        String unirestGet = Configs.getApiDomainURL() + "/countries";

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj,"api");
        if (api == null) return Collections.emptyList();

        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of countries in the system: " + results);

        JSONObject countries = JSON.getJSON(api, "countries");
        if ( countries == null ) return Collections.emptyList();
        
        return JSONRetriever.getCountriesFromJSONObject(countries);

    }

    // ++++++++++++++++++++++++++++ FIXTURE +++++++++++++++++++++++++++++++++++++
    /**
     * @param day in format YYYY-MM-DD
     */
    List<Fixture> getFixturesByDay(String day){

        String unirestGet = Configs.getApiDomainURL() + "/fixtures/date/" + day;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet,null);
        if ( response == null ) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj,"api");
        if (api == null) return Collections.emptyList();
        
        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of fixtures for day: " + day + " : " + results);

        if ( results == 0 ) return Collections.emptyList();

        JSONObject fixtures = JSON.getJSON(api,"fixtures");
        if ( fixtures == null ) return Collections.emptyList();

        List<Fixture> fixs = JSONRetriever.getFixturesFromJSONObject(fixtures);
        fixs.sort(FixturesUtils.getDateComparator());

        return fixs;
    }


    /**
     * Get all Live matches currently playing
     */
    public List<Fixture> getLiveFixtures(){

        String unirestGet = Configs.getApiDomainURL() + "/fixtures/live";
        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet,null);

        if ( response == null ) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();
        
        int liveMatches = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of current live fixtures for day: " + liveMatches);

        JSONObject fixtures = JSON.getJSON(api, "fixtures");
        if (fixtures == null) return Collections.emptyList();

        List<Fixture> fixs = JSONRetriever.getFixturesFromJSONObject(fixtures);
        fixs.sort(FixturesUtils.getDateComparator());
        return fixs;

    }

    /**
     * Returns all matches (completed, live and to begin) for a league/competition in one season
     * @param leagueId
     */
    public List<Fixture> getAllMatchesByLeague(String leagueId){

        String unirestGet = Configs.getApiDomainURL() + "/fixtures/league/" + leagueId;
        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet,null);

        if ( response == null ) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();
        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int numOfMatches = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of matches for league: " + leagueId + " this season: " + numOfMatches);

        JSONObject fixtures = JSON.getJSON(api, "fixtures");
        if (fixtures == null) return Collections.emptyList();

        List<Fixture> fixs = JSONRetriever.getFixturesFromJSONObject(fixtures);
        fixs.sort(FixturesUtils.getDateComparator());
        return fixs;

    }


    /**
     * Returns all matches in multiple seasons for a single team.
     * @param teamId
     */
    public List<Fixture> getAllMatchesByTeam(String teamId) {
        String unirestGet = Configs.getApiDomainURL() + "/fixtures/team/" + teamId;
        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);

        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();
        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int matches = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of matches for team: " + teamId + " : " + matches);

        JSONObject fixtures = JSON.getJSON(api, "fixtures");
        if (fixtures == null) return Collections.emptyList();

        List<Fixture> fixs = JSONRetriever.getFixturesFromJSONObject(fixtures);
        fixs.sort(FixturesUtils.getDateComparator());
        return fixs;
    }


    /**
     * Returns a Fixture identified by id: fixtureId. In theory there should be only one fixture by ID but these method returns a list.
     * @param fixtureId
     */
    public List<Fixture> getFixtureByFixtureId(String fixtureId){
        String unirestGet = Configs.getApiDomainURL() + "/fixtures/id/" + fixtureId;
        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);

        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();
        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int matches = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of fixtures for fixtureId " + fixtureId + " : " + matches);

        JSONObject fixtures = JSON.getJSON(api, "fixtures");
        if (fixtures == null) return Collections.emptyList();

        List<Fixture> fixs = JSONRetriever.getFixturesFromJSONObject(fixtures);
        fixs.sort(FixturesUtils.getDateComparator());
        return fixs;
    }



    /**
     * Returns a list of H2H matches between two teams among multiple seasons.
     * In these matches each team could have played as home team or away team.
     */
    public List<Fixture> getH2H(String teamId1, String teamId2) {
        String unirestGet = Configs.getApiDomainURL() + "/fixtures/h2h/" + teamId1 +"/" + teamId2;
        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);

        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();
        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int matches = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of H2H found for teams: " + teamId1 + " vs " + teamId2 + " : " + matches);

        JSONObject fixtures = JSON.getJSON(api, "fixtures");
        if (fixtures == null) return Collections.emptyList();

        List<Fixture> fixs = JSONRetriever.getFixturesFromJSONObject(fixtures);
        fixs.sort(FixturesUtils.getDateComparator());
        return fixs;
    }

    // +++++++++++++++++++++++++++++++++++++++ LEAGUE +++++++++++++++++++++++++++++++++++++++++

    /**
     * Returns all Leagues in the system
     */
    List<League> getLeagues(){
        String unirestGet = Configs.getApiDomainURL() + "/leagues";

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj,"api");
        if (api == null) return Collections.emptyList();

        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of leagues in the system : " + results);

        JSONObject leagues = JSON.getJSON(api,"leagues");
        if (leagues == null) return Collections.emptyList();

        return JSONRetriever.getLeaguesFromJSONObject(leagues);
    }

    /**
     * Returns a list of leagues identified by a leagueId. In theory there should only be one league identified by a single leagueId
     */
    public List<League> getLeaguesById(String id) {
        String unirestGet = Configs.getApiDomainURL() + "/leagues/league/" + id;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of leagues with league_id: " + id + " : " + results);

        JSONObject leagues = JSON.getJSON(api, "leagues");
        if (leagues == null) return Collections.emptyList();

        return JSONRetriever.getLeaguesFromJSONObject(leagues);
    }


    /**
     * seasonYear input has to be with this format: YYYY
     */
    public List<League> getLeaguesBySeasonYear(String seasonYear) {

        String unirestGet = Configs.getApiDomainURL() + "/leagues/season/" + seasonYear;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of leagues playedTotal in season: with league_id: " + seasonYear + " : " + results);

        JSONObject leagues = JSON.getJSON(api, "leagues");
        if (leagues == null) return Collections.emptyList();

        return JSONRetriever.getLeaguesFromJSONObject(leagues);
    }


    /**
     * Returns all leagues playedTotal in a country identified by a country name, in multiple seasons
     */
    public List<League> getLeaguesByCountryNameAllSeasons(String countryName) {

        String unirestGet = Configs.getApiDomainURL() + "/leagues/country/" + countryName;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of leagues playedTotal in these seasons with countryName: " + countryName + " : " + results);

        JSONObject leagues = JSON.getJSON(api, "leagues");
        if (leagues == null) return Collections.emptyList();

        return JSONRetriever.getLeaguesFromJSONObject(leagues);
    }

    /**
     * Returns all leagues playedTotal in a country identified by a country name, in one season identified by a season year with format: YYYY.
     */
    public List<League> getLeaguesByCountryNameAndSeason(String countryName, String seasonYear) {

        String unirestGet = Configs.getApiDomainURL() + "/leagues/country/" + countryName + "/" + seasonYear;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int results = api.getInt("results");
        Logger.getLogger(this.getClass())
                .info("Number of leagues playedTotal in season: "+seasonYear+" with countryName: " + countryName + " : " + results);

        JSONObject leagues = JSON.getJSON(api, "leagues");
        if (leagues == null) return Collections.emptyList();

        return JSONRetriever.getLeaguesFromJSONObject(leagues);
    }

    // ++++++++++++++++++++++++++++++++++++++++++++ ODDS ++++++++++++++++++++++++++++++++++++

    /**
     * Returns all Odds for a fixtureId
     */
    public MatchOdds getOddsForFixture(String fixtureId) {
        String unirestGet = Configs.getApiDomainURL() + "/odds/" + fixtureId;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return new MatchOdds();

        JSONObject myObj = response.getBody().getObject();
        
        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return new MatchOdds();

        JSONObject odds = JSON.getJSON(api, "odds");
        if (odds == null) return new MatchOdds();

        return JSONRetriever.getOddsFromFixtureJSONObject(odds,fixtureId);
        
    }

    // +++++++++++++++++++++++++++++++++++++++++++ SEASON ++++++++++++++++++++++++++++++
    List<Season> getSeasonsList(){
        String unirestGet = Configs.getApiDomainURL() + "/seasons";

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj,"api");
        if (api == null) return Collections.emptyList();
        
        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of seasons in the system: " + results);

        JSONObject seasons = JSON.getJSON(api, "seasons");
        if (seasons == null) return Collections.emptyList();

        return JSONRetriever.getSeasonsFromJSONObject(seasons);
    }

    // +++++++++++++++++++++++++++++++++++++++++ STANDINGS ++++++++++++++++++++++++++++++

    //USA APILEAGUES
    Standing retrieveStandingsByLeague(String leagueId){
        String unirestGet = Configs.getApiDomainURL() + "/leagueTable/" + leagueId;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return null;

        JSONObject myObj = response.getBody().getObject();

        if ( myObj == null ) return null;
        JSONObject api = JSON.getJSON(myObj, "api");

        if (api == null) return null;
        JSONArray standings = JSON.getJSONArray(api, "standings");

        if (standings == null) return null;
        JSONArray standing = JSON.getJSONArrayByIndex(standings,0);

        if (standing == null) return null;
        return JSONRetriever.getStandingsFromJSONArray(standing, leagueId);
    }

    //USA APILEAGUES
    List<Standing> retrieveAllStandings() {
        List<League> leagues = this.getLeagues();


        List<Standing> standings = new ArrayList<>();
        for(League league : leagues){
            Standing standing = this.retrieveStandingsByLeague(league.getId());
            if (standing == null){
                Logger.getLogger(DataUpdater.class).warn("Null standings for league: " + league.getId());
                continue;
            }
            standings.add(standing);
        }

        return standings;
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++ STATISTICS +++++++++++++++++++++++++

    /**
     * Date has to be in format: YYYY-MM-DD.
     * If date parameter has the correct format (see above) this method returns the statistics regarding all matches
     * with league leagueId BEFORE the date (INCLUDED).
     */
    private ModelStatitics getTeamStatisticsBeforeDate(String teamId, String leagueId, String date){
        String unirestGet = Configs.getApiDomainURL() + "/statistics/" + leagueId + "/" + teamId;

        if ( date != null ){
            unirestGet += "/" + date;
        }

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return new EmptyModelStatistics(teamId);

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return new EmptyModelStatistics(teamId);

        JSONObject stats = JSON.getJSON(api, "stats");

        return JSONRetriever.getTeamStatistics(stats,teamId);
    }

    /**
     * Date has to be in format: YYYY-MM-DD.
     * If date parameter has the correct format (see above) this method returns the statistics regarding all matches
     * with league leagueId AFTER the date (EXCLUDED).
     */
    public ModelStatitics getTeamStatisticsAfterDate(String teamId, String leagueId, String date) {

        boolean correctDate = DateUtils.matchesDate(date, Configs.getFixtureInputDateFormat());
        if ( !correctDate ) return new EmptyModelStatistics(teamId);

        ModelStatitics totalStatistics = this.getTeamStatisticsBeforeDate(teamId, leagueId, null);

        ModelStatitics statiticsBeforeDate = this.getTeamStatisticsBeforeDate(teamId, leagueId, date);
        
        return totalStatistics.minus(statiticsBeforeDate);
    }

    // ++++++++++++++++++++++++++++++++++++++++++ TEAM +++++++++++++++++++++++++++++++++++++++++++
    /**
     * Returns all teams identified by an ID. There should only be one team identified by an ID but this method returns a list.
     */
    public List<Team> getTeamsByTeamId(String teamId){
        String unirestGet = Configs.getApiDomainURL() + "/teams/team/" + teamId;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj, "api");
        if ( api == null ) return Collections.emptyList();
        
        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of teams with teamId : " + teamId + " : " + results);

        JSONObject teams = JSON.getJSON(api, "teams");
        if (teams == null) return Collections.emptyList();

        return JSONRetriever.getTeamsFromJSONObject(teams);
    }

    //USA APILEAGUE
    List<Team> getAllTeams() {
        List<League> leagues = this.getLeagues();

        List<Team> teams = new ArrayList<>();
        leagues.forEach( league -> {
            List<Team> leagueTeams = this.getTeamsByLeagueId(league.getId());

            leagueTeams.forEach( leagueTeam -> {
                if ( !teams.contains(leagueTeam) ){
                    teams.add(leagueTeam);
                }
            });
        });

        return teams;
    }

    /**
     * Get all teams that play in one league identified by a league_id
     */
    List<Team> getTeamsByLeagueId(String leagueId) {
        String unirestGet = Configs.getApiDomainURL() + "/teams/league/" + leagueId;

        HttpResponse<JsonNode> response = UnirestUtils.get(unirestGet, null);
        if (response == null) return Collections.emptyList();

        JSONObject myObj = response.getBody().getObject();

        JSONObject api = JSON.getJSON(myObj, "api");
        if (api == null) return Collections.emptyList();

        int results = api.getInt("results");
        Logger.getLogger(this.getClass()).info("Number of teams that play in league with leagueId : " + leagueId + " : " + results);

        JSONObject teams = JSON.getJSON(api, "teams");
        if (teams == null) return Collections.emptyList();

        return JSONRetriever.getTeamsFromJSONObject(teams);
    }
}
