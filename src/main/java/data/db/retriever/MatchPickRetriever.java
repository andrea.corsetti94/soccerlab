package data.db.retriever;

import data.db.dao.DAOMatchPick;
import main.Logger;
import model.MatchPick;
import odds.Markets;

import java.util.List;

public class MatchPickRetriever {

    public List<MatchPick> retrieveMatchPicks(){
        DAOMatchPick daoFixture = new DAOMatchPick();
        return daoFixture.extract("SELECT * FROM MATCHPICK;");
    }

    public List<MatchPick> retrieveMatchPicksByFixture(String fixtureId){
        DAOMatchPick daoFixture = new DAOMatchPick();
        return daoFixture.extract("SELECT * FROM MATCHPICK WHERE FIXTUREID = ?;", fixtureId);
    }

    public MatchPick retrieveMatchPick(String fixtureId, Markets.MarketValue marketValue) {
        DAOMatchPick daoFixture = new DAOMatchPick();
        List<MatchPick> picks = daoFixture.extract("SELECT * FROM MATCHPICK WHERE FIXTUREID = ? AND " +
                        "MARKET = ?;", fixtureId, marketValue.toString());
        if ( picks == null || picks.isEmpty() ){
            Logger.getLogger(this.getClass()).warn("No Match pick retrieved for fixtureId " + fixtureId
                    + " and market value: " + marketValue);
            return null;
        }

        return picks.get(0);
    }

    public List<MatchPick> retrievePicksByOdds(double lowerBound, double upperBound){
        DAOMatchPick daoFixture = new DAOMatchPick();
        return daoFixture.extract("SELECT * FROM MATCHPICK WHERE ODDS <= ? AND ODDS >= ?;", upperBound, lowerBound);
    }

    public List<MatchPick> retrievePicksByChance(double lowerBound, double upperBound) {
        DAOMatchPick daoFixture = new DAOMatchPick();
        return daoFixture.extract("SELECT * FROM MATCHPICK WHERE CHANCE <= ? AND CHANCE >= ?;", upperBound, lowerBound);
    }

    public List<MatchPick> retrievePicksByValue(double lowerBound, double upperBound) {
        DAOMatchPick daoFixture = new DAOMatchPick();
        return daoFixture.extract("SELECT * FROM MATCHPICK WHERE PICKVALUE <= ? AND PICKVALUE >= ?;", upperBound, lowerBound);
    }

    public List<MatchPick> retrieveOpenPicks(){
        DAOMatchPick daoFixture = new DAOMatchPick();
        return daoFixture.extract("SELECT * FROM MATCHPICK " +
                "WHERE PICKRESULT IS NULL OR PICKRESULT == 'TO_BE_DEFINED';");
    }

    public List<MatchPick> retrieveOpenValuePicks(){
        DAOMatchPick daoFixture = new DAOMatchPick();
        return daoFixture.extract("SELECT * FROM MATCHPICK " +
                "WHERE (PICKRESULT IS NULL OR PICKRESULT == 'TO_BE_DEFINED')" +
                " AND PICKVALUE > 0;");
    }
}
