package data.db.retriever;

import data.db.DB;
import data.interfaces.IExtracter;
import main.Configs;
import main.Logger;
import scheduler.SchedulerType;
import utils.DateUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;


public class SchedulersDataRetriever implements IExtracter {

    public void writeUpdate(SchedulerType type) {

        Date now = Calendar.getInstance().getTime();
        String update = DateUtils.toString(now, Configs.getFixtureStringFormat());

        DB.write(
                "INSERT OR REPLACE INTO UPDATESCHEDULER VALUES(?,?);",
                type.getId(),
                update);

    }

    public String getLastUpdate(SchedulerType type) {
        String id = type.getId();

        Object lastUpd = DB.read(this,
                "SELECT LASTUPDATE FROM UPDATESCHEDULER WHERE SCHEDULERID = ?;",
                id);

        if ( lastUpd == null ){
            Logger.getLogger(this.getClass()).error("Null last update fetched. Returning empty last update.." );
            return "";
        }
        else if ( ! (lastUpd instanceof String) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a string. " +
                    "Returning empty last update.." );
            return "";
        }

        return (String) lastUpd;
    }

    @Override
    public Object fetch(ResultSet rs) throws SQLException {
        if (rs.next()) {
            return rs.getString(1);
        }
        return "";
    }
}
