package data.db.retriever;

import data.db.dao.DAOFixture;
import main.Configs;
import main.Logger;
import model.Fixture;
import utils.DateUtils;

import java.util.*;

public class FixtureDataRetriever {

    /**
     * @param day in format YYYY-MM-DD
     * @return fixtures played that day
     */
    public List<Fixture> getFixturesByDay(String day) {
        DAOFixture daoFixture = new DAOFixture();
        return daoFixture.extract("SELECT * FROM FIXTURE WHERE EVENTDATE LIKE ?" +
                " ORDER BY DATETIME(EVENTDATE) ASC;", day + "%");
    }

    public List<Fixture> getAllMatchesByLeague(String leagueId) {
        DAOFixture daoFixture = new DAOFixture();
        return daoFixture.extract("SELECT * FROM FIXTURE WHERE LEAGUEID = ?;", leagueId);
    }

    public List<Fixture> getAllMatchesByTeam(String teamId) {
        DAOFixture daoFixture = new DAOFixture();
        return daoFixture.extract(
                "SELECT * FROM FIXTURE WHERE HOMETEAMID = ? OR AWAYTEAMID = ?;", teamId, teamId
        );
    }

    public Fixture byFixtureId(String fixtureId){
        List<Fixture> fixtureList = getFixtureByFixtureId(fixtureId);
        if ( fixtureList == null || fixtureList.isEmpty() ){
            Logger.getLogger(this.getClass()).warn("No Fixtures with ID: " + fixtureId
             +" Null fixture returned");
            return null;
        }
        return fixtureList.get(0);
    }

    private List<Fixture> getFixtureByFixtureId(String fixtureId) {
        DAOFixture daoFixture = new DAOFixture();
        return daoFixture.extract("SELECT * FROM FIXTURE WHERE FIXTUREID = ?;", fixtureId);
    }
    /**
     * Returns a list of H2H matches between two teams among multiple seasons.
     * In these matches each team could have played as home team or away team.
     * @param teamId1 team1
     * @param teamId2 team2
     * @return matches played by team1 against team2
     */
    public List<Fixture> getH2H(String teamId1, String teamId2) {
        DAOFixture daoFixture = new DAOFixture();
        return daoFixture
                .extract("SELECT * FROM FIXTURE WHERE (HOMETEAMID = ? AND AWAYTEAMID = ?) OR (HOMETEAMID = ? AND AWAYTEAMID = ?);"
                ,teamId1, teamId2, teamId2, teamId1);
    }

    public List<Fixture> getLastTeamMatches(String teamId1, int numOfMatches, String... leaguesId){
        List<String> leaguesIdList;

        if ( leaguesId == null) leaguesIdList = new ArrayList<>();
        else leaguesIdList = Arrays.asList(leaguesId);

        DAOFixture daoFixture = new DAOFixture();
        String query;
        if ( leaguesIdList.isEmpty() ){
            query = "select * from Fixture " +
                    "where (homeTeamId = ? or awayTeamId = ?) " +
                    "and (statusShort = ? or status = ?) " +
                    "and datetime(eventDate) <= datetime('now') "+
                    "group by eventDate " +
                    "order by datetime(eventDate) desc " +
                    "limit ?;";

            return daoFixture.extract(query, teamId1, teamId1, "FT", "Match Finished", numOfMatches);
        }
        else{
            int leagues = leaguesIdList.size();

            String subQuery = "(";
            for(int i = 0; i < leagues; i++){
                subQuery += "?";
                if ( i != leagues - 1 ) subQuery += ",";
            }
            subQuery += ")";
            query = "select * from Fixture " +
                    "where (homeTeamId = ? or awayTeamId = ?) " +
                    "and (statusShort = ? or status = ?) " +
                    "and leagueId in " + subQuery +
                    " and datetime(eventDate) <= datetime('now') " +
                    " group by eventDate " +
                    " order by datetime(eventDate) desc " +
                    "limit ?;";

            List<Object> paramsList = new ArrayList<>();
            paramsList.add(teamId1);
            paramsList.add(teamId1);
            paramsList.add("FT");
            paramsList.add("Match Finished");

            for(int i = 0; i < leagues; i++){
                paramsList.add(leaguesIdList.get(i));
            }
            paramsList.add(numOfMatches);
            Object[] params = paramsList.toArray();

            return daoFixture.extract(query, params);

        }

    }

    public List<Fixture> getLiveFixtures() {
        DAOFixture daoFixture = new DAOFixture();

        Date date = Calendar.getInstance().getTime();

        String today = DateUtils.toString(date, Configs.getFixtureInputDateFormat());

        return daoFixture.extract(
                "select * from Fixture " +
                        "where eventDate like ?" +
                        " and statusShort in('1H', 'HT', '2H');", today + "%");
    }

}
