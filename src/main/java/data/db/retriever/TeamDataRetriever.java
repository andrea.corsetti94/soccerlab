package data.db.retriever;

import data.db.dao.DAOTeam;
import model.Team;

import java.util.List;

class TeamDataRetriever {

    public List<Team> getTeamsByTeamId(String teamId) {
        DAOTeam daoTeam = new DAOTeam();
        return daoTeam.extract("SELECT * FROM TEAM WHERE TEAMID = ?;", teamId);

    }

    public List<Team> getAllTeams() {
        DAOTeam daoTeam = new DAOTeam();
        return daoTeam.extract("SELECT * FROM TEAM;");
    }

    public List<Team> getTeamsByLeagueId(String leagueId) {
        DAOTeam daoTeam = new DAOTeam();
        return daoTeam.extract("SELECT * FROM TEAM WHERE TEAMID IN (" +
                "SELECT TEAMID FROM LEAGUETEAM WHERE LEAGUEID = ?);", leagueId);
    }

}
