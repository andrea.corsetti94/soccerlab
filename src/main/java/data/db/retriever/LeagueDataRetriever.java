package data.db.retriever;

import data.db.dao.DAOLeague;
import main.Logger;
import model.League;

import java.util.List;

public class LeagueDataRetriever {

    public List<League> getLeagues() {
        DAOLeague daoLeague = new DAOLeague();
        return daoLeague.extract("SELECT * FROM LEAGUE;");
    }

    private List<League> getLeaguesById(String id) {
        DAOLeague daoLeague = new DAOLeague();
        return daoLeague.extract("SELECT * FROM LEAGUE WHERE LEAGUEID = ?;", id);
    }

    public League getLeague(String id) {
        List<League> leagueList = this.getLeaguesById(id);
        if ( leagueList == null || leagueList.isEmpty() ){
            Logger.getLogger(this.getClass()).warn("No Leagues retrieved with id: " + id);
            return null;
        }
        return leagueList.get(0);
    }

    public List<League> getLeaguesBySeasonYear(String seasonYear) {
        DAOLeague daoLeague = new DAOLeague();
        return daoLeague.extract("SELECT * FROM LEAGUE WHERE SEASONYEAR = ?;", seasonYear);
    }

    public List<League> getLeaguesByCountryNameAllSeasons(String countryName) {
        DAOLeague daoLeague = new DAOLeague();
        return daoLeague.extract("SELECT * FROM LEAGUE WHERE COUNTRYNAME = ?;", countryName);
    }

    public List<League> getLeaguesByCountryNameAndSeason(String countryName, String seasonYear) {
        DAOLeague daoLeague = new DAOLeague();
        return daoLeague.extract("SELECT * FROM LEAGUE WHERE COUNTRYNAME = ? " +
                " AND SEASONYEAR = ?;", countryName, seasonYear);
    }

}
