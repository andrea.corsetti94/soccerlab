package data.db.retriever;

import data.db.dao.DAOStanding;
import main.Logger;
import model.Standing;

import java.util.List;

class StandingsDataRetriever {

    public Standing retrieveStandingsByLeague(String leagueId) {

        DAOStanding daoStanding = new DAOStanding();
        List<Standing> standings = daoStanding.extract("SELECT * FROM STANDINGPOSITION WHERE LEAGUEID = ?;", leagueId);

        if ( standings == null || standings.isEmpty() ){
            Logger.getLogger(this.getClass()).warn("No Standings retrieved for leagueId: " + leagueId);
            return null;
        }

        if ( standings.size() > 1 ){
            Logger.getLogger(this.getClass()).warn("Multiple Standings retrieved for leagueId: " + leagueId
            + "\nFirst standings returned");
        }
        return standings.get(0);

    }

    public List<Standing> retrieveAllStandings() {

        DAOStanding daoStanding = new DAOStanding();
        return daoStanding.extract("SELECT * FROM STANDINGPOSITION;");

    }


}
