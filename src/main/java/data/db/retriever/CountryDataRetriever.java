package data.db.retriever;

import data.db.dao.DAOCountry;
import model.Country;

import java.util.List;

class CountryDataRetriever {

    public List<Country> retrieveCountryList() {

        DAOCountry daoCountry = new DAOCountry();
        return daoCountry.extract("SELECT * FROM COUNTRY;");
    }

}
