package data.db.retriever;

import data.db.dao.DAOBets;
import model.Bet;

import java.util.List;

public class BetsDataRetriever {

    public List<Bet> retrieveAllBets(){
        DAOBets daoBets = new DAOBets();
        return daoBets.extract("SELECT * FROM BETS;");
    }

    public List<Bet> retrieveAllAlgoBets(String algoId){
        DAOBets daoBets = new DAOBets();
        return daoBets.extract("SELECT * FROM BETS WHERE ALGOID = ?;", algoId);
    }

    public List<Bet> retrieveAllAlgoBetsCompleted(String algoId){
        DAOBets daoBets = new DAOBets();
        return daoBets
                .extract("select bets.* from bets, (select fixtureId, eventDate from Fixture " +
                "where (statusShort = 'FT' or status = 'Match Finished') and date(eventDate) <= date('NOW') " +
                ") orderedFixtures " +
                "where orderedFixtures.fixtureId == bets.fixtureId AND " +
                "bets.algoId = ? " +
                "order by datetime(orderedFixtures.eventDate) desc;", algoId);
    }

    public List<Bet> retrieveLastAlgoBetsCompleted(String algoId, int limit){
        DAOBets daoBets = new DAOBets();
        return daoBets
                .extract("select bets.* from bets, (select fixtureId, eventDate from Fixture " +
                        "where (statusShort = 'FT' or status = 'Match Finished') and date(eventDate) <= date('NOW') " +
                        ") orderedFixtures " +
                        "where orderedFixtures.fixtureId == bets.fixtureId AND " +
                        "bets.algoId = ? " +
                        "order by datetime(orderedFixtures.eventDate) desc " +
                        "limit ?;", algoId, limit);

    }

    public double retrieveProfitForAlgo(String algoId){
        DAOBets daoBets = new DAOBets();
        return daoBets
                .extractDouble("select sum(profit) from Bets where algoId = ?;", algoId);
    }

    private double retrieveProfitForBetList(List<Bet> betList){
        double profit = 0;
        for(Bet bet: betList){
            profit += bet.getProfit();
        }
        return profit;
    }

    public double retriveROIForBetList(List<Bet> betList){
        double amountSum = 0;
        double profit = retrieveProfitForBetList(betList);

        for(Bet bet: betList){
            amountSum += bet.getAmount();
        }

        if ( amountSum == 0 ) return 0;
        return profit * 100 / amountSum;
    }

    public List<Bet> retrieveOpenBets(){
        DAOBets daoBets = new DAOBets();
        return daoBets.extract("SELECT * FROM BETS WHERE PROFIT IS NULL;");
    }

    /**
     * @param algoId, algorithm Id
     * @param fixtureId, match Id
     * @return true if algorithm @algoId has not placed a bet on a match @fixtureId yet, false otherwise
     */
    public boolean hasAlgoAlreadyPlacedBetOnFixture(String algoId, String fixtureId){
        DAOBets daoBets = new DAOBets();
        List<Bet> bets = daoBets.extract("SELECT * FROM BETS WHERE ALGOID = ? AND FIXTUREID = ?;"
                ,algoId,fixtureId);

        return ! bets.isEmpty();
    }
}
