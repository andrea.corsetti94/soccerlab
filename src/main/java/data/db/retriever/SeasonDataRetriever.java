package data.db.retriever;

import data.db.dao.DAOSeason;
import model.Season;

import java.util.List;

class SeasonDataRetriever {

    public List<Season> getSeasonsList() {
        DAOSeason daoSeason = new DAOSeason();
        return daoSeason.extract("SELECT * FROM SEASON;");
    }

}
