package data.db.retriever;

import data.db.dao.DAOAlgorithm;
import model.Algorithm;

import java.util.List;

public class AlgorithmDataRetriever {

    public List<Algorithm> retrieveAlgorithms(){
        DAOAlgorithm daoAlgorithm = new DAOAlgorithm();
        return daoAlgorithm.extract("SELECT * FROM ALGORITHM;");
    }
}
