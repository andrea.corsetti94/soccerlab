package data.db.retriever;

import data.db.dao.DAOFixture;
import model.Fixture;
import model.statistics.EmptyModelStatistics;
import model.statistics.ModelStatitics;
import model.statistics.StatisticsType;
import model.statistics.Stats;
import utils.FixturesUtils;

import java.util.List;

class StatisticsDataRetriever {

    /**
     * Date has to be in format: YYYY-MM-DD.
     * If date parameter has the correct format (see above) this method returns the statistics regarding all matches
     * with league leagueId BEFORE the date (INCLUDED).
     */
    public ModelStatitics getTeamStatisticsBeforeDate(String teamId, String leagueId, String date) {

        DAOFixture daoFixture = new DAOFixture();
        List<Fixture> fixtures = daoFixture.extract("SELECT * FROM FIXTURE WHERE " +
                "(HOMETEAMID = ? OR AWAYTEAMID = ?) AND LEAGUEID = ? AND DATETIME(EVENTDATE) <= ?;",
                teamId, teamId, leagueId, date);

        return evalStatistics(fixtures, teamId);

    }

    /**
     * Date has to be in format: YYYY-MM-DD.
     * If date parameter has the correct format (see above) this method returns the statistics regarding all matches
     * with league leagueId AFTER the date (EXCLUDED).
     */
    public ModelStatitics getTeamStatisticsAfterDate(String teamId, String leagueId, String date) {

        DAOFixture daoFixture = new DAOFixture();
        List<Fixture> fixtures = daoFixture.extract("SELECT * FROM FIXTURE WHERE " +
                        "(HOMETEAMID = ? OR AWAYTEAMID = ?) AND LEAGUEID = ?" +
                        " AND DATETIME(EVENTDATE) > ? AND (status is 'Match Finished' OR statusShort = 'FT');",
                teamId, teamId, leagueId, date);

        return evalStatistics(fixtures, teamId);

    }

    /**
     * Date has to be in format: YYYY-MM-DD.
     * this method returns the statistics regarding matches played by teamId and finished for leagueId
     */
    public ModelStatitics getTeamStatistics(String teamId, String leagueId) {

        DAOFixture daoFixture = new DAOFixture();
        List<Fixture> fixtures = daoFixture.extract("SELECT * FROM FIXTURE WHERE " +
                        "(HOMETEAMID = ? OR AWAYTEAMID = ?) AND LEAGUEID = ?" +
                        " AND (status is 'Match Finished' OR statusShort = 'FT');",
                teamId, teamId, leagueId);

        return evalStatistics(fixtures, teamId);

    }

    private ModelStatitics evalStatistics(List<Fixture> fixtures, String teamId){
        if ( fixtures.isEmpty() ) return new EmptyModelStatistics(teamId);

        int playedTotal = fixtures.size();
        int winsTotal = (int) fixtures.stream().filter( f -> f.winnerTeamId().equals(teamId) ).count();
        int lostTotal = (int) fixtures.stream().filter( f -> f.loserTeamId().equals(teamId) ).count();
        int drawTotal = playedTotal - winsTotal - lostTotal;
        int goalsForTotal = fixtures.stream().mapToInt( f -> f.getTeamGoalsFor(teamId)).sum();
        int goalsAgainstTotal = fixtures.stream().mapToInt( f -> f.getTeamGoalsConceived(teamId)).sum();

        List<Fixture> homeMatches = FixturesUtils.filterByHDA(fixtures, teamId, StatisticsType.HOME_STATISTICS);
        int playedHome = homeMatches.size();
        int winsHome = (int) homeMatches.stream().filter( f -> f.winnerTeamId().equals(teamId) ).count();
        int lostHome = (int) homeMatches.stream().filter( f -> f.loserTeamId().equals(teamId) ).count();
        int drawHome = playedTotal - winsTotal - lostTotal;
        int goalsForHome = homeMatches.stream().mapToInt( f -> f.getTeamGoalsFor(teamId)).sum();
        int goalsAgainstHome = homeMatches.stream().mapToInt( f -> f.getTeamGoalsConceived(teamId)).sum();


        List<Fixture> awayMatches = FixturesUtils.filterByHDA(fixtures, teamId, StatisticsType.AWAY_STATISTICS);
        int playedAway = awayMatches.size();
        int winsAway = (int) awayMatches.stream().filter( f -> f.winnerTeamId().equals(teamId) ).count();
        int lostAway = (int) awayMatches.stream().filter( f -> f.loserTeamId().equals(teamId) ).count();
        int drawAway = playedTotal - winsTotal - lostTotal;
        int goalsForAway = awayMatches.stream().mapToInt( f -> f.getTeamGoalsFor(teamId)).sum();
        int goalsAgainstAway = awayMatches.stream().mapToInt( f -> f.getTeamGoalsConceived(teamId)).sum();


        Stats totalStats = new Stats(StatisticsType.TOTAL_STATISTICS, playedTotal, winsTotal,
                drawTotal, lostTotal, goalsForTotal, goalsAgainstTotal);

        Stats homeStats = new Stats(StatisticsType.HOME_STATISTICS, playedHome, winsHome,
                drawHome, lostHome, goalsForHome, goalsAgainstHome);

        Stats awayStats = new Stats(StatisticsType.AWAY_STATISTICS, playedAway, winsAway,
                drawAway, lostAway, goalsForAway, goalsAgainstAway);


        return new ModelStatitics(teamId, homeStats, awayStats, totalStats);
    }
}
