package data.db;

import data.interfaces.IExtracter;
import main.Configs;
import main.Logger;
import utils.DBUtils;

import java.sql.*;

public class DB {

    private static Connection connection;

    static{
        init();
    }

    private static void init(){
        try {
            Class.forName("org.sqlite.JDBC");
            String dbPath = Configs.getDbPath() + Configs.getDbFilename();
            connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
        } catch (Exception e) {
            Logger.getLogger(DB.class).error("Application initializing a connection to the database.", e);
            System.exit(1);
        }
    }

    private static synchronized void close(AutoCloseable... objects){
        if ( objects != null && objects.length > 0 ){
            for( AutoCloseable ob: objects ){
                try {
                    if ( ob != null) ob.close();
                } catch (Exception e) {
                    Logger.getLogger(DB.class).error("DB error closing statement: ", e);
                }
            }
        }
    }

    private static synchronized Connection getConnection(){
        try {
            if ( connection == null || connection.isClosed() ){
                init();
            }
            return connection;
        } catch (Exception e) {
            Logger.getLogger(DB.class).error("Application initializing a connection to the database.", e);
            System.exit(1);
        }
        return null;
    }

    public static synchronized void executeBatch(DBStatements statements){
        Connection connection = getConnection();

        PreparedStatement preparedStatement = null;
        String statementString = statements.getStatement();
        try{
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(statementString);

            for(int i = 0; i < statements.getParams().size(); i++ ){
                Object[] params = statements.getParams().get(i);
                for (int j = 0; j < params.length; j++) {
                    preparedStatement.setObject(j + 1, params[j]);
                }
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();

            connection.commit();
            connection.setAutoCommit(true);
        }
        catch (SQLException e){
            Logger.getLogger(DB.class).error("Error executing: " + statementString, e);
        } finally {
            DB.close(preparedStatement, connection);
        }

        Logger.getLogger(DB.class).info("Correctly executed batch: " + statementString);

    }


    public static synchronized void write(String statement, Object... params ){

        Connection connection = getConnection();
        String sqlWrite = DBUtils.evalSQLString(statement, params);

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(statement);
            for (int i = 0; i < params.length; i++) {
                preparedStatement.setObject(i + 1, params[i]);
            }

            preparedStatement.executeUpdate();

            Logger.getLogger(DB.class).info("Correctly executed: " + sqlWrite);
        }
        catch (SQLException e){

            Logger.getLogger(DB.class).error("Error executing: " + sqlWrite, e) ;

        } finally {
            DB.close(preparedStatement, connection);
        }
    }

    public static synchronized Object read(IExtracter extracter, String statement, Object... params){
        Connection connection = getConnection();

        String sqlRead = DBUtils.evalSQLString(statement, params);

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(statement);
            int index = 1;
            for (Object ob : params) {
                preparedStatement.setObject(index++, ob);
            }
            resultSet = preparedStatement.executeQuery();
            return extracter.fetch(resultSet);

        } catch (SQLException e) {
            Logger.getLogger(DB.class).error("Error executing: " + sqlRead, e) ;
        }
        finally {
            DB.close(resultSet, preparedStatement, connection);
        }
        return null;
    }

}
