package data.db;


import java.util.List;

public class DBStatements {

    private final String statement;

    private final List<Object[]> params;

    public DBStatements(String statement, List<Object[]> params) {
        this.statement = statement;
        this.params = params;
    }


    public String getStatement() {
        return statement;
    }

    public List<Object[]> getParams() {
        return params;
    }
}
