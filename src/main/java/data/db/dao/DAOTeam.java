package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.Team;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOTeam implements DAO<Team>, IExtracter {

    @Override
    public void put(List<Team> items) {

        List<Object[]> statementParams = new ArrayList<>();
        for (Team team : items) {

            statementParams.add(new Object[]{
                    team.getId(),
                    team.getName(),
                    team.getImgUrl()
            });

        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO TEAM VALUES(?,?,?);",
                statementParams);

        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating teams... Finished.");

    }

    @Override
    public List<Team> extract(String statementString, Object... params) {

        Object teamList = DB.read(this, statementString, params);
        if ( teamList == null ){
            Logger.getLogger(this.getClass()).error("Null teamList fetched. Returning empty teamList.." );
            return Collections.emptyList();
        }
        else if ( ! (teamList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a teamList. " +
                    "Returning empty teamList.." );
            return Collections.emptyList();
        }

        return (List<Team>) teamList;
    }


    @Override
    public Object fetch(ResultSet queryResult) throws SQLException {
        List<Team> teams = new ArrayList<>();

        while (queryResult.next()) {
            int index = 1;

            String id = queryResult.getString(index++);
            String name = queryResult.getString(index++);
            String imgUrl = queryResult.getString(index);

            Team team = new Team(id, name, imgUrl);
            teams.add(team);
        }

        return teams;
    }
}
