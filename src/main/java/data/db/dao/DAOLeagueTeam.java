package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.LeagueTeam;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOLeagueTeam implements DAO<LeagueTeam>, IExtracter {


    @Override
    public void put(List<LeagueTeam> items) {

        List<Object[]> statementParams = new ArrayList<>();
        for (LeagueTeam leagueTeam : items) {

            statementParams.add(new Object[]{
                    leagueTeam.getLeagueId(),
                    leagueTeam.getTeamId()
            });

        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO LEAGUETEAM VALUES(?,?);",
                statementParams);

        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating leagueTeam... Finished.");

    }

    @Override
    public List<LeagueTeam> extract(String statementString, Object... params) {

        Object leagueTeamList = DB.read(this, statementString, params);
        if ( leagueTeamList == null ){
            Logger.getLogger(this.getClass()).error("Null leagueList fetched. Returning empty leagueTeamList.." );
            return Collections.emptyList();
        }
        else if ( ! (leagueTeamList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a leagueTeamList. " +
                    "Returning empty leagueTeamList.." );
            return Collections.emptyList();
        }

        return (List<LeagueTeam>) leagueTeamList;
    }

    @Override
    public Object fetch(ResultSet queryResult) throws SQLException {
        List<LeagueTeam> leagueTeams = new ArrayList<>();

        while (queryResult.next()) {
            int index = 1;

            String leagueId = queryResult.getString(index++);
            String teamId = queryResult.getString(index);

            LeagueTeam leagueTeam = new LeagueTeam(leagueId, teamId);
            leagueTeams.add(leagueTeam);
        }

        return leagueTeams;
    }
}
