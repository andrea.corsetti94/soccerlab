package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.Country;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOCountry implements DAO<Country>, IExtracter {

    @Override
    public void put(List<Country> items) {

        List<Object[]> statementParams = new ArrayList<>();
        for(Country country: items){

            statementParams.add(new Object[]{country.getId(), country.getName()});

        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO COUNTRY VALUES(?,?);",
                statementParams);

        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating countries... Finished.");

    }

    @Override
    public List<Country> extract(String statementString, Object... params) {

        Object countryList = DB.read(this, statementString, params);
        if ( countryList == null ){
            Logger.getLogger(this.getClass()).error("Null countryList fetched. Returning empty country list.." );
            return Collections.emptyList();
        }
        else if ( ! (countryList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a country list. " +
                    "Returning empty country list.." );
            return Collections.emptyList();
        }

        return (List<Country>) countryList;

    }

    @Override
    public Object fetch(ResultSet rs) throws SQLException {
        List<Country> countries = new ArrayList<>();

        while (rs.next()) {
            int index = 1;

            String id = rs.getString(index++);
            String countryName = rs.getString(index);

            Country country = new Country(id, countryName);
            countries.add(country);
        }

        return countries;
    }
}
