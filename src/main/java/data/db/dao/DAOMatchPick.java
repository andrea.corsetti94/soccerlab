package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.db.retriever.FixtureDataRetriever;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.Fixture;
import model.MatchPick;
import odds.Markets;
import odds.markets.MarketResults;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOMatchPick implements DAO<MatchPick>, IExtracter {

    @Override
    public void put(List<MatchPick> items) {
        List<Object[]> statementParams = new ArrayList<>();
        for(MatchPick matchPick: items){

            String fixtureId = matchPick.getFixtureId();

            Markets.MarketValue marketValue = matchPick.getMarketValue();
            double odds = matchPick.getOddValue();
            double chance = matchPick.getChance();
            double value = matchPick.getExpectedValue();

            statementParams.add(new Object[]{
                    fixtureId, marketValue, odds, chance, value, MarketResults.Result.TO_BE_DEFINED
            });

        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO MATCHPICK VALUES(?,?,?,?,?,?);",
                statementParams);

        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating match picks... Finished.");
    }

    @Override
    public List<MatchPick> extract(String statementString, Object... params) {

        Object matchPickList = DB.read(this, statementString, params);
        if ( matchPickList == null ){
            Logger.getLogger(this.getClass()).error("Null matchpick list fetched. Returning empty match pick list.." );
            return Collections.emptyList();
        }
        else if ( ! (matchPickList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a match pick list. " +
                    "Returning empty match pick list.." );
            return Collections.emptyList();
        }

        return (List<MatchPick>) matchPickList;
    }

    @Override
    public Object fetch(ResultSet rs) throws SQLException {
        List<MatchPick> matchPicks = new ArrayList<>();

        while (rs.next()) {
            int index = 1;

            String fixtureId = rs.getString(index++);
            String marketValueString = rs.getString(index++);
            double odds = rs.getDouble(index++);
            double chance = rs.getDouble(index++);

            //index + 1 because fetching skips "Pick value" attribute
            MarketResults.Result pickResult = MarketResults.Result.byName(rs.getString(index + 1));

            Markets.MarketValue marketValue = Markets.byName(marketValueString);

            MatchPick matchPick = new MatchPick(fixtureId, marketValue, odds, chance);
            matchPick.setResult(pickResult);

            matchPicks.add(matchPick);
        }

        return matchPicks;
    }

    public void updateResults(List<MatchPick> picks){

        List<Object[]> statementParams = new ArrayList<>();
        for (MatchPick matchPick : picks) {

            String fixtureId = matchPick.getFixtureId();
            Fixture fixture = new FixtureDataRetriever().byFixtureId(fixtureId);

            Markets.MarketValue marketValue = matchPick.getMarketValue();

            MarketResults.Result pickResult;
            if (matchPick.getResult() == null || matchPick.getResult() == MarketResults.Result.TO_BE_DEFINED) {
                pickResult = new MarketResults(fixture).result(marketValue);
            } else {
                continue;
            }

            statementParams.add(new Object[]{
                    pickResult, fixtureId, marketValue
            });

        }

        DBStatements statements = new DBStatements(
                "UPDATE MATCHPICK SET PICKRESULT = ? WHERE FIXTUREID = ? AND MARKET = ?;",
                statementParams);

        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating match picks results... Finished.");
    }
}
