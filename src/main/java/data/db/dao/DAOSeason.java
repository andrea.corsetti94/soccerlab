package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.Season;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOSeason implements DAO<Season>, IExtracter {

    @Override
    public void put(List<Season> items) {

        List<Object[]> statementParams = new ArrayList<>();
        for (Season season : items) {

            statementParams.add(new Object[]{
                    season.getYear()
            });

        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO SEASON VALUES(?);",
                statementParams);

        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating seasons... Finished.");

    }

    @Override
    public List<Season> extract(String statementString, Object... params) {

        Object seasonList = DB.read(this, statementString, params);
        if ( seasonList == null ){
            Logger.getLogger(this.getClass()).error("Null seasonList fetched. Returning empty seasonList.." );
            return Collections.emptyList();
        }
        else if ( ! (seasonList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a seasonList. " +
                    "Returning empty seasonList.." );
            return Collections.emptyList();
        }

        return (List<Season>) seasonList;
    }

    @Override
    public Object fetch(ResultSet queryResult) throws SQLException {
        List<Season> seasons = new ArrayList<>();

        while (queryResult.next()) {
            String year = queryResult.getString(1);

            Season season = new Season(year);
            seasons.add(season);
        }

        return seasons;
    }
}
