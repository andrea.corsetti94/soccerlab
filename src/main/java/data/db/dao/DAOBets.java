package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.db.retriever.MatchPickRetriever;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.Bet;
import model.MatchPick;
import odds.Markets;
import odds.markets.MarketResults;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOBets implements DAO<Bet>, IExtracter {

    @Override
    public void put(List<Bet> items) {
        List<Object[]> statementParams = new ArrayList<>();
        for(Bet bet: items){
            statementParams.add(new Object[]{
                bet.getAlgoId(), bet.getFixtureId(), bet.getMarketValue(), bet.getAmount(), bet.getOdds(), null
            });
            //we pass NULL as profit parameter. Profit parameter will be updated in another method
        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO BETS VALUES(?,?,?,?,?,?);",
                statementParams);

        DB.executeBatch(statements);
        Logger.getLogger(this.getClass()).info("Updating bets... Finished.");
    }

    @Override
    public List<Bet> extract(String statementString, Object... params) {
        Object betList = DB.read(this, statementString, params);
        if ( betList == null ){
            Logger.getLogger(this.getClass()).error("Null bets list fetched. Returning empty bet list.." );
            return Collections.emptyList();
        }
        else if ( ! (betList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a bet list. " +
                    "Returning empty bet list.." );
            return Collections.emptyList();
        }

        return (List<Bet>) betList;
    }

    public double extractDouble(String statementString, Object... params){
        Object doubleVal = DB.read(this, statementString, params);
        if ( doubleVal == null ){
            Logger.getLogger(this.getClass()).error("Null bets list fetched. Returning 0.." );
            return 0.0;
        }
        else if ( ! (doubleVal instanceof Double) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a double " +
                    "Returning 0.." );
            return 0.0;
        }

        return (Double) doubleVal;
    }

    @Override
    public Object fetch(ResultSet rs) throws SQLException {
        List<Bet> bets = new ArrayList<>();

        while (rs.next()) {
            int index = 1;

            String algoId = rs.getString(index++);
            String fixtureId = rs.getString(index++);
            Markets.MarketValue market = Markets.byName(rs.getString(index++));
            double amount = rs.getDouble(index++);
            double odds = rs.getDouble(index++);
            double profit = rs.getDouble(index);

            Bet bet = new Bet(algoId, fixtureId, market, amount, odds, profit);
            bets.add(bet);
        }

        return bets;
    }

    public void updateOpenBets(List<Bet> openBets){
        List<Object[]> statementParams = new ArrayList<>();

        MatchPickRetriever matchPickRetriever = new MatchPickRetriever();
        for(Bet bet: openBets){
            String algoId = bet.getAlgoId();
            String fixtureId = bet.getFixtureId();
            Markets.MarketValue market = bet.getMarketValue();

            MatchPick matchPick = matchPickRetriever.retrieveMatchPick(fixtureId, market);
            MarketResults.Result pickResult = matchPick.getResult();
            double pickOdds = bet.getOdds();

            double amount = bet.getAmount();

            double profit;
            if ( pickResult == null || pickResult == MarketResults.Result.TO_BE_DEFINED ) {
                continue;
            }
            else if ( pickResult == MarketResults.Result.YES ){
                profit = Math.max(0, amount * ( pickOdds - 1 ));
            }
            else if (pickResult == MarketResults.Result.NO ){
                profit = amount * -1;
            }
            else{
                continue;
            }

            statementParams.add(new Object[]{
                    profit, algoId, fixtureId, market
            });
        }

        DBStatements statements = new DBStatements(
                "UPDATE BETS SET PROFIT = ? WHERE ALGOID = ? AND FIXTUREID = ? AND MARKET = ?;",
                statementParams);

        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating bets results... Finished.");
    }
}
