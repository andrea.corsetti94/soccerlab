package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.League;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOLeague implements DAO<League>, IExtracter {

    @Override
    public void put(List<League> items) {

        List<Object[]> statementParams = new ArrayList<>();
        for(League league: items){

            statementParams.add(new Object[]{
                    league.getId(),
                    league.getName(),
                    league.getCountryName(),
                    league.getSeasonYear(),
                    league.getSeasonStartDate(),
                    league.getSeasonEndDate(),
                    league.getSeasonLogoUrl()
            });
        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO LEAGUE VALUES(?,?,?,?,?,?,?);",
                statementParams);

        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating leagues... Finished.");

    }

    @Override
    public List<League> extract(String statementString, Object... params) {

        Object leagueList = DB.read(this, statementString, params);
        if ( leagueList == null ){
            Logger.getLogger(this.getClass()).error("Null leagueList fetched. Returning empty leagueList.." );
            return Collections.emptyList();
        }
        else if ( ! (leagueList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a leagueList. " +
                    "Returning empty leagueList.." );
            return Collections.emptyList();
        }

        return (List<League>) leagueList;
    }


    @Override
    public Object fetch(ResultSet queryResult) throws SQLException {
        List<League> leagues = new ArrayList<>();

        while (queryResult.next()) {
            int index = 1;

            String id = queryResult.getString(index++);
            String leagueName = queryResult.getString(index++);
            String countryName = queryResult.getString(index++);
            String seasonYear = queryResult.getString(index++);
            String seasonStartDate = queryResult.getString(index++);
            String seasonEndDate = queryResult.getString(index++);
            String seasonLogoURL = queryResult.getString(index);

            League league = new League(id, leagueName, countryName, seasonYear,
                    seasonStartDate, seasonEndDate, seasonLogoURL);

            leagues.add(league);
        }

        return leagues;
    }
}
