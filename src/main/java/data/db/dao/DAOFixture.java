package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Configs;
import main.Logger;
import model.Fixture;
import utils.DateUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOFixture implements DAO<Fixture>, IExtracter {

    @Override
    public void put(List<Fixture> fixtures) {

        List<Object[]> statementParams = new ArrayList<>();
        for(Fixture fixture: fixtures){
            String eventDate = DateUtils.toString(fixture.getEventDate(), Configs.getFixtureStringFormat());

            statementParams.add( new Object[]{ fixture.getId(),
                    eventDate,
                    fixture.getLeagueId(),
                    fixture.getRound(),
                    fixture.getHomeTeamId(),
                    fixture.getHomeTeam(),
                    fixture.getAwayTeamId(),
                    fixture.getAwayTeam(),
                    fixture.getStatus(),
                    fixture.getStatusShort(),
                    fixture.getEventTimestamp(),
                    fixture.getGoalsHomeTeam(),
                    fixture.getGoalsAwayTeam(),
                    fixture.getHalfTimeScore(),
                    fixture.getFinalScore(),
                    fixture.getPenalty(),
                    fixture.getElapsed(),
                    fixture.getFirstHalfStart(),
                    fixture.getSecondHalfStart() }
                    );


        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO FIXTURE VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
                statementParams);
        DB.executeBatch(statements);

        Logger.getLogger(this.getClass()).info("Updating fixtures.... Finished.");

    }

    @Override
    public List<Fixture> extract(String statementString, Object... params) {

        Object fixtureList = DB.read(this, statementString, params);
        if ( fixtureList == null ){
            Logger.getLogger(this.getClass()).error("Null fixtureList fetched. Returning empty fixture list.." );
            return Collections.emptyList();
        }
        else if ( ! (fixtureList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a fixture list. " +
                    "Returning empty fixture list.." );
            return Collections.emptyList();
        }

        return (List<Fixture>) fixtureList;

    }

    @Override
    public Object fetch(ResultSet queryResult) throws SQLException {
        List<Fixture> fixtures = new ArrayList<>();

        while (queryResult.next()) {
            int index = 1;

            String id = queryResult.getString(index++);
            String eventdate = queryResult.getString(index++);
            String leagueId = queryResult.getString(index++);
            String round = queryResult.getString(index++);
            String hometeamId = queryResult.getString(index++);
            String hometeamName = queryResult.getString(index++);
            String awayTeamId = queryResult.getString(index++);
            String awayTeamName = queryResult.getString(index++);
            String status = queryResult.getString(index++);
            String statusShort = queryResult.getString(index++);
            String eventTimestamp = queryResult.getString(index++);
            String goalsHomeTeam = queryResult.getString(index++);
            String goalsAwayTeam = queryResult.getString(index++);
            String halfTimeScore = queryResult.getString(index++);
            String finalScore = queryResult.getString(index++);
            String penalty = queryResult.getString(index++);
            String elapsed = queryResult.getString(index++);
            String firstHalfStart = queryResult.getString(index++);
            String secondHalfStart = queryResult.getString(index);

            Fixture fixture = new Fixture(id, eventTimestamp, eventdate, leagueId, round, hometeamId, awayTeamId,
                    hometeamName, awayTeamName, status, statusShort, goalsHomeTeam, goalsAwayTeam,
                    halfTimeScore, finalScore, penalty, elapsed, firstHalfStart, secondHalfStart);

            fixtures.add(fixture);
        }

        return fixtures;
    }
}
