package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.Standing;
import model.StandingPosition;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class DAOStanding implements DAO<Standing>, IExtracter {

    @Override
    public void put(List<Standing> items) {

        List<Object[]> statementParams = new ArrayList<>();
        for (Standing standing : items) {
            if ( standing == null ) continue;

            List<StandingPosition> sps = standing.getPositions();
            String leagueId = standing.getLeagueId();

            sps.forEach(sp -> statementParams.add(new Object[]{
                    leagueId,
                    sp.getRank(),
                    sp.getTeamName(),
                    sp.getPlayed(),
                    sp.getWonMatches(),
                    sp.getDrawMatches(),
                    sp.getLostMatches(),
                    sp.getGoalsScored(),
                    sp.getGoalsReceived(),
                    sp.getGoalsDiff(),
                    sp.getPoints(),
                    sp.getGroup(),
                    sp.getLastUpd()
            }));

        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO STANDINGPOSITION VALUES " +
                        "(?,?,?,?,?,?,?,?,?,?,?,?,?);",
                statementParams);

        DB.executeBatch(statements);
        Logger.getLogger(this.getClass()).info("Updating standings... Finished.");

    }

    @Override
    public List<Standing> extract(String statementString, Object... params) {

        Object standingList = DB.read(this, statementString, params);
        if ( standingList == null ){
            Logger.getLogger(this.getClass()).error("Null standingList fetched. Returning empty standingList.." );
            return Collections.emptyList();
        }
        else if ( ! (standingList instanceof List) ){
            Logger.getLogger(this.getClass()).error("object fetched is not a standingList. " +
                    "Returning empty standingList.." );
            return Collections.emptyList();
        }

        return (List<Standing>) standingList;
    }

    @Override
    public Object fetch(ResultSet queryResult) throws SQLException {
        Map<String,List<StandingPosition>> map = new HashMap<>();

        while (queryResult.next()) {
            int index = 1;

            String leagueId = queryResult.getString(index++);
            String rank = queryResult.getString(index++);
            String teamName = queryResult.getString(index++);
            int played = queryResult.getInt(index++);
            int won = queryResult.getInt(index++);
            int draw = queryResult.getInt(index++);
            int lost = queryResult.getInt(index++);
            int goalsFor = queryResult.getInt(index++);
            int goalsAgainst = queryResult.getInt(index++);
            int goalsDiff = queryResult.getInt(index++);
            int points = queryResult.getInt(index++);
            String group = queryResult.getString(index++);
            String lastUpd = queryResult.getString(index);

            StandingPosition sp = new StandingPosition(rank, teamName, played, won,
                    draw, lost, goalsFor, goalsAgainst, goalsDiff, points, group, lastUpd);

            if ( ! map.containsKey(leagueId) ){
                map.put(leagueId, new ArrayList<>());
            }
            map.get(leagueId).add(sp);

        }

        List<Standing> standings = new ArrayList<>();

        for( String leagueId: map.keySet() ){
            Standing standing = new Standing(leagueId, map.get(leagueId));
            standings.add(standing);
        }

        return standings;
    }
}
