package data.db.dao;

import data.db.DB;
import data.db.DBStatements;
import data.interfaces.DAO;
import data.interfaces.IExtracter;
import main.Logger;
import model.Algorithm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAOAlgorithm implements DAO<Algorithm>, IExtracter {

    @Override
    public void put(List<Algorithm> items) {
        List<Object[]> statementParams = new ArrayList<>();
        for (Algorithm algorithm : items) {
            statementParams.add(new Object[]{
                    algorithm.getIdName()
            });
        }

        DBStatements statements = new DBStatements(
                "INSERT OR REPLACE INTO ALGORITHM VALUES(?);",
                statementParams);

        DB.executeBatch(statements);
        Logger.getLogger(this.getClass()).info("Updating algorithms... Finished.");
    }

    @Override
    public List<Algorithm> extract(String statementString, Object... params) {
        Object algoList = DB.read(this, statementString, params);
        if (algoList == null) {
            Logger.getLogger(this.getClass()).error("Null algo list fetched. Returning empty algo list..");
            return Collections.emptyList();
        } else if (!(algoList instanceof List)) {
            Logger.getLogger(this.getClass()).error("object fetched is not an algo list. " +
                    "Returning empty algo list..");
            return Collections.emptyList();
        }

        return (List<Algorithm>) algoList;
    }

    @Override
    public Object fetch(ResultSet rs) throws SQLException {
        List<Algorithm> algos = new ArrayList<>();

        while (rs.next()) {
            String algoId = rs.getString(1);
            Algorithm algo = new Algorithm(algoId);
            algos.add(algo);
        }

        return algos;
    }
}
