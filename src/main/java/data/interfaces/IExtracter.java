package data.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IExtracter {
    Object fetch(ResultSet rs) throws SQLException;
}
