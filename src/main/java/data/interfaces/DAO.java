package data.interfaces;

import java.util.List;

public interface DAO<T> {

    void put(List<T> items);
    List<T> extract(String statementString, Object... params);

}
