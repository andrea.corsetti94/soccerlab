package data;

import data.db.dao.*;
import data.db.retriever.BetsDataRetriever;
import data.db.retriever.FixtureDataRetriever;
import data.db.retriever.MatchPickRetriever;
import data.interfaces.DAO;
import main.Configs;
import main.Logger;
import model.*;
import odds.Markets;
import odds.MatchOdds;
import odds.OddsRetriever;
import prediction.BetsManager;
import prediction.MatchProbability;
import prediction.PoissonPredictor;
import prediction.excel.ExcelFixtureWriter;
import utils.DateUtils;
import utils.MathUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DataUpdater {

    /**
     * Retrieve all fixtures from API and stores them on DB.
     * If already stored nothing happens, if values change they are updated, if they are not present they get
     * inserted. --------------------- 12 minutes
     */
    public static void updateAllFixtures(){
        String format = Configs.getFixtureInputDateFormat();

        Calendar futureLimit = Calendar.getInstance();
        futureLimit.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR) + 1);
        futureLimit.set(Calendar.MONTH, 11);
        futureLimit.set(Calendar.DATE, 31);
        Date futureLimitDate = futureLimit.getTime();

        Calendar pastLimit = Calendar.getInstance();
        pastLimit.set(Calendar.YEAR, 2015);
        pastLimit.set(Calendar.MONTH, 11);
        pastLimit.set(Calendar.DATE, 31);

        Date limitDate = pastLimit.getTime();

        Date tmpDate = futureLimitDate;
        while( tmpDate.after(limitDate) ){

            String dateString = DateUtils.toString(tmpDate,format);

            List<Fixture> matches = new API().getFixturesByDay(dateString);
            DAO<Fixture> daoFixture = new DAOFixture();
            daoFixture.put(matches);

            tmpDate = DateUtils.advanceDays(tmpDate, -1);
            Logger.getLogger(DataUpdater.class).info("* * DB Fixture Update completed for day: "+dateString+" * *");

        }

        Logger.getLogger(DataUpdater.class).info("* * * * * * * DB AllFixtures Update completed * * * * * * * *");

    }

    /**
     * Retrieve fixtures from API in a period that goes two weeks prior to two weeks forward, and stores them on DB.
     * If already stored nothing happens, if values change they are updated, if they are not present they get
     * inserted.
     */
    public static void updateCloseFixtures(){
        String format = Configs.getFixtureInputDateFormat();

        Date today = Calendar.getInstance().getTime();
        Date futureLimit = DateUtils.advanceDays(today, 14);
        Date pastLimit = DateUtils.advanceDays(today, -14);


        Date tmpDate = futureLimit;
        while( tmpDate.after(pastLimit) ){

            String dateString = DateUtils.toString(tmpDate,format);

            List<Fixture> matches = new API().getFixturesByDay(dateString);
            DAO<Fixture> daoFixture = new DAOFixture();
            daoFixture.put(matches);

            tmpDate = DateUtils.advanceDays(tmpDate, -1);
        }

        Logger.getLogger(DataUpdater.class).info("* * * * * * * DB Current Fixtures Update completed * * * * * * * *");

    }

    /**
     * Retrieve fixtures from API to play or played today, yesterday and tomorrow, and stores them on DB.
     * If already stored nothing happens, if values change they are updated, if they are not present they get
     * inserted.
     */
    public static void updateLiveFixtures() {
        String format = Configs.getFixtureInputDateFormat();

        Date today = Calendar.getInstance().getTime();
        Date futureLimit = DateUtils.advanceDays(today, 1);
        Date pastLimit = DateUtils.advanceDays(today, -2);

        Date tmpDate = futureLimit;
        while (tmpDate.after(pastLimit)) {

            String dateString = DateUtils.toString(tmpDate, format);

            List<Fixture> matches = new API().getFixturesByDay(dateString);
            DAO<Fixture> daoFixture = new DAOFixture();
            daoFixture.put(matches);

            tmpDate = DateUtils.advanceDays(tmpDate, -1);
        }

        Logger.getLogger(DataUpdater.class).info("* * * * * * * DB Current Fixtures Update completed * * * * * * * *");

    }

    public static void updateCountries(){
        DAO<Country> dao = new DAOCountry();
        dao.put(new API().retrieveCountryList());

        Logger.getLogger(DataUpdater.class).info("* * * * * * * DB Country Update completed * * * * * * * *");

    }

    public static void updateLeagues(){
        DAO<League> dao = new DAOLeague();
        dao.put( new API().getLeagues() );

        Logger.getLogger(DataUpdater.class).info("* * * * * * * DB League Update completed * * * * * * * *");

    }

    public static void updateSeasons(){
        DAO<Season> dao = new DAOSeason();
        dao.put(new API().getSeasonsList());

        Logger.getLogger(DataUpdater.class).info("* * * * * * * DB Season Update completed * * * * * * * *");

    }

    /**
     * + + + + + + + 4 minutes
     */
    public static void updateAllStandings(){
        DAO<Standing> dao = new DAOStanding();
        dao.put(new API().retrieveAllStandings());

        Logger.getLogger(DataUpdater.class).info("* * * * * * * DB Standings Update completed * * * * * * * *");

    }

    public static void updateLeagueStandings(String leagueId){

        Standing standing = new API().retrieveStandingsByLeague(leagueId);

        if ( standing == null ){
            Logger.getLogger(DataUpdater.class).warn("Null standings for league: " + leagueId);
            return;
        }

        List<Standing> standings = new ArrayList<>();
        standings.add(standing);

        DAO<Standing> dao = new DAOStanding();
        dao.put(standings);

        Logger.getLogger(DataUpdater.class).info("* * DB Standings Update completed for leagueId: "+leagueId+" * *");
    }

    /**
     *  + + + + + + + + + + 6 minutes
     */
    public static void updateAllTeams(){
        DAO<Team> daoTeam = new DAOTeam();
        daoTeam.put(new API().getAllTeams());

        Logger.getLogger(DataUpdater.class).info("* * * * * * * DB Teams Update completed * * * * * * * *");

    }


    /**
     * + + + + + + + + + + + + 4 minutes
     */
    public static void updateLeagueTeams(){

        API api = new API();
        List<League> leagues = api.getLeagues();

        List<LeagueTeam> leagueTeams = new ArrayList<>();
        leagues.forEach( league -> {
            List<Team> teams = api.getTeamsByLeagueId(league.getId());

            teams.forEach( team -> {
                LeagueTeam leagueTeam = new LeagueTeam(league.getId(), team.getId());
                leagueTeams.add(leagueTeam);
            });
        });

        DAO<LeagueTeam> daoLeagueTeam = new DAOLeagueTeam();
        daoLeagueTeam.put(leagueTeams);
    }

    /**
     * This method retrieves all fixtures in the next 2 days, fetch every match odd (for every legit market),
     * computes all probabilities (for each market) and store them
     * */
    public static void updateMatchPicks(){

        int daysToConsider = 5;
        List<Fixture> allMatches = new ArrayList<>();

        String format = Configs.getFixtureInputDateFormat();

        Date today = Calendar.getInstance().getTime();
        Date futureLimit = DateUtils.advanceDays(today, daysToConsider);

        Date tmpDate = today;
        while (tmpDate.before(futureLimit)) {

            String dateString = DateUtils.toString(tmpDate, format);

            List<Fixture> matches = new FixtureDataRetriever().getFixturesByDay(dateString);
            allMatches.addAll(matches);

            tmpDate = DateUtils.advanceDays(tmpDate, 1);
        }

        //now allMatches contains every match.
        List<MatchPick> matchPicks = new ArrayList<>();
        API api = new API();
        for(Fixture match: allMatches){

            MatchOdds odds = api.getOddsForFixture(match.getId());

            if (odds.isLegit()) {
                MatchProbability mp = PoissonPredictor.getMatchProbability(match);

                for (Markets.MarketValue mv : Markets.validMarketValues()) {
                    double mvOdds = new OddsRetriever().fecthFixtureOdds(match, odds, mv);
                    if (mvOdds <= 0) continue;

                    double chance = PoissonPredictor.predictEvent(mp, mv);

                    MatchPick matchPick =  new MatchPick(
                            match.getId(), mv, MathUtils.round(mvOdds, 2), MathUtils.round(chance, 2));
                    matchPicks.add(matchPick);

                }
            }
        }

        List<MatchPick> alreadyPresentPicks = new MatchPickRetriever().retrieveMatchPicks();

        List<MatchPick> picksNotAlreadyStored = new ArrayList<>();
        for(MatchPick pick: matchPicks){
            if ( !alreadyPresentPicks.contains(pick) ){
                picksNotAlreadyStored.add(pick);
            }
        }

        DAOMatchPick daoMatchPick = new DAOMatchPick();
        daoMatchPick.put(picksNotAlreadyStored);
    }

    /**
     * Set match pick results for finished matches and updates bets
     */
    public static void updatePickBetResults(){
        List<MatchPick> openPicks = new MatchPickRetriever().retrieveOpenPicks();

        if ( openPicks == null || openPicks.isEmpty() ) return;
        DAOMatchPick daoMatchPick = new DAOMatchPick();
        daoMatchPick.updateResults(openPicks);

        //update related bets
        List<Bet> openBets = new BetsDataRetriever().retrieveOpenBets();
        DAOBets daoBets = new DAOBets();
        daoBets.updateOpenBets(openBets);
    }

    /**
     * Place new bets
     */
    public static void updateBets(){
        DAOBets daoBets = new DAOBets();
        BetsManager.placeBets(daoBets);
    }

    public static void updateExcels(){
        int days = 5;

        Date todayDate = Calendar.getInstance().getTime();

        Date tmpDate = todayDate;
        Date futureDate = DateUtils.advanceDays(todayDate, days);
        while (tmpDate.before(futureDate)) {
            String dayString = DateUtils.toString(tmpDate, Configs.getFixtureInputDateFormat());
            List<Fixture> fixtures = new FixtureDataRetriever().getFixturesByDay(dayString);

            ExcelFixtureWriter writer = new ExcelFixtureWriter();
            writer.writeFixtureList(fixtures, dayString);

            tmpDate = DateUtils.advanceDays(tmpDate, 1);
            Logger.getLogger(DataUpdater.class).info("Matches on " + dayString + " stored.");
        }
    }

    public static void updateNow(){
        DataUpdater.updateCloseFixtures();
        DataUpdater.updatePickBetResults();
        DataUpdater.updateMatchPicks();
        DataUpdater.updateBets();

        DataUpdater.updateExcels();
    }

    public static void updateComplete(){

        DataUpdater.updateSeasons();
        DataUpdater.updateCountries();
        DataUpdater.updateLeagues();
        DataUpdater.updateAllTeams();
        DataUpdater.updateLeagueTeams();
        DataUpdater.updateAllStandings();

        DataUpdater.updateAllFixtures();
        DataUpdater.updatePickBetResults();
        DataUpdater.updateMatchPicks();
        DataUpdater.updateBets();
        DataUpdater.updateExcels();
    }
}
