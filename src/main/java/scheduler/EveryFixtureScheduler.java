package scheduler;

import data.db.retriever.SchedulersDataRetriever;
import data.DataUpdater;
import main.Logger;


class EveryFixtureScheduler extends Thread {

    private static final long PERIOD = (long) 1000 * 60 * 60 * 24 * 7;

    private final SchedulersDataRetriever SchedulersDataRetriever;
    private final SchedulerType type = SchedulerType.EVERYFIXTURE;

    EveryFixtureScheduler(){
        SchedulersDataRetriever = new SchedulersDataRetriever();
    }

    @Override
    public void run() {

        while (true) {
            try {
                String lastUpd = SchedulersDataRetriever.getLastUpdate(type);

                long toWaitForNextUpdate = SchedulerManager.isTimeToUpdate(lastUpd, PERIOD);
                if (toWaitForNextUpdate == 0) {
                    this.update();
                } else {
                    Thread.sleep(toWaitForNextUpdate);
                }
            } catch (InterruptedException e) {
                Logger.getLogger(this.getClass()).warn("Every fixtures scheduler thread has been interrupted: ", e);
                Thread.currentThread().interrupt();
            }
        }

    }

    private void update(){

        DataUpdater.updateAllFixtures();
        SchedulersDataRetriever.writeUpdate(type);

    }

}
