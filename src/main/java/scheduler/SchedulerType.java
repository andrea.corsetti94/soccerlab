package scheduler;

public enum SchedulerType {

    COUNTRY, CLOSEFIXTURE, LIVEFIXTURE, EVERYFIXTURE, LEAGUE, TEAM, SEASON, STANDING, DBBACKUP, MATCHPICK,
    BETS, EXCEL;

    public String getId() {
        return this.toString();
    }



}
