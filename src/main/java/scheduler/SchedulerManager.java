package scheduler;

import main.Configs;
import utils.DateUtils;

import java.util.Calendar;
import java.util.Date;

public class SchedulerManager {

    private static final Thread countryScheduler;
    private static final Thread closeFixtureScheduler;
    private static final Thread liveFixtureScheduler;
    private static final Thread everyFixtureScheduler;
    private static final Thread leagueScheduler;
    private static final Thread seaonsScheduler;
    private static final Thread standingsScheduler;
    private static final Thread teamScheduler;
    private static final Thread dbBackupScheduler;
    private static final Thread matchPicksScheduler;
    private static final Thread betsScheduler;
    private static final Thread excelScheduler;

    static {
        countryScheduler = new CountryScheduler();
        closeFixtureScheduler = new CloseFixtureScheduler();
        liveFixtureScheduler = new LiveFixtureScheduler();
        everyFixtureScheduler = new EveryFixtureScheduler();
        leagueScheduler = new LeagueScheduler();
        seaonsScheduler = new SeasonScheduler();
        standingsScheduler = new StandingsScheduler();
        teamScheduler = new TeamScheduler();
        dbBackupScheduler = new DBBackupScheduler();
        matchPicksScheduler = new MatchPickScheduler();
        betsScheduler = new BetScheduler();
        excelScheduler = new ExcelScheduler();
    }

    public static void startSchedulerExecution(){
        liveFixtureScheduler.start();
        countryScheduler.start();
        closeFixtureScheduler.start();
        everyFixtureScheduler.start();
        leagueScheduler.start();
        seaonsScheduler.start();
        standingsScheduler.start();
        teamScheduler.start();
        dbBackupScheduler.start();
        matchPicksScheduler.start();
        betsScheduler.start();
        excelScheduler.start();
    }

    public static void stopSchedulerExecution(){
        liveFixtureScheduler.interrupt();
        countryScheduler.interrupt();
        closeFixtureScheduler.interrupt();
        everyFixtureScheduler.interrupt();
        leagueScheduler.interrupt();
        seaonsScheduler.interrupt();
        standingsScheduler.interrupt();
        teamScheduler.interrupt();
        dbBackupScheduler.interrupt();
        matchPicksScheduler.interrupt();
        betsScheduler.interrupt();
        excelScheduler.interrupt();
    }

    /**
     * Returns the milliseconds to wait to update according to the period taken in input and
     * the last update done
     * @param lastUpd extracted from DB.
     * @param periodInMilliseconds period of time (in milliseconds) to wait to activate a scheduler
     * @return 0 if it is time to update (lastUpd + periodInMilliseconds <= today)
     * or the milliseconds to wait for updating
     */
    static long isTimeToUpdate(String lastUpd, long periodInMilliseconds){
        if (lastUpd == null || lastUpd.isEmpty() ) return 0;

        Date lastUpdateDate = DateUtils.formatDate(lastUpd, Configs.getFixtureStringFormat());
        if ( lastUpdateDate == null ) return 0; //if there are problems with date format, update again
        Date today = Calendar.getInstance().getTime();

        long lastUpdateLong = lastUpdateDate.getTime();
        long todayLong = today.getTime();

        return Math.max(0, lastUpdateLong + periodInMilliseconds - todayLong);
    }

}
