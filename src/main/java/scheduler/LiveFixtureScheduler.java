package scheduler;

import data.DataUpdater;
import data.db.retriever.SchedulersDataRetriever;
import main.Logger;

class LiveFixtureScheduler extends Thread {

    private static final long PERIOD = (long) 1000 * 60 ; //1 minuto

    private final data.db.retriever.SchedulersDataRetriever SchedulersDataRetriever;
    private final SchedulerType type = SchedulerType.LIVEFIXTURE;

    LiveFixtureScheduler() {
        SchedulersDataRetriever = new SchedulersDataRetriever();
    }

    @Override
    public void run() {

        while (true) {
            try {
                String lastUpd = SchedulersDataRetriever.getLastUpdate(type);

                long toWaitForNextUpdate = SchedulerManager.isTimeToUpdate(lastUpd, PERIOD);
                if (toWaitForNextUpdate == 0) {
                    this.update();
                } else {
                    Thread.sleep(toWaitForNextUpdate);
                }
            } catch (InterruptedException e) {
                Logger.getLogger(this.getClass()).warn("Live fixtures scheduler thread has been interrupted: ", e);
                Thread.currentThread().interrupt();
            }
        }

    }

    private void update() {

        DataUpdater.updateLiveFixtures();
        SchedulersDataRetriever.writeUpdate(type);

    }
}
