package scheduler;

import data.DataUpdater;
import data.db.retriever.SchedulersDataRetriever;
import main.Logger;

class BetScheduler extends Thread {

    private static final long PERIOD = (long) 1000 * 60 * 60 * 12; //12 hours

    private final data.db.retriever.SchedulersDataRetriever SchedulersDataRetriever;
    private final SchedulerType type = SchedulerType.BETS;

    BetScheduler(){
        SchedulersDataRetriever = new SchedulersDataRetriever();
    }

    @Override
    public void run(){

        while(true){
            try{
                String lastUpd = SchedulersDataRetriever.getLastUpdate(type);

                long toWaitForNextUpdate = SchedulerManager.isTimeToUpdate(lastUpd, PERIOD);
                if ( toWaitForNextUpdate == 0 ){
                    this.update();
                }
                else{
                    Thread.sleep(toWaitForNextUpdate);
                }
            }
            catch(InterruptedException e){
                Logger.getLogger(this.getClass()).warn("Bets scheduler thread has been interrupted: ",e);
                Thread.currentThread().interrupt();
            }
        }

    }

    private void update(){
        DataUpdater.updateBets();
        SchedulersDataRetriever.writeUpdate(type);
    }
}
