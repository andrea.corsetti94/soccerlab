package scheduler;

import data.db.retriever.SchedulersDataRetriever;
import main.Configs;
import main.Logger;
import utils.DateUtils;
import utils.FileUtils;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

class DBBackupScheduler extends Thread {

    private static final long PERIOD = (long) 1000 * 60 * 60 * 24;

    private final data.db.retriever.SchedulersDataRetriever SchedulersDataRetriever;
    private final SchedulerType type = SchedulerType.DBBACKUP;

    DBBackupScheduler(){
        SchedulersDataRetriever = new SchedulersDataRetriever();
    }

    @Override
    public void run(){

        while(true){
            try{
                String lastUpd = SchedulersDataRetriever.getLastUpdate(type);

                long toWaitForNextUpdate = SchedulerManager.isTimeToUpdate(lastUpd, PERIOD);
                if ( toWaitForNextUpdate == 0 ){
                    this.update();
                }
                else{
                    Thread.sleep(toWaitForNextUpdate);
                }
            }
            catch(InterruptedException e){
                Logger.getLogger(this.getClass()).warn("DB Backup scheduler thread has been interrupted: ",e);
                Thread.currentThread().interrupt();
            }
        }

    }


    private void update(){

        //backs up database file
        this.backUp();

        //deletes files with last modified date older than a week
        this.deleteOldFiles();

        SchedulersDataRetriever.writeUpdate(type);
    }

    /**
     * Deletes every file under DB directory that is older than a week.
     * The "age" is defined by last update time.
     */
    private void deleteOldFiles(){
        long timePeriod = 1000 * 60 * 60 * 24 * 7; //ONE WEEK

        File folder = new File(Configs.getDbPath());
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles == null || listOfFiles.length == 0) {
            Logger.getLogger(this.getClass()).info("No files to delete.");
            return;
        }

        Date now = Calendar.getInstance().getTime();
        for (int i = 0; i < listOfFiles.length; i++) {

            File currentFile = listOfFiles[i];

            if (currentFile.getName().equals(Configs.getDbFilename())) {
                continue; //DO NOT DELETE ORIGINAL DB FILE
            }

            Date lastModified = FileUtils.lastModified(currentFile, Configs.getFixtureStringFormat());
            boolean toDelete = DateUtils.dateDifferenceGreaterThan(now, lastModified, timePeriod);

            if (toDelete) {

                //delete currentFile
                if (currentFile.delete()) {
                    Logger.getLogger(this.getClass()).info(currentFile.getName() + " has been deleted.");
                } else {
                    Logger.getLogger(this.getClass()).warn(currentFile.getName() + " has NOT been deleted.");
                }

            }
        }
    }

    /**
     * Backs up current DB in another file under the same directory
     */
    private void backUp(){
        Date now = Calendar.getInstance().getTime();

        String dbPath = Configs.getDbPath() + Configs.getDbFilename();
        String copiedDbName = Configs.getDbPath() + "backup_" +
                DateUtils.toString(now, Configs.getFixtureInputDateFormat()) + "_" + Configs.getDbFilename();
        FileUtils.copy(dbPath, copiedDbName);
    }
}
