package scheduler;

import data.DataUpdater;
import data.db.retriever.SchedulersDataRetriever;
import main.Logger;

class MatchPickScheduler extends Thread {

    private static final long PERIOD = (long) 1000 * 60 * 60 * 12; // 12 ore

    private final data.db.retriever.SchedulersDataRetriever SchedulersDataRetriever;
    private final SchedulerType type = SchedulerType.MATCHPICK;

    MatchPickScheduler() {
        SchedulersDataRetriever = new SchedulersDataRetriever();
    }

    @Override
    public void run() {

        while (true) {
            try {
                String lastUpd = SchedulersDataRetriever.getLastUpdate(type);

                long toWaitForNextUpdate = SchedulerManager.isTimeToUpdate(lastUpd, PERIOD);
                if (toWaitForNextUpdate == 0) {
                    this.update();
                } else {
                    Thread.sleep(toWaitForNextUpdate);
                }
            } catch (InterruptedException e) {
                Logger.getLogger(this.getClass()).warn("Match Pick scheduler thread has been interrupted: ", e);
                Thread.currentThread().interrupt();
            }
        }

    }

    private void update() {
        DataUpdater.updateMatchPicks();
        DataUpdater.updatePickBetResults();
        SchedulersDataRetriever.writeUpdate(type);
    }
}
