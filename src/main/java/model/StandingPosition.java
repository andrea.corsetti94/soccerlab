package model;

public class StandingPosition {

    private final String rank;
    private final String teamName;
    private final int played;
    private final int wonMatches;
    private final int drawMatches;
    private final int lostMatches;
    private final int goalsScored;
    private final int goalsReceived;
    private final int goalsDiff;
    private final int points;
    private final String group;
    private final String lastUpd;

    public StandingPosition(String rank, String teamName, int played, int wonMatches, int drawMatches, int lostMatches,
              int goalsScored, int goalsReceived, int goalsDiff, int points, String group, String lastUpd) {

        this.rank = rank;
        this.teamName = teamName;
        this.played = played;
        this.wonMatches = wonMatches;
        this.drawMatches = drawMatches;
        this.lostMatches = lostMatches;
        this.goalsScored = goalsScored;
        this.goalsReceived = goalsReceived;
        this.goalsDiff = goalsDiff;
        this.points = points;
        this.group = group;
        this.lastUpd = lastUpd;
    }

    public String getRank() {
        return rank;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getPlayed() {
        return played;
    }

    public int getWonMatches() {
        return wonMatches;
    }

    public int getDrawMatches() {
        return drawMatches;
    }

    public int getLostMatches() {
        return lostMatches;
    }

    public int getGoalsScored() {
        return goalsScored;
    }

    public int getGoalsReceived() {
        return goalsReceived;
    }

    public int getGoalsDiff() {
        return goalsDiff;
    }

    public int getPoints() {
        return points;
    }

    public String getGroup() {
        return group;
    }

    public String getLastUpd() {
        return lastUpd;
    }

    @Override
    public String toString() {
        return "StandingPosition{" +
                "  rank=" + rank +
                ", teamName='" + teamName + '\'' +
                ", points=" + points +
                ", played=" + played +
                ", wonMatches=" + wonMatches +
                ", drawMatches=" + drawMatches +
                ", lostMatches=" + lostMatches +
                ", goalsScored=" + goalsScored +
                ", goalsReceived=" + goalsReceived +
                ", goalsDiff=" + goalsDiff +
                ", group=" + group +
                '}'; 
    }


}
