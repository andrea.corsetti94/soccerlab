package model;

import main.Configs;
import utils.DateUtils;

import java.util.Date;
import java.util.Objects;

public class Fixture {

    private final String id;
    private final String eventTimestamp;
    private final Date eventDate;
    private final String leagueId;
    private final String round;
    private final String homeTeamId;
    private final String awayTeamId;
    private final String homeTeam;
    private final String awayTeam;
    private final String status;
    private final String statusShort;
    private final String goalsHomeTeam;
    private final String goalsAwayTeam;
    private final String halfTimeScore;
    private final String finalScore;
    private final Object penalty;
    private final String elapsed;
    private final String firstHalfStart;
    private final String secondHalfStart;


    public Fixture(String id, String eventTimestamp, String eventDate, String leagueId,
                   String round, String homeTeamId, String awayTeamId, String homeTeam,
                   String awayTeam, String status, String statusShort, String goalsHomeTeam,
                   String goalsAwayTeam, String halfTimeScore, String finalScore, Object penalty,
                   String elapsed, String firstHalfStart, String secondHalfStart) {

        this.id = id;
        this.eventTimestamp = eventTimestamp;
        this.eventDate = DateUtils.formatFixtureDate(eventDate);
        this.leagueId = leagueId;
        this.round = round;
        this.homeTeamId = homeTeamId;
        this.awayTeamId = awayTeamId;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.status = status;
        this.statusShort = statusShort;
        this.goalsHomeTeam = goalsHomeTeam;
        this.goalsAwayTeam = goalsAwayTeam;
        this.halfTimeScore = halfTimeScore;
        this.finalScore = finalScore;
        this.penalty = penalty;
        this.elapsed = elapsed;
        this.firstHalfStart = firstHalfStart;
        this.secondHalfStart = secondHalfStart;
    }

    public String getId() {
        return id;
    }

    public String getEventTimestamp() {
        return eventTimestamp;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public String getRound() {
        return round;
    }

    public String getHomeTeamId() {
        return homeTeamId;
    }

    public String getAwayTeamId() {
        return awayTeamId;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusShort() {
        return statusShort;
    }

    public String getGoalsHomeTeam() {
        return goalsHomeTeam;
    }

    public String getGoalsAwayTeam() {
        return goalsAwayTeam;
    }

    public String getHalfTimeScore() {
        return halfTimeScore;
    }

    public String getFinalScore() {
        return finalScore;
    }

    public Object getPenalty() {
        return penalty;
    }

    public String getElapsed() {
        return elapsed;
    }

    public String getFirstHalfStart() {
        return firstHalfStart;
    }

    public String getSecondHalfStart() {
        return secondHalfStart;
    }

    @Override
    public String toString() {

        String match = "Match: " + homeTeam + " vs " + awayTeam + " = " + goalsHomeTeam + "-" + goalsAwayTeam;

        return "Fixture{" +
                "id='" + id + '\'' +
                ", Date='" + DateUtils.toString(eventDate,Configs.getFixtureStringFormat()) + '\'' +
                ", " + match +
                ", status: '" + status + '\'' +
                ", finalScore: '" + finalScore + '\'' +
                ", elapsed='" + elapsed + '\'' +
                ", leagueId='" + leagueId + '\'' +
                ", round='" + round + '\'' +
                ", homeTeamId='" + homeTeamId + '\'' +
                ", awayTeamId='" + awayTeamId + '\'' +
                ", statusShort='" + statusShort + '\'' +
                ", halfTimeScore='" + halfTimeScore + '\'' +
                ", penalty='" + penalty + '\'' +
                '}';
    }

    public boolean isCompleted(){
        return this.getStatusShort().equals("FT") || this.getStatus().equals("Match Finished");
    }

    public String winnerTeamId(){
        try{
            int goalsHome = Integer.parseInt( this.goalsHomeTeam );
            int goalsAway = Integer.parseInt( this.goalsAwayTeam );

            if ( goalsHome == goalsAway ) return "";

            if ( goalsHome > goalsAway ) return this.homeTeamId;
            return this.awayTeamId;
        }
        catch (NumberFormatException e){
            return "";
        }

    }

    public String loserTeamId(){
        try{
            int goalsHome = Integer.parseInt( this.goalsHomeTeam );
            int goalsAway = Integer.parseInt( this.goalsAwayTeam );

            if ( goalsHome == goalsAway ) return "";

            if ( goalsHome > goalsAway ) return this.awayTeamId;
            return this.homeTeamId;
        }
        catch ( NumberFormatException e){
            return "";
        }

    }

    public int getTeamGoalsFor(String teamId){
        try{
            if ( teamId.equals(this.homeTeamId) ) return Integer.parseInt( this.goalsHomeTeam );
            if ( teamId.equals(this.awayTeamId) ) return Integer.parseInt( this.goalsAwayTeam );
        }
        catch (NumberFormatException e){
            return 0;
        }

        return 0;
    }

    public int getTeamGoalsConceived(String teamId){
        try{
            if ( teamId.equals(this.homeTeamId) ) return Integer.parseInt( this.goalsAwayTeam );
            if ( teamId.equals(this.awayTeamId) ) return Integer.parseInt( this.goalsHomeTeam );
        }
        catch (NumberFormatException e){
            return 0;
        }

        return 0;
    }

    public int pointsForTeam(String teamId){
        if ( this.winnerTeamId().equals(teamId) ) return 3;
        if ( this.winnerTeamId().isEmpty() ) return 1;
        return 0;
    }

    /**
     * Equals does not include ID parameter
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fixture fixture = (Fixture) o;
        return  Objects.equals(eventTimestamp, fixture.eventTimestamp) &&
                Objects.equals(homeTeamId, fixture.homeTeamId) &&
                Objects.equals(awayTeamId, fixture.awayTeamId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, eventTimestamp, homeTeamId, awayTeamId);
    }
}
