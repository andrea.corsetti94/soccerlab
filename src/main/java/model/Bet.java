package model;

import odds.Markets;

public class Bet {

    private final String algoId;
    private final String fixtureId;
    private final Markets.MarketValue marketValue;
    private final double profit;
    private final double amount;
    private final double odds;

    public Bet(String algoId, String fixtureId, Markets.MarketValue marketValue, double amount, double odds, double profit) {
        this.algoId = algoId;
        this.fixtureId = fixtureId;
        this.marketValue = marketValue;
        this.amount = amount;
        this.odds = odds;
        this.profit = profit;
    }

    public Markets.MarketValue getMarketValue(){
        return marketValue;
    }

    public String getAlgoId() {
        return algoId;
    }

    public String getFixtureId() {
        return fixtureId;
    }

    public double getProfit() {
        return profit;
    }

    public double getAmount() {
        return amount;
    }

    public double getOdds() {
        return odds;
    }

    @Override
    public String toString() {
        return "Bet{" +
                "algoId='" + algoId + '\'' +
                ", fixtureId='" + fixtureId + '\'' +
                ", marketValue=" + marketValue +
                ", amount=" + amount +
                ", odds=" + odds +
                ", profit=" + profit +
                '}';
    }
}
