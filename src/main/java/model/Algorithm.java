package model;

public class Algorithm {
    private final String idName;

    public Algorithm(String idName) {
        this.idName = idName;
    }

    public String getIdName() {
        return idName;
    }
}
