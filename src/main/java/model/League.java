package model;

public class League {

    private final String id;
    private final String name;
    private final String countryName;
    private final String seasonYear;
    private final String seasonStartDate;
    private final String seasonEndDate;
    private final String seasonLogoUrl;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getSeasonYear() {
        return seasonYear;
    }

    public String getSeasonStartDate() {
        return seasonStartDate;
    }

    public String getSeasonEndDate() {
        return seasonEndDate;
    }

    public String getSeasonLogoUrl() {
        return seasonLogoUrl;
    }

    public League(String id, String name, String countryName, String seasonYear,
                  String seasonStartDate, String seasonEndDate, String seasonLogoUrl) {
        this.id = id;
        this.name = name;
        this.countryName = countryName;
        this.seasonYear = seasonYear;
        this.seasonStartDate = seasonStartDate;
        this.seasonEndDate = seasonEndDate;
        this.seasonLogoUrl = seasonLogoUrl;
    }

    @Override
    public String toString() {
        return "League{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", countryName='" + countryName + '\'' +
                ", seasonYear='" + seasonYear + '\'' +
                ", seasonStartDate='" + seasonStartDate + '\'' +
                ", seasonEndDate='" + seasonEndDate + '\'' +
                ", seasonLogoUrl='" + seasonLogoUrl + '\'' +
                '}';
    }
}
