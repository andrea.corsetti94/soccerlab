package model;

public class LeagueTeam {

    private final String leagueId;
    private final String teamId;

    public LeagueTeam(String leagueId, String teamId) {
        this.leagueId = leagueId;
        this.teamId = teamId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public String getTeamId() {
        return teamId;
    }

    @Override
    public String toString() {
        return "LeagueTeam{" +
                "leagueId='" + leagueId + '\'' +
                ", teamId='" + teamId + '\'' +
                '}';
    }
}
