package model;

public class Season {

    private final String year;

    public Season(String year) {
        this.year = year;
    }


    public String getYear(){
        return year;
    }

    @Override
    public String toString() {
        return "Season{" +
                "year='" + year + '\'' +
                '}';
    }
}
