package model;

import java.util.List;

public class Standing {

    private final String leagueId;
    private final List<StandingPosition> positions;

    public Standing(String leagueId, List<StandingPosition> positions) {
        this.leagueId = leagueId;
        this.positions = positions;
        this.orderPositions();
    }

    private void orderPositions(){
        positions.sort((position1, position2) -> {
            int rank1 = Integer.parseInt(position1.getRank());
            int rank2 = Integer.parseInt(position2.getRank());

            if ( rank1 < rank2 ) return -1;
            else if ( rank1 == rank2 ) return 0;
            return 1;
        });
    }

    public String getLeagueId() {
        return leagueId;
    }

    public List<StandingPosition> getPositions() {
        return positions;
    }

    @Override
    public String toString() {
        String positions = "";
        for(int i = 1; i <= getPositions().size(); i++){
            positions += i + ") " + getPositions().get(i - 1).toString() + "\n";
        }

        return "Standing{" +
                "league id='" + leagueId + '\'' +
                ", positions=\n" + positions +
                '}';
    }
}
