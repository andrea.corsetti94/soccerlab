package model;

public class Country {


    private final String id;
    private final String name;


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Country(String id, String name) {
        this.id = id;
        this.name = name;
    }


    @Override
    public String toString() {
        return "Country{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
