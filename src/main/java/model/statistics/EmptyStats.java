package model.statistics;

class EmptyStats extends Stats {

    public EmptyStats(StatisticsType type){
        super(type, 0, 0, 0, 0, 0, 0);
    }
}
