package model.statistics;

import model.Team;
import utils.MathUtils;

class TeamStatsByLeague {

    private final Team team;
    private final int goalsForAway;
    private final int goalsConceivedAway;
    private final int playedAway;
    private final int goalsForHome;
    private final int goalsConceivedHome;
    private final int playedHome;
    private final int pointsHome;
    private final int pointsAway;

    private double avgGoalsForHome;
    private double avgGoalsAgainstHome;
    private double avgGoalsForAway;
    private double avgGoalsAgainstAway;
    private double avgGoalsForTotal;
    private double avgGoalsAgainstTotal;
    private double ppgTotal;
    private double ppgHome;
    private double ppgAway;
    private double ppgHomeAdvantage;

    public TeamStatsByLeague(Team team, int goalsForHome, int goalsConceivedHome, int playedHome, int goalsForAway,
                   int goalsConceivedAway, int playedAway, int pointsHome, int pointsAway
    ) {
        this.team = team;
        this.goalsForAway = goalsForAway;
        this.goalsConceivedAway = goalsConceivedAway;
        this.playedAway = playedAway;
        this.goalsForHome = goalsForHome;
        this.goalsConceivedHome = goalsConceivedHome;
        this.playedHome = playedHome;
        this.pointsHome = pointsHome;
        this.pointsAway = pointsAway;

        if (playedHome > 0){
            this.avgGoalsForHome = MathUtils.round( (double) goalsForHome/playedHome,2);
            this.avgGoalsAgainstHome = MathUtils.round((double) goalsConceivedHome / playedHome, 2);
            this.ppgHome = MathUtils.round((double) pointsHome / playedHome, 2);
        }

        if ( playedAway > 0){
            this.avgGoalsForAway = MathUtils.round((double) goalsForAway / playedAway, 2);
            this.avgGoalsAgainstAway = MathUtils.round((double) goalsConceivedAway / playedAway, 2);
            this.ppgAway = MathUtils.round((double) pointsAway / playedAway, 2);
        }

        if ( playedAway + playedHome > 0 ){
            this.avgGoalsForTotal = MathUtils.round((double) (goalsForHome + goalsForAway) / (playedAway + playedHome), 2);
            this.avgGoalsAgainstTotal = MathUtils.round((double) (goalsConceivedHome + goalsConceivedAway) / (playedAway + playedHome), 2);

            this.ppgTotal = MathUtils.round((double) (pointsHome+pointsAway) / (playedAway + playedHome), 2);
            this.ppgHomeAdvantage = this.ppgHome / this.ppgTotal;
        }
    }

    public int getGoalsForAway() {
        return goalsForAway;
    }

    public int getGoalsConceivedAway() {
        return goalsConceivedAway;
    }

    public int getPlayedAway() {
        return playedAway;
    }

    public int getGoalsForHome() {
        return goalsForHome;
    }

    public int getGoalsConceivedHome() {
        return goalsConceivedHome;
    }

    public int getPlayedHome() {
        return playedHome;
    }

    public double getAvgGoalsForHome() {
        return avgGoalsForHome;
    }

    public double getAvgGoalsAgainstHome() {
        return avgGoalsAgainstHome;
    }

    public double getAvgGoalsForAway() {
        return avgGoalsForAway;
    }

    public double getAvgGoalsAgainstAway() {
        return avgGoalsAgainstAway;
    }

    public double getAvgGoalsForTotal() {
        return avgGoalsForTotal;
    }

    public double getAvgGoalsAgainstTotal() {
        return avgGoalsAgainstTotal;
    }

    public Team getTeam() {
        return team;
    }

    public int getPointsHome() {
        return pointsHome;
    }

    public int getPointsAway() {
        return pointsAway;
    }

    public double getPpgTotal() {
        return ppgTotal;
    }

    public double getPpgHome() {
        return ppgHome;
    }

    public double getPpgAway() {
        return ppgAway;
    }

    public double getPpgHomeAdvantage() {
        return ppgHomeAdvantage;
    }

    @Override
    public String toString() {
        return
                ", played=" + playedAway + playedHome +
                        ", playedHome=" + playedHome +
                        ", PPG - Home=" + ppgHome +
                        ", goalsForHome=" + goalsForHome +
                        ", goalsConceivedHome=" + goalsConceivedHome +
                        ", avgGoalsForHome=" + avgGoalsForHome +
                        ", avgGoalsAgainstHome=" + avgGoalsAgainstHome +
                ", playedAway=" + playedAway +
                ", PPG - Away=" + ppgAway +
                        ", goalsForAway=" + goalsForAway +
                        ", goalsConceivedAway=" + goalsConceivedAway +
                ", avgGoalsForAway=" + avgGoalsForAway +
                ", avgGoalsAgainstAway=" + avgGoalsAgainstAway +
                ", avgGoalsForTotal=" + avgGoalsForTotal +
                ", avgGoalsAgainstTotal=" + avgGoalsAgainstTotal
                ;
    }
}
