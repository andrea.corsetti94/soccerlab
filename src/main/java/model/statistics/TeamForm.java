package model.statistics;

import utils.MathUtils;

public class TeamForm implements Comparable<TeamForm> {

    private final double teamFormHome;
    private final double teamFormAway;
    private final double teamFormTotal;

    private final String teamId;

    public TeamForm(String teamId, double teamFormHome, double teamFormAway, double teamFormTotal) {
        this.teamFormHome = MathUtils.round(teamFormHome, 2);
        this.teamFormAway = MathUtils.round(teamFormAway, 2);
        this.teamFormTotal = MathUtils.round(teamFormTotal, 2);
        this.teamId = teamId;
    }

    @Override
    public String toString() {
        return "TeamForm{" +
                "teamId='" + teamId + "'" +
                ", teamFormTotal=" + teamFormTotal +
                ", teamFormHome=" + teamFormHome +
                ", teamFormAway=" + teamFormAway +
                '}';
    }

    public double getTeamFormHome() {
        return teamFormHome;
    }

    public double getTeamFormAway() {
        return teamFormAway;
    }

    private double getTeamFormTotal() {
        return teamFormTotal;
    }

    public String getTeamId() {
        return teamId;
    }

    @Override
    public int compareTo(TeamForm teamForm) {
        if ( this.getTeamFormTotal() > teamForm.getTeamFormTotal() ) return -1;
        else if ( this.getTeamFormTotal() == teamForm.getTeamFormTotal() ) return 0;
        return 1;
    }
}
