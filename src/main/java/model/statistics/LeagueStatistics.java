package model.statistics;

import utils.MathUtils;

class LeagueStatistics {

    private final int homePlayed;
    private final int awayPlayed;
    private final int totalPlayed;

    private double avgGoalsAgainstAway;
    private double avgGoalsAgainstHome;
    private double avgGoalsForHome;
    private double avgGoalsForAway;
    private double avgGoalsForTotal;
    private double avgGoalsAgainstTotal;

    public LeagueStatistics(int homePlayed, int awayPlayed, int goalsAgainstAway,
                            int goalsAgainstHome, int goalsForHome, int goalsForAway) {

        this.homePlayed = homePlayed;
        this.awayPlayed = awayPlayed;
        this.totalPlayed = homePlayed + awayPlayed;

        if ( homePlayed > 0 ){
            this.avgGoalsAgainstHome = MathUtils.round((double) goalsAgainstHome/homePlayed, 2);
            this.avgGoalsForHome = MathUtils.round((double) goalsForHome/homePlayed, 2);
        }

        if ( awayPlayed > 0 ){
            this.avgGoalsAgainstAway = MathUtils.round((double) goalsAgainstAway/awayPlayed, 2);
            this.avgGoalsForAway = MathUtils.round((double) goalsForAway/awayPlayed, 2);
        }

        if ( this.totalPlayed > 0 ){
            this.avgGoalsForTotal = MathUtils.round((double) (goalsForHome + goalsForAway)/totalPlayed, 2);
            this.avgGoalsAgainstTotal = MathUtils.round((double) (goalsAgainstAway + goalsAgainstHome)/totalPlayed, 2);
        }

    }

    public double getAvgGoalsForTotal() {
        return avgGoalsForTotal;
    }

    public double getAvgGoalsAgainstTotal() {
        return avgGoalsAgainstTotal;
    }

    public int getHomePlayed() {
        return homePlayed;
    }

    public int getAwayPlayed() {
        return awayPlayed;
    }

    public int getTotalPlayed() {
        return totalPlayed;
    }

    public double getAvgGoalsAgainstAway() {
        return avgGoalsAgainstAway;
    }

    public double getAvgGoalsAgainstHome() {
        return avgGoalsAgainstHome;
    }

    public double getAvgGoalsForHome() {
        return avgGoalsForHome;
    }

    public double getAvgGoalsForAway() {
        return avgGoalsForAway;
    }

    @Override
    public String toString() {
        return "LeagueStatistics{" +
                "homePlayed=" + homePlayed +
                ", awayPlayed=" + awayPlayed +
                ", totalPlayed=" + totalPlayed +
                ", avgGoalsForHome=" + avgGoalsForHome +
                ", avgGoalsForAway=" + avgGoalsForAway +
                ", avgGoalsAgainstHome=" + avgGoalsAgainstHome +
                ", avgGoalsAgainstAway=" + avgGoalsAgainstAway +
                ", avgGoalsForTotal=" + avgGoalsForTotal +
                ", avgGoalsAgainstTotal=" + avgGoalsAgainstTotal +
                '}';
    }
}
