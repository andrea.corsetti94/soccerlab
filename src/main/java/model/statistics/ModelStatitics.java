package model.statistics;

public class ModelStatitics {

    private final String id;

    private Stats homeStats;
    private Stats awayStats;
    private Stats totalStats;

    public ModelStatitics(String id, Stats homeStats, Stats awayStats,
                          Stats totalStats) {
        this.id = id;
        this.homeStats = homeStats;
        this.awayStats = awayStats;
        this.totalStats = totalStats;
    }

    public String getId() {
        return id;
    }

    private Stats getHomeStats() {
        return homeStats;
    }

    private Stats getAwayStats() {
        return awayStats;
    }

    private Stats getTotalStats() {
        return totalStats;
    }

    protected void setStats(Stats homeStats, Stats awayStats, Stats totalStats){
        this.homeStats = homeStats;
        this.awayStats = awayStats;
        this.totalStats = totalStats;
    }

    @Override
    public String toString() {
        return "ModelStatitics{" +
                "team=" + id +
                "\nhomeStats=" + homeStats +
                "\nawayStats=" + awayStats +
                "\ntotalStats=" + totalStats +
                '}';
    }

    public ModelStatitics minus(ModelStatitics ms){
        Stats homeStats = this.homeStats.minus(ms.getHomeStats());
        Stats awayStats = this.awayStats.minus(ms.getAwayStats());
        Stats totalStats = this.totalStats.minus(ms.getTotalStats());

        return new ModelStatitics(this.id, homeStats, awayStats, totalStats);
    }
}
