package model.statistics;

public enum StatisticsType {
    HOME_STATISTICS, AWAY_STATISTICS, TOTAL_STATISTICS
}
