package model.statistics;

public class EmptyModelStatistics extends ModelStatitics {

    public EmptyModelStatistics(String id){
        super(id, new EmptyStats(StatisticsType.HOME_STATISTICS), new EmptyStats(StatisticsType.AWAY_STATISTICS)
                , new EmptyStats(StatisticsType.TOTAL_STATISTICS));
    }
}
