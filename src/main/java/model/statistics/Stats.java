package model.statistics;

import utils.MathUtils;

public class Stats {

    private final StatisticsType type;

    private final int played;
    private final int wins;
    private final int draws;
    private final int lost;
    private final int goalsFor;
    private final int goalsConceived;
    private double avgGoalsFor;
    private double avgGoalsConceived;

    public Stats(StatisticsType type, int played, int wins, int draws, int lost, int goalsFor, int goalsConceived) {

        this.type = type;
        this.played = played;
        this.wins = wins;
        this.draws = draws;
        this.lost = lost;
        this.goalsFor = goalsFor;
        this.goalsConceived = goalsConceived;

        if(played > 0){
            this.avgGoalsFor = MathUtils.round((double) goalsFor/played, 2);
            this.avgGoalsConceived = MathUtils.round((double) goalsConceived/played, 2);
        }

    }

    private StatisticsType getType() {
        return type;
    }

    private int getPlayed() {
        return played;
    }

    private int getWins() {
        return wins;
    }

    private int getDraws() {
        return draws;
    }

    private int getLost() {
        return lost;
    }

    private int getGoalsFor() {
        return goalsFor;
    }

    private int getGoalsConceived() {
        return goalsConceived;
    }

    public double getAvgGoalsFor() {
        return avgGoalsFor;
    }

    public double getAvgGoalsConceived() {
        return avgGoalsConceived;
    }

    @Override
    public String toString() {
        return "Stats{" +
                "type=" + type +
                ", played=" + played +
                ", wins=" + wins +
                ", draws=" + draws +
                ", lost=" + lost +
                ", goalsFor=" + goalsFor +
                ", goalsConceived=" + goalsConceived +
                ", avgGoalsFor=" + avgGoalsFor +
                ", avgGoalsConceived=" + avgGoalsConceived +
                '}';
    }

    public Stats minus(Stats subtracting){
        int played = Math.max(this.played - subtracting.getPlayed(), 0);
        int wins = Math.max(this.wins - subtracting.getWins(), 0);
        int draws = Math.max(this.draws - subtracting.getDraws(), 0);
        int lost = Math.max(this.lost - subtracting.getLost(), 0);
        int goalsFor = Math.max(this.goalsFor - subtracting.getGoalsFor(), 0);
        int goalsConceived = Math.max(this.goalsConceived - subtracting.getGoalsConceived(), 0);

        return new Stats(this.getType(), played, wins, draws, lost, goalsFor, goalsConceived);
    }
}
