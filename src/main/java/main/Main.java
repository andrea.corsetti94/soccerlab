package main;

import data.DataUpdater;
import data.db.DB;
import data.db.DBStatements;
import data.db.dao.DAOAlgorithm;
import data.db.dao.DAOBets;
import data.db.retriever.AlgorithmDataRetriever;
import data.db.retriever.BetsDataRetriever;
import data.db.retriever.MatchPickRetriever;
import model.Algorithm;
import model.Bet;
import model.MatchPick;
import scheduler.SchedulerManager;
import utils.AlgoBetsUtils;
import utils.ZohoAPI;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.*;


class Main {
    
    static {
        if ( Configs.isDataSchedulerActive() ){
            SchedulerManager.startSchedulerExecution();
        }
    }

    public static void main(String[] args) {
        DataUpdater.updateNow();
        //DataUpdater.updateBets();

        //ZohoAPI.postXML();
    }
}

