package main;

public class Logger {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);

    public static org.apache.log4j.Logger getLogger() {
        return logger;
    }

    public static org.apache.log4j.Logger getLogger(Class<?> clazz) {
        return org.apache.log4j.Logger.getLogger(clazz);
    }

}
