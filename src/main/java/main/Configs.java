package main;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configs {

    private static String apiKei;
    private static String fixtureInputDateFormat;
    private static String seasonYearFormat;
    private static String fixtureJSONDateFormat;
    private static String fixtureStringFormat;
    private static String dbPath;
    private static String dbFileName;
    private static String apiDomainURL;
    private static boolean isDataSchedulerActive;
    private static boolean isOddsBalancerActive;
    private static String excelFixturesPath;
    private static int matchesToConsider;
    private static int teamFormMatchesToConsider;

    static {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = Configs.class.getResourceAsStream("/config.properties");
            // load a properties file
            prop.load(input);

            apiKei = prop.getProperty("API_KEY");
            fixtureInputDateFormat = prop.getProperty("FIXTURE_INPUT_DATE_FORMAT");
            seasonYearFormat = prop.getProperty("SEASON_YEAR_FORMAT");
            fixtureJSONDateFormat = prop.getProperty("FIXTURE_JSON_DATE_FORMAT");
            fixtureStringFormat = prop.getProperty("FIXTURE_STRING_FORMAT");
            dbPath = prop.getProperty("DB_REL_PATH");
            dbFileName = prop.getProperty("DB_FILENAME");
            apiDomainURL = prop.getProperty("API_DOMAIN_URL");
            isDataSchedulerActive = Boolean.parseBoolean(prop.getProperty("DATA_SCHEDULER_ACTIVE"));
            isOddsBalancerActive = Boolean.parseBoolean(prop.getProperty("ODDS_BALANCER"));
            excelFixturesPath = prop.getProperty("EXCEL_FIXTURES_PATH");
            matchesToConsider = Integer.parseInt(prop.getProperty("STATS_MATCHES_TO_CONSIDER"));
            teamFormMatchesToConsider = Integer.parseInt(prop.getProperty("TEAM_FORM_MATCHES_TO_CONSIDER"));

        } catch (Exception ex) {
            Logger.getLogger(Configs.class).fatal("Error initialising application. Exit.", ex);
            System.exit(0);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    Logger.getLogger(Configs.class).fatal("Error initialising application. Exit.", e);
                    System.exit(0);
                }
            }
        }
    }


    public static String getApiKey(){
        return apiKei;
    }

    public static String getFixtureInputDateFormat(){
        return fixtureInputDateFormat;
    }

    public static String getSeasonYearFormat() {
        return seasonYearFormat;
    }

    public static String getFixtureJSONDateFormat() {
        return fixtureJSONDateFormat;
    }

    public static String getFixtureStringFormat() {
        return fixtureStringFormat;
    }

    public static String getDbPath() {
        return dbPath;
    }

    public static String getDbFilename() {
        return dbFileName;
    }

    public static String getApiDomainURL() {
        return apiDomainURL;
    }

    public static boolean isDataSchedulerActive(){
        return isDataSchedulerActive;
    }

    public static boolean isOddsBalancerActive() {
        return isOddsBalancerActive;
    }

    public static String getExcelFixturesPath(){
        return excelFixturesPath;
    }

    public static int getMatchesToConsider(){ return matchesToConsider; }

    public static int getTeamFormMatchesToConsider(){ return teamFormMatchesToConsider; }

}
