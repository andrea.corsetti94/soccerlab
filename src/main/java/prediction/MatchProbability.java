package prediction;

import data.db.retriever.FixtureDataRetriever;
import main.Configs;
import main.Logger;
import model.Fixture;
import odds.MarketChance;
import odds.Markets;

import java.util.ArrayList;
import java.util.List;

public class MatchProbability {

    private List<MarketChance> eventProbs;
    private final String homeTeamId;
    private final String awayTeamId;
    private final Fixture fixture;
    private final PoissonTable poissonTable;

    private boolean reliable = true;

    MatchProbability(Fixture fixture, PoissonTable pt) {
        this.fixture = fixture;
        this.homeTeamId = fixture.getHomeTeamId();
        this.awayTeamId = fixture.getAwayTeamId();
        this.poissonTable = pt;

        this.computePoissonTable();

        this.checkReliability();
    }

    private void computePoissonTable(){
        this.eventProbs = new ArrayList<>();

        List<MarketChance> exactScoreTable = this.poissonTable.getPoissonTable();

        //double over0_5 = computeOver0_5(exactScoreTable);
        double over1_5 = computeOver(exactScoreTable, 1);
        double over2_5 = computeOver(exactScoreTable, 2);
        double over3_5 = computeOver(exactScoreTable, 3);
        double over4_5 = computeOver(exactScoreTable, 4);
        //double under0_5 = 1 - over0_5;
        double under1_5 = 1 - over1_5;
        double under2_5 = 1 - over2_5;
        double under3_5 = 1 - over3_5;
        double under4_5 = 1 - over4_5;
        double homeFT = computeHomeFT(exactScoreTable);
        double awayFT = computeAwayFT(exactScoreTable);
        double drawFT = 1 - (homeFT + awayFT);
        double hdDC = homeFT + drawFT;
        double daDC = drawFT + awayFT;
        double haDC = homeFT + awayFT;
        double goal = computeGoalFT(exactScoreTable);
        double noGoal = 1 - goal;
        /*
        double goalHome = computeGoalHomeFT(exactScoreTable);
        double noGoalHome = 1 - goalHome;
        double goalAway = computeGoalAwayFT(exactScoreTable);
        double noGoalAway = 1 - goalAway;
        double oneGoalTotal = computeGoalSumFT(exactScoreTable, 1);
        double twoGoalTotal = computeGoalSumFT(exactScoreTable, 2);
        double threeGoalTotal = computeGoalSumFT(exactScoreTable, 3);
        double fourGoalTotal = computeGoalSumFT(exactScoreTable, 4);
        double fiveGoalTotal = computeGoalSumFT(exactScoreTable, 5);
        double sixGoalTotal = computeGoalSumFT(exactScoreTable, 6);
        double sevenGoalTotal = computeGoalSumFT(exactScoreTable, 7);
        double moreThanSevenGoalTotal = 1 - (under0_5 +oneGoalTotal+twoGoalTotal+threeGoalTotal+fiveGoalTotal+fourGoalTotal
                +sixGoalTotal+sevenGoalTotal);
        double oddGoals = computeOddEvenFT(exactScoreTable, true);
        double evenGoals = 1 - oddGoals;
        */
        double home_over1_5 = homeFT*over1_5;
        double home_over2_5 = homeFT*over2_5;
        double home_over3_5 = homeFT*over3_5;
        double home_over4_5 = homeFT*over4_5;
        double home_under1_5 = homeFT*under1_5;
        double home_under2_5 = homeFT*under2_5;
        double home_under3_5 = homeFT*under3_5;
        double home_under4_5 = homeFT*under4_5;
        double draw_over1_5 = drawFT*over1_5;
        double draw_over2_5 = drawFT*over2_5;
        double draw_over3_5 = drawFT*over3_5;
        double draw_over4_5 = drawFT*over4_5;
        double draw_under1_5 = drawFT*under1_5;
        double draw_under2_5 = drawFT*under2_5;
        double draw_under3_5 = drawFT*under3_5;
        double draw_under4_5 = drawFT*under4_5;
        double away_over1_5 = awayFT*over1_5;
        double away_over2_5 = awayFT*over2_5;
        double away_over3_5 = awayFT*over3_5;
        double away_over4_5 = awayFT*over4_5;
        double away_under1_5 = awayFT*under1_5;
        double away_under2_5 = awayFT*under2_5;
        double away_under3_5 = awayFT*under3_5;
        double away_under4_5 = awayFT*under4_5;
        double home_goalYes = homeFT*goal;
        double home_goalNo = homeFT*noGoal;
        double draw_goalYes = drawFT*goal;
        double draw_goalNo = drawFT*noGoal;
        double away_goalYes = awayFT*goal;
        double away_goalNo = awayFT*noGoal;

        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME, homeFT));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW, drawFT));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY, awayFT));
        eventProbs.add(new MarketChance(Markets.MarketValue.DC_HOME_DRAW, hdDC));
        eventProbs.add(new MarketChance(Markets.MarketValue.DC_DRAW_AWAY, daDC));
        eventProbs.add(new MarketChance(Markets.MarketValue.DC_HOME_AWAY, haDC));
        eventProbs.add(new MarketChance(Markets.MarketValue.BTTS_YES, goal));
        eventProbs.add(new MarketChance(Markets.MarketValue.BTTS_NO, noGoal));
        /*
        eventProbs.add("Goal Home Team", goalHome);
        eventProbs.add("No-Goal Home Team", noGoalHome);
        eventProbs.add("Goal Away Team", goalAway);
        eventProbs.add("No-Goal Away Team", noGoalAway);
        eventProbs.add("Over 0.5", over0_5);
        eventProbs.add("Under 0.5", under0_5);
        */
        eventProbs.add(new MarketChance(Markets.MarketValue.O1_5, over1_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.O2_5, over2_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.O3_5, over3_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.O4_5, over4_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.U1_5, under1_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.U2_5, under2_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.U3_5, under3_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.U4_5, under4_5));
        /*
        eventProbs.add("Odd", oddGoals);
        eventProbs.add("Even", evenGoals);
        eventProbs.add("Goal Sum: 0", zeroGoalTotal);
        eventProbs.add("Goal Sum: 1", oneGoalTotal);
        eventProbs.add("Goal Sum: 2", twoGoalTotal);
        eventProbs.add("Goal Sum: 3", threeGoalTotal);
        eventProbs.add("Goal Sum: 4", fourGoalTotal);
        eventProbs.add("Goal Sum: 5", fiveGoalTotal);
        eventProbs.add("Goal Sum: 6", sixGoalTotal);
        eventProbs.add("Goal Sum: 7", sevenGoalTotal);
        eventProbs.add("Goal Sum: more than 7", moreThanSevenGoalTotal);
        */
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME_O1_5, home_over1_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME_O2_5, home_over2_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME_O3_5, home_over3_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME_O4_5, home_over4_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME_U1_5, home_under1_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME_U2_5, home_under2_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME_U3_5, home_under3_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_HOME_U4_5, home_under4_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW_O1_5, draw_over1_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW_O2_5, draw_over2_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW_O3_5, draw_over3_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW_O4_5, draw_over4_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW_U1_5, draw_under1_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW_U2_5, draw_under2_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW_U3_5, draw_under3_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_DRAW_U4_5, draw_under4_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY_O1_5, away_over1_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY_O2_5, away_over2_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY_O3_5, away_over3_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY_O4_5, away_over4_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY_U1_5, away_under1_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY_U2_5, away_under2_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY_U3_5, away_under3_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_AWAY_U4_5, away_under4_5));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_BTTS_HOME_YES, home_goalYes));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_BTTS_DRAW_YES, draw_goalYes));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_BTTS_AWAY_YES, away_goalYes));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_BTTS_HOME_NO, home_goalNo));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_BTTS_DRAW_NO, draw_goalNo));
        eventProbs.add(new MarketChance(Markets.MarketValue.HDA_BTTS_AWAY_NO, away_goalNo));

        eventProbs.addAll(exactScoreTable);
    }

    private static double computeOver(List<MarketChance> exactScoreTable, int overLimit){
        double chance = 0.0;
        for(MarketChance mc: exactScoreTable){
            String score = mc.getMarketValue().getRepresentation();

            if(!score.contains("-")){
                chance += mc.getChance();
                continue;
            }

            int homeGoals = extractHomeGoals(score);
            int awayGoals = extractAwayGoals(score);

            if(homeGoals + awayGoals > overLimit){
                chance += mc.getChance();
            }
        }
        return chance;
    }

    private static double computeHomeFT(List<MarketChance> exactScoreTable){
        double chance = 0.0;
        for(MarketChance mc: exactScoreTable){
            String score = mc.getMarketValue().getRepresentation();

            if(!score.contains("-")) continue;

            int homeGoals = extractHomeGoals(score);
            int awayGoals = extractAwayGoals(score);

            if(homeGoals > awayGoals){
                chance += mc.getChance();
            }
        }
        return chance;
    }

    private static double computeAwayFT(List<MarketChance> exactScoreTable){
        double chance = 0.0;
        for(MarketChance mc: exactScoreTable){
            String score = mc.getMarketValue().getRepresentation();

            if(!score.contains("-")) continue;

            int homeGoals = extractHomeGoals(score);
            int awayGoals = extractAwayGoals(score);

            if(homeGoals < awayGoals){
                chance += mc.getChance();
            }
        }
        return chance;
    }

    private static double computeGoalFT(List<MarketChance> exactScoreTable) {
        double chance = 0.0;
        for(MarketChance mc: exactScoreTable){
            String score = mc.getMarketValue().getRepresentation();

            if (!score.contains("-")) continue;

            int homeGoals = extractHomeGoals(score);
            int awayGoals = extractAwayGoals(score);

            if (homeGoals > 0 && awayGoals > 0) {
                chance += mc.getChance();
            }
        }
        return chance;
    }

    private static int extractHomeGoals(String result){
        String substring = result.substring(0,result.indexOf("-")).trim();
        return Integer.parseInt(substring);
    }

    private static int extractAwayGoals(String result){
        String substring = result.substring(result.indexOf("-") + 1).trim();
        return Integer.parseInt(substring);
    }

    public boolean isReliable() {
        return reliable;
    }

    private void setReliable(boolean reliable) {
        this.reliable = reliable;
    }

    /**
     * Determines whether this match probability is reliable or not
     */
    private void checkReliability(){
        double homeExpectancy = this.poissonTable.getHomeExpectancy();
        double awayExpectancy = this.poissonTable.getAwayExpectancy();

        if ( homeExpectancy <= 0.01 || awayExpectancy <= 0.01 ){
            Logger.getLogger(this.getClass()).warn(" Home or Away team expectancy <= 0.01 for fixture: " + this.fixture.getId());
            this.setReliable(false);
        }

        FixtureDataRetriever fixtureDataRetriever = new FixtureDataRetriever();
        int homeMatches = fixtureDataRetriever.getLastTeamMatches(fixture.getHomeTeamId(), Configs.getMatchesToConsider()).size();
        int awayMatches = fixtureDataRetriever.getLastTeamMatches(fixture.getAwayTeamId(), Configs.getMatchesToConsider()).size();

        if (homeMatches <= 5 || awayMatches <= 5) {
            this.setReliable(false);
        }

    }

    double getProbability(Markets.MarketValue marketValue){
        for(MarketChance mc: eventProbs){
            if ( mc.getMarketValue() == marketValue ){
                return mc.getChanceBy100();
            }
        }

        Logger.getLogger(this.getClass()).warn("No Probability calculated for Market Value: " + marketValue
                + " in Fixture between " + this.homeTeamId + " and " + this.awayTeamId + "\n0 is returned.");
        return 0;
    }

    public String getProbabilityRepresentation(Markets.MarketValue marketValue){
        for (MarketChance mc : eventProbs) {
            if (mc.getMarketValue() == marketValue) {
                return mc.getChanceRepresentation();
            }
        }

        Logger.getLogger(this.getClass()).warn("No Probability calculated for Market Value: " + marketValue
                + " in Fixture between " + this.homeTeamId + " and " + this.awayTeamId + "\n0 is returned.");
        return "-";
    }

    @Override
    public String toString(){
        String toReturn = "Match probability: \n";
        for(MarketChance mc: this.eventProbs){
            toReturn += mc.toString() + " ,";
        }
        return toReturn.substring(0, toReturn.length() - 2);
    }
}
