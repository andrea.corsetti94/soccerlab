package prediction;

import model.statistics.TeamForm;

class TeamFormExpectancy extends Expectancy {

    private double homeGoalExpectancy;
    private double awayGoalExpectancy;

    TeamFormExpectancy(String homeTeamId, String awayTeamId) {
        super(homeTeamId, awayTeamId);
    }

    @Override
    public double getHomeGoalExpectancy() {
        return homeGoalExpectancy;
    }


    @Override
    public double getAwayGoalExpectancy() {
        return awayGoalExpectancy;
    }


    @Override
    public void computeExpectancy() {

        String homeId = super.getHomeTeamId();
        String awayId = super.getAwayTeamId();

        TeamStatsComparator teamStatsComparator = new TeamStatsComparator(homeId, awayId);

        double homeAttack = teamStatsComparator.getHomeAttackingStrength();
        double homeDefence = teamStatsComparator.getHomeDefensiveStrength();

        double awayAttack = teamStatsComparator.getAwayAttackingStrength();
        double awayDefence = teamStatsComparator.getAwayDefensiveStrength();

        TeamForm homeTeamForm = TeamStrengthCalculator.extractForm(homeId);
        TeamForm awayTeamForm = TeamStrengthCalculator.extractForm(awayId);

        double formHome = homeTeamForm.getTeamFormHome();
        double formAway = awayTeamForm.getTeamFormAway();

        //home and away goal expectancy
        double hge = homeAttack * awayDefence;
        double age = awayAttack * homeDefence;

        double offset = offset(formHome, formAway);
        //double offset = 0;

        if (formHome > formAway) {
            homeGoalExpectancy = hge + offset;
            awayGoalExpectancy = age;
        } else {
            homeGoalExpectancy = hge + (offset / 3);
            awayGoalExpectancy = age + offset;
        }

        /*
        Logger.getLogger(this.getClass()).debug("TeamFormExpectancy for: home team: " + super.getHomeTeamId()+":\n"
            + "Home Attack: " + homeAttack + ", Home defence: " + homeDefence + ", FORM home: " + formHome
            +", HGE: " + hge + " ===> Home Goal Expectancy: " + homeGoalExpectancy + "\n"+
                "Away Attack: " + awayAttack + ", Away defence: " + awayDefence + ", FORM Away: " + formAway
                +", AGE: " + age + " ===> Away Goal Expectancy: " + awayGoalExpectancy + "\n");
         */
    }

    /**
     * Returns the offset to be added to the home team and detracted from the away team.
     * If negative, the away team gets advantaged.
     */
    private double offset(double ppgHome, double ppgAway) {
        double absOffset = Math.abs(ppgHome - ppgAway);

        if (absOffset > 1.5) return 0.8;
        if (absOffset > 1) return 0.65;
        if (absOffset > 0.75) return 0.55;
        if (absOffset > 0.5) return 0.45;
        if (absOffset > 0.25) return 0.35;
        if (absOffset > 0.15) return 0.15;

        return 0.05;

    }
}
