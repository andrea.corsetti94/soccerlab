package prediction;

import model.Fixture;
import odds.Markets;

public class PoissonPredictor {

    public static MatchProbability getMatchProbability(Fixture fixture){

        String homeTeamId = fixture.getHomeTeamId();
        String awayTeamId = fixture.getAwayTeamId();

        Expectancy expectancy = new TeamFormExpectancy(homeTeamId, awayTeamId);

        PoissonTable pt = new PoissonTable(expectancy);
        return new MatchProbability(fixture, pt);

    }

    public static double predictEvent(MatchProbability pmp, Markets.MarketValue marketValue){
        return pmp.getProbability(marketValue);
    }
}
