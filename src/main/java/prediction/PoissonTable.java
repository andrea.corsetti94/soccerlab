package prediction;

import odds.MarketChance;
import odds.Markets;
import utils.MathUtils;

import java.util.ArrayList;
import java.util.List;

class PoissonTable {

    private final double homeExpectancy;
    private final double awayExpectancy;

    private List<MarketChance> poissonTable;

    PoissonTable(Expectancy expectancy) {

        this.homeExpectancy = expectancy.getHomeGoalExpectancy();
        this.awayExpectancy = expectancy.getAwayGoalExpectancy();

        this.buildTable();
    }

    private void buildTable(){
        poissonTable = new ArrayList<>();

        double otherChance = 0;

        for(int home = 0; home < 9; home++){
            for(int away = 0; away < 9; away++){

                double chance =
                        MathUtils.poisson(home, homeExpectancy) * MathUtils.poisson(away, awayExpectancy);

                String result = home + " - " + away;
                Markets.MarketValue marketValue = Markets.getMarketValueByRepresentation(result);

                if ( marketValue != Markets.MarketValue.EMPTY_MARKET_VALUE ){
                    poissonTable.add(new MarketChance(marketValue, chance));
                }

                if ( home > 4 && away > 4 ) otherChance += chance;

            }
        }

        poissonTable.add(new MarketChance(Markets.MarketValue.OTHER, otherChance));
    }


    List<MarketChance> getPoissonTable() {
        return poissonTable;
    }

    @Override
    public String toString(){
        StringBuilder poissonTableString = new StringBuilder("\n");

        for(MarketChance mc: this.getPoissonTable() ){
            poissonTableString.append("Market: ").append(mc.getMarketValue().toString()).append(" - Chance: ").append(mc.getChanceRepresentation()).append("\n");
        }
        return poissonTableString.toString();
    }

    public double getHomeExpectancy() {
        return homeExpectancy;
    }

    public double getAwayExpectancy() {
        return awayExpectancy;
    }
}
