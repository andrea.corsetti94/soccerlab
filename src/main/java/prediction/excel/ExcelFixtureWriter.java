package prediction.excel;

import main.Configs;
import main.Logger;
import model.Fixture;
import odds.Markets;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import prediction.MatchProbability;
import prediction.PoissonPredictor;
import utils.DateUtils;
import utils.FixturesUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelFixtureWriter {

    public void writeFixtureList(List<Fixture> fixtureList, String filename){
        //Create blank workbook
        Workbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        Sheet spreadsheet = workbook.createSheet(filename);

        Row row = spreadsheet.createRow(0);

        //create columns
        int colIndex = 0;
        row.createCell(colIndex).setCellValue("Date");
        row.createCell(++colIndex).setCellValue("Country");
        row.createCell(++colIndex).setCellValue("League");
        row.createCell(++colIndex).setCellValue("Home Team");
        row.createCell(++colIndex).setCellValue("Away Team");
        row.createCell(++colIndex).setCellValue("Home");
        row.createCell(++colIndex).setCellValue("Draw");
        row.createCell(++colIndex).setCellValue("Away");
        row.createCell(++colIndex).setCellValue("Goal");
        row.createCell(++colIndex).setCellValue("No Goal");
        row.createCell(++colIndex).setCellValue("Over 1,5");
        row.createCell(++colIndex).setCellValue("Under 1,5");
        row.createCell(++colIndex).setCellValue("Over 2,5");
        row.createCell(++colIndex).setCellValue("Under 2,5");
        row.createCell(++colIndex).setCellValue("Over 3,5");
        row.createCell(++colIndex).setCellValue("Under 3,5");
        row.createCell(++colIndex).setCellValue("Over 4,5");
        row.createCell(++colIndex).setCellValue("Under 4,5");
        row.createCell(++colIndex).setCellValue("Home OR Draw");
        row.createCell(++colIndex).setCellValue("Draw OR Away");
        row.createCell(++colIndex).setCellValue("Home OR Away");
        row.createCell(++colIndex).setCellValue("Home AND Goal");
        row.createCell(++colIndex).setCellValue("Home AND No Goal");
        row.createCell(++colIndex).setCellValue("Draw AND Goal");
        row.createCell(++colIndex).setCellValue("Draw AND No Goal");
        row.createCell(++colIndex).setCellValue("Away AND Goal");
        row.createCell(++colIndex).setCellValue("Away AND No Goal");
        row.createCell(++colIndex).setCellValue("Home & Over 1,5");
        row.createCell(++colIndex).setCellValue("Home & Under 1,5");
        row.createCell(++colIndex).setCellValue("Home & Over 2,5");
        row.createCell(++colIndex).setCellValue("Home & Under 2,5");
        row.createCell(++colIndex).setCellValue("Home & Over 3,5");
        row.createCell(++colIndex).setCellValue("Home & Under 3,5");
        row.createCell(++colIndex).setCellValue("Home & Over 4,5");
        row.createCell(++colIndex).setCellValue("Home & Under 4,5");
        row.createCell(++colIndex).setCellValue("Draw & Over 1,5");
        row.createCell(++colIndex).setCellValue("Draw & Under 1,5");
        row.createCell(++colIndex).setCellValue("Draw & Over 2,5");
        row.createCell(++colIndex).setCellValue("Draw & Under 2,5");
        row.createCell(++colIndex).setCellValue("Draw & Over 3,5");
        row.createCell(++colIndex).setCellValue("Draw & Under 3,5");
        row.createCell(++colIndex).setCellValue("Draw & Over 4,5");
        row.createCell(++colIndex).setCellValue("Draw & Under 4,5");
        row.createCell(++colIndex).setCellValue("Away & Over 1,5");
        row.createCell(++colIndex).setCellValue("Away & Under 1,5");
        row.createCell(++colIndex).setCellValue("Away & Over 2,5");
        row.createCell(++colIndex).setCellValue("Away & Under 2,5");
        row.createCell(++colIndex).setCellValue("Away & Over 3,5");
        row.createCell(++colIndex).setCellValue("Away & Under 3,5");
        row.createCell(++colIndex).setCellValue("Away & Over 4,5");
        row.createCell(++colIndex).setCellValue("Away & Under 4,5");
        row.createCell(++colIndex).setCellValue("0 - 0");
        row.createCell(++colIndex).setCellValue("1 - 0");
        row.createCell(++colIndex).setCellValue("0 - 1");
        row.createCell(++colIndex).setCellValue("1 - 1");
        row.createCell(++colIndex).setCellValue("2 - 0");
        row.createCell(++colIndex).setCellValue("0 - 2");
        row.createCell(++colIndex).setCellValue("3 - 0");
        row.createCell(++colIndex).setCellValue("2 - 1");
        row.createCell(++colIndex).setCellValue("1 - 2");
        row.createCell(++colIndex).setCellValue("0 - 3");
        row.createCell(++colIndex).setCellValue("4 - 0");
        row.createCell(++colIndex).setCellValue("3 - 1");
        row.createCell(++colIndex).setCellValue("2 - 2");
        row.createCell(++colIndex).setCellValue("1 - 3");
        row.createCell(++colIndex).setCellValue("0 - 4");
        row.createCell(++colIndex).setCellValue("4 - 1");
        row.createCell(++colIndex).setCellValue("3 - 2");
        row.createCell(++colIndex).setCellValue("2 - 3");
        row.createCell(++colIndex).setCellValue("1 - 4");
        row.createCell(++colIndex).setCellValue("4 - 2");
        row.createCell(++colIndex).setCellValue("3 - 3");
        row.createCell(++colIndex).setCellValue("2 - 4");
        row.createCell(++colIndex).setCellValue("4 - 3");
        row.createCell(++colIndex).setCellValue("3 - 4");
        row.createCell(++colIndex).setCellValue("4 - 4");
        row.createCell(++colIndex).setCellValue("Other");

        List<Fixture> fixturesAlreadyWritten = new ArrayList<>();

        fixtureList.forEach( fixture -> {

            if ( fixturesAlreadyWritten.contains(fixture) ) return;
            fixturesAlreadyWritten.add(fixture);

            if ( fixture.isCompleted() ) return;
            MatchProbability fixtureMatchProbability = PoissonPredictor.getMatchProbability(fixture);

            if ( ! fixtureMatchProbability.isReliable() ) return;

            int num = spreadsheet.getLastRowNum();
            Row nextRow = spreadsheet.createRow(++num);

            String leagueName = FixturesUtils.leagueNameByFixture(fixture);
            String countryName = FixturesUtils.countryNameByFixture(fixture);
            String eventDate = DateUtils.toString(fixture.getEventDate(), Configs.getFixtureStringFormat());

            int cellColumnIndex = 0;

            nextRow.createCell(cellColumnIndex).setCellValue(eventDate);
            nextRow.createCell(++cellColumnIndex).setCellValue(countryName);
            nextRow.createCell(++cellColumnIndex).setCellValue(leagueName);
            nextRow.createCell(++cellColumnIndex).setCellValue(fixture.getHomeTeam());
            nextRow.createCell(++cellColumnIndex).setCellValue(fixture.getAwayTeam());
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.BTTS_YES));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.BTTS_NO));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.O1_5));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.U1_5));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.O2_5));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.U2_5));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.O3_5));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.U3_5));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.O4_5));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.U4_5));
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.DC_HOME_DRAW) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.DC_DRAW_AWAY) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.DC_HOME_AWAY) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_BTTS_HOME_YES) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_BTTS_HOME_NO) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_BTTS_DRAW_YES) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_BTTS_DRAW_NO) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_BTTS_AWAY_YES) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_BTTS_AWAY_NO) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME_O1_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME_U1_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME_O2_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME_U2_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME_O3_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME_U3_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME_O4_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_HOME_U4_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW_O1_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW_U1_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW_O2_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW_U2_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW_O3_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW_U3_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW_O4_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_DRAW_U4_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY_O1_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY_U1_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY_O2_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY_U2_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY_O3_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY_U3_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY_O4_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.HDA_AWAY_U4_5) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.NIL_NIL) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.ONE_NIL) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.NIL_ONE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.ONE_ONE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.TWO_NIL) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.NIL_TWO) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.THREE_NIL) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.TWO_ONE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.ONE_TWO) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.NIL_THREE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.FOUR_NIL) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.THREE_ONE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.TWO_TWO) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.ONE_THREE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.NIL_FOUR) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.FOUR_ONE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.THREE_TWO) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.TWO_THREE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.ONE_FOUR) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.FOUR_TWO) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.THREE_THREE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.TWO_FOUR) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.FOUR_THREE) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.THREE_FOUR) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.FOUR_FOUR) );
            nextRow.createCell(++cellColumnIndex)
                    .setCellValue(fixtureMatchProbability.getProbabilityRepresentation(Markets.MarketValue.OTHER) );
        });

        //adjust column width
        for(int i = 0; i <= colIndex; i++) {
            spreadsheet.autoSizeColumn(i);
        }

        File f = new File(this.buildFilePath(filename));

        //Write the workbook in file system
        try (FileOutputStream out = new FileOutputStream(f)
        ){
            workbook.write(out);
        }
        catch(IOException e) {
            Logger.getLogger(this.getClass()).error("IOException writing file: " + filename, e);
        }
    }

    private String buildFilePath(String filename){
        String format = ".xlsx";
        if ( !filename.endsWith(format) ) filename += format;
        return Configs.getExcelFixturesPath() + filename;
    }
}
