package prediction;

import data.db.retriever.FixtureDataRetriever;
import main.Configs;
import model.Fixture;
import model.statistics.TeamForm;
import utils.FixturesUtils;

import java.util.List;

class TeamStrengthCalculator {

    static TeamForm extractForm(String teamId) {

        List<Fixture> lastTeamMatches = lastMatches(teamId);

        int totalMatchesCount = 0;
        int homeMatchesCount = 0;
        int awayMatchesCount = 0;

        double homeForm = 0.0;
        double awayForm = 0.0;
        double totalForm = 0.0;

        lastTeamMatches.sort(FixturesUtils.getDateComparator());

        for (int i = lastTeamMatches.size() - 1; i >= 0; i--) {
            Fixture fixture = lastTeamMatches.get(i);
            int pointsForGame = fixture.pointsForTeam(teamId);

            if (fixture.getHomeTeamId().equals(teamId)) {
                homeMatchesCount++;
                homeForm += pointsForGame;
            }
            else if (fixture.getAwayTeamId().equals(teamId) ) {
                awayMatchesCount++;
                awayForm += pointsForGame;
            }

            totalMatchesCount++;
            totalForm += pointsForGame;

        }

        if (homeMatchesCount != 0) {
            homeForm = homeForm / homeMatchesCount;
        }

        if (awayMatchesCount != 0) {
            awayForm = awayForm / awayMatchesCount;
        }

        if (totalMatchesCount != 0) {
            totalForm = totalForm / totalMatchesCount;
        }

        return new TeamForm(teamId, homeForm, awayForm, totalForm);

    }

    private static List<Fixture> lastMatches(String teamId){
        FixtureDataRetriever fixtureDataRetriever = new FixtureDataRetriever();
        return fixtureDataRetriever.getLastTeamMatches(teamId, Configs.getTeamFormMatchesToConsider());
    }

}
