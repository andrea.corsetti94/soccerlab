package prediction;

abstract class Expectancy {

    private final String homeTeamId;
    private final String awayTeamId;

    Expectancy(String homeTeamId, String awayTeamId) {
        this.homeTeamId = homeTeamId;
        this.awayTeamId = awayTeamId;

        this.computeExpectancy();
    }

    String getHomeTeamId(){
        return homeTeamId;
    }
    String getAwayTeamId(){
        return awayTeamId;
    }

    public abstract double getHomeGoalExpectancy();
    public abstract double getAwayGoalExpectancy();

    protected abstract void computeExpectancy();

    @Override
    public String toString() {
        return "Expectancy{" +
                "homeGoalExpectancy=" + this.getHomeGoalExpectancy() +
                ", awayGoalExpectancy=" + this.getAwayGoalExpectancy() +
                '}';
    }
}
