package prediction;

import data.db.retriever.FixtureDataRetriever;
import main.Configs;
import model.Fixture;
import utils.FixturesUtils;

import java.util.List;

class TeamStatsComparator {

    private static final int NUM_OF_MATCHES_TO_CONSIDER = Configs.getMatchesToConsider();

    private final String homeTeamId;
    private final String awayTeamId;

    private double homeAttackingStrength;
    private double homeDefensiveStrength;

    private double awayAttackingStrength;
    private double awayDefensiveStrength;


    TeamStatsComparator(String homeTeamId, String awayTeamId) {
        this.homeTeamId = homeTeamId;
        this.awayTeamId = awayTeamId;

        this.compare();
    }

    private void compare(){
        FixtureDataRetriever retriever = new FixtureDataRetriever();

        List<Fixture> lastHomeTeamMatches = retriever.getLastTeamMatches(this.homeTeamId, NUM_OF_MATCHES_TO_CONSIDER);
        int homeMatchesSize = lastHomeTeamMatches.size();

        List<Fixture> lastAwayTeamMatches = retriever.getLastTeamMatches(this.awayTeamId, NUM_OF_MATCHES_TO_CONSIDER);
        int awayMatchesSize = lastAwayTeamMatches.size();

        int homeTeamScoredGoals = FixturesUtils.goalScored(lastHomeTeamMatches, this.homeTeamId);
        int homeTeamReceivedGoals = FixturesUtils.goalReceived(lastHomeTeamMatches, this.homeTeamId);
        double avgHomeTeamScoredGoals = 0;
        double avgHomeTeamReceivedGoals = 0;
        if ( homeMatchesSize > 0 ) {
            avgHomeTeamScoredGoals = (double) homeTeamScoredGoals / homeMatchesSize;
            avgHomeTeamReceivedGoals = (double) homeTeamReceivedGoals / homeMatchesSize;
        }

        int awayTeamScoredGoals = FixturesUtils.goalScored(lastAwayTeamMatches, this.awayTeamId);
        int awayTeamReceivedGoals = FixturesUtils.goalReceived(lastAwayTeamMatches, this.awayTeamId);

        double avgAwayTeamScoredGoals = 0;
        double avgAwayTeamReceivedGoals = 0;
        if (awayMatchesSize > 0) {
            avgAwayTeamScoredGoals = (double) awayTeamScoredGoals / awayMatchesSize;
            avgAwayTeamReceivedGoals = (double) awayTeamReceivedGoals / awayMatchesSize;
        }

        double avgScoredGoals = (avgHomeTeamScoredGoals + avgAwayTeamScoredGoals) / 2;
        double avgReceivedGoals = (avgHomeTeamReceivedGoals + avgAwayTeamReceivedGoals) / 2;

        this.homeAttackingStrength = avgHomeTeamScoredGoals / avgScoredGoals;
        this.awayAttackingStrength = avgAwayTeamScoredGoals / avgScoredGoals;

        this.homeDefensiveStrength = avgHomeTeamReceivedGoals / avgReceivedGoals;
        this.awayDefensiveStrength = avgAwayTeamReceivedGoals /avgReceivedGoals;

    }

    double getHomeAttackingStrength() {
        return homeAttackingStrength;
    }

    double getHomeDefensiveStrength() {
        return homeDefensiveStrength;
    }

    double getAwayAttackingStrength() {
        return awayAttackingStrength;
    }

    double getAwayDefensiveStrength() {
        return awayDefensiveStrength;
    }

    @Override
    public String toString() {
        return "TeamStatsComparator{" +
                "homeTeamId='" + homeTeamId + '\'' +
                ", homeAttackingStrength=" + homeAttackingStrength +
                ", homeDefensiveStrength=" + homeDefensiveStrength +
                ", awayAttackingStrength=" + awayAttackingStrength +
                ", awayDefensiveStrength=" + awayDefensiveStrength +
                '}';
    }
}
