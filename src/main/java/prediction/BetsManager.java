package prediction;

import data.db.dao.DAOBets;
import data.db.retriever.AlgorithmDataRetriever;
import data.db.retriever.BetsDataRetriever;
import data.db.retriever.MatchPickRetriever;
import model.Algorithm;
import model.Bet;
import model.MatchPick;
import utils.AlgoBetsUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BetsManager {

    public static void placeBets(DAOBets daoBets){
        final double lowerBoundOdds = 1.10;
        final double upperBoundOdds = 4.5;
        List<Bet> bets = new ArrayList<>();
        BetsDataRetriever betsDataRetriever = new BetsDataRetriever();

        List<MatchPick> matchPickList = new MatchPickRetriever().retrieveOpenValuePicks();

        List<Algorithm> algorithms = new AlgorithmDataRetriever().retrieveAlgorithms();
        for(Algorithm algorithm: algorithms){
            int betsToPlace = new Random().nextInt(10) + 8; //random between 8 and 10

            List<MatchPick> algoPicks = AlgoBetsUtils.randomPicks(matchPickList, betsToPlace);
            for(MatchPick pick: algoPicks){

                //garantees that an algorithm bets on a single match max 1 time.
                if ( betsDataRetriever.hasAlgoAlreadyPlacedBetOnFixture(algorithm.getIdName(), pick.getFixtureId()) ) continue;

                if ( pick.getOddValue() <= upperBoundOdds && pick.getOddValue() >= lowerBoundOdds ){
                    Bet bet = new Bet(algorithm.getIdName(), pick.getFixtureId(), pick.getMarketValue(), AlgoBetsUtils.randomAmount(), pick.getOddValue(), 0);
                    bets.add(bet);
                }
            }
        }

        daoBets.put(bets);
    }

}
